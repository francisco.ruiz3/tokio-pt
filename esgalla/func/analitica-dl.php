<?php
function datalayer_errorcampo($validation_result){
	$form = $validation_result['form'];
	$inputNames = array_column($form['fields'], 'inputName', 'id');
	$valores = GFFormsModel::get_current_lead();

	$id_campo_prelead_tipo = array_search('prelead_tipo', $inputNames);
	$prelead_tipo = $valores[$id_campo_prelead_tipo];

	$id_campo_idform = array_search('prelead_form', $inputNames);
	$identificacion_formularios = $valores[$id_campo_idform];

	foreach ($form['fields'] as &$field) {
		if ($field->failed_validation == true) {
			$valorCookieGA = $_COOKIE["idgatokio"];
			$script = '<script>
				var user_id = "'.str_replace('"','',$valorCookieGA).'";
				user_id  = (user_id  != "") ? user_id  : undefined;
				var gclid = sessionStorage.getItem("dl_gclid"); gclid  = (gclid === null) ? undefined  : gclid;
	      	var mll = sessionStorage.getItem("dl_mll"); mll  = (mll  === null) ? undefined  : mll;
				window.dataLayer = window.dataLayer || [];
				dataLayer.push({
					"ClientID": user_id,
					"mll": mll,
					"gclid": gclid,
					"event": "Event",
					"eventCategory": "'.$prelead_tipo.'",
					"eventAction": "solicitud_error",
					"eventLabel": "error_' . $identificacion_formularios . '_' . $inputNames[$field->id] . '"
				});
			</script>';
			$field->validation_message = $field->validation_message . $script;
		}

	}
	return $validation_result;
}

add_filter('gform_validation', 'datalayer_errorcampo'); //000 Lead - Simple



function datalayer_rellenarcampos($formulario) {
	date_default_timezone_set('Europe/Madrid');

	$inputNames = array_column($formulario['fields'], 'inputName', 'id');
	$id_campo_userid = 'input_'.array_search('user_id', $inputNames);
	$id_campo_producto_id = 'input_'.array_search('producto_id', $inputNames);
	$id_campo_mll = 'input_'.array_search('mll', $inputNames);
	$id_campo_telefono = 'input_'.array_search('telefono', $inputNames);
	$id_campo_fechaentrada = 'input_'.array_search('fecha_entrada', $inputNames);
	$id_campo_lead_id = 'input_'.array_search('lead_id', $inputNames);
	$id_campo_transaction_id = 'input_'.array_search('transaction_id', $inputNames);

	$producto_id = (rgpost( $id_campo_producto_id ) != "") ? rgpost( $id_campo_producto_id ) : "0000";
	$mll = (rgpost( $id_campo_mll ) != "") ? rgpost( $id_campo_mll ) : "0000";
	$tlf = (rgpost( $id_campo_telefono ) != "") ? rgpost( $id_campo_telefono ) : "0000";

	$user_id = 'userid-'.str_replace(' ', '', $producto_id.'-'.$mll.'-'.substr($tlf,-4).'-'.date("Ymd-Gis"));
	$lead_id = $producto_id.'-'.substr($tlf, -4).'-'.date("Ymd-Gis");

	$_POST[$id_campo_fechaentrada] = date("Y-m-d H:i:s");
	$_POST[$id_campo_lead_id] = $lead_id;
	$_POST[$id_campo_userid] = $user_id;
	$_POST[$id_campo_transaction_id] = 'TL-'.$lead_id;
	if(isset($_COOKIE['idgatokio'])) $_POST[$id_campo_userid] = $_COOKIE['idgatokio'];
}
add_action( 'gform_pre_submission', 'datalayer_rellenarcampos', 10, 2 );




function datalayer_confirmacion($valores, $formulario){
	//$id_pagina_envio = url_to_postid($valores['source_url']);
	//echo '<script>console.log('.json_encode($valores).');</script>';
	//echo '<script>console.log('.json_encode($formulario).');</script>';
	//echo '<script>pasos_formularios('.$formulario['id'].', "trysend", 2);</script>';

	//Creamos un array con ID=>inputName de los campos para facilitar la búsqueda
	$inputNames = array_column($formulario['fields'], 'inputName', 'id');
	//echo '<script>console.log('.json_encode($inputNames).');</script>';


	//Buscamos los valores de los diferentes campos
	$id_campo_prelead_name = array_search('prelead_name', $inputNames);
	$page_title = $valores[$id_campo_prelead_name];

	$id_campo_idcurso = array_search('producto_id', $inputNames);
	$product_id = $valores[$id_campo_idcurso];

	$id_campo_nombrecurso = array_search('producto_nombre', $inputNames);
	$product_name = $valores[$id_campo_nombrecurso];
	$product_name_conbarras = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $product_name);
	$product_name_conbarras = strtolower(str_replace(' ', '_', $product_name_conbarras));

	$id_campo_tipocurso = array_search('producto_tipo', $inputNames);
	$product_type = $valores[$id_campo_tipocurso];

	$id_campo_categoriacurso = array_search('producto_categoria', $inputNames);
	$product_category = $valores[$id_campo_categoriacurso];


	$id_campo_paginatipo = array_search('prelead_tipo', $inputNames);
	$page_type = $valores[$id_campo_paginatipo];

	$id_campo_lead_id = array_search('lead_id', $inputNames);
	$lead_id = $valores[$id_campo_lead_id];

	$id_campo_transaction_id = array_search('transaction_id', $inputNames);
	$transaction_id = $valores[$id_campo_transaction_id];

	// Otros valores
	// Comentamos el valor $transaction_id por ser el antiguo
	// $transaction_id = 'T'.$valores['id'];
	$tipo_solicitud = "solicitud_formacion_trysend";
	$valorCookieGA = (isset($_COOKIE["idgatokio"])) ? $_COOKIE["idgatokio"] : '' ;

	echo '
	<script>
		$valores_formulario = {
			"producto_id": "'.$product_id.'",
			"producto_nombre": "'.$product_name.'",
			"producto_nombre_barras": "'.$product_name_conbarras.'",
			"producto_tipo": "'.$product_type.'",
			"producto_categoria": "'.$product_category.'",
			"prelead_name": "'.$page_title.'",
			"prelead_tipo": "'.$page_type.'",
			"lead_id": "'.$lead_id.'",
			"transaction_id": "'.$transaction_id.'",
			"valorCookieGA": "'.$valorCookieGA.'",
			"tipo_solicitud": "'.$tipo_solicitud.'",
			"usuario_ip": "'.$valores["ip"].'",
			"usuario_id": "'.str_replace('"','',$valorCookieGA).'",
		};

		sessionStorage.setItem("valores_formulario", JSON.stringify($valores_formulario));
	</script>
	';

}

add_action('gform_after_submission', 'datalayer_confirmacion', 10, 3);




function envio_datos_crm($valorEntradas, $formulario) {
	date_default_timezone_set('Europe/Madrid');

	//echo ' <script type="text/javascript">console.log("variable formulario campos");console.log('.json_encode($formulario->fields).')</script>';
	//echo ' <script type="text/javascript">console.log("variable formulario");console.log('.json_encode($formulario).')</script>';
	$inputNames = array_column($formulario['fields'], 'inputName', 'id');

	//En la consola de las herramientas de desarrollo activar el registro persistente
	//Primero mostrarmos la variable $formulario, para comprobar los datos que recibimos
	//echo ' <script type="text/javascript">console.log("variable inputNames");console.log('.json_encode($inputNames).')</script>';

	$id_campo_idcurso = 'input_'.array_search('producto_id', $inputNames);
	$id_campo_tipocurso = 'input_'.array_search('producto_tipo', $inputNames);
	$id_campo_nombrecurso = 'input_'.array_search('nombrecurso', $inputNames);
	$id_campo_modollegada = 'input_'.array_search('modollegada', $inputNames);

	$id_campo_nombre = 'input_'.array_search('nombre', $inputNames);
	$id_campo_telefono = 'input_'.array_search('telefono', $inputNames);
	$id_campo_email = 'input_'.array_search('email', $inputNames);
	$id_campo_cp = 'input_'.array_search('codigopostal', $inputNames);

	$id_campo_prenombre = 'input_'.array_search('prelead_name', $inputNames);
	$id_campo_pretipo = 'input_'.array_search('prelead_tipo', $inputNames);
	$id_campo_preseccion = 'input_'.array_search('prelead_seccion', $inputNames);
	$id_campo_preform = 'input_'.array_search('prelead_form', $inputNames);
	// $id_campo_preautor = 'input_'.array_search('prelead_author', $inputNames);
	// $id_campo_precontent = 'input_'.array_search('prelead_content', $inputNames);

	$id_campo_mll = 'input_'.array_search('mll', $inputNames);
	$id_campo_userid = 'input_'.array_search('user_id', $inputNames);
	$id_campo_gclid = 'input_'.array_search('gclid', $inputNames);
	$id_campo_leadid = 'input_'.array_search('lead_id', $inputNames);
	$id_campo_district = 'input_'.array_search('district', $inputNames);

	$transaction_id = 'T'.$valorEntradas['id'];

	$development_websites = array("https://enter.tokioschool.com/");
	$endpoint = "https://tools.campustraining.es/tasks/campustraining-load-leads-noredirect.php";
	if ( in_array(  get_site_url(), $development_websites ) ) {
		$endpoint = "https://ctintegration.nicesigh.com/tasks/campustraining-load-leads-noredirect.php";
	}

	$endpoint .= '?idcurso=' . urlencode(rgpost( $id_campo_idcurso ));
	$endpoint .= '&tipocurso=' . urlencode(rgpost( $id_campo_tipocurso ));
	$endpoint .= '&nombrecurso=' . urlencode(rgpost( $id_campo_nombrecurso ));

	$endpoint .= '&nombre=' . urlencode(rgpost( $id_campo_nombre ));
	$endpoint .= '&telefono=' . urlencode(rgpost( $id_campo_telefono ));
	$endpoint .= '&email=' . urlencode(rgpost( $id_campo_email ));
	$endpoint .= '&codigo=' . urlencode(rgpost( $id_campo_cp ));
	$endpoint .= '&modollegada' . urlencode(rgpost( $id_campo_modollegada ));

	$endpoint .= '&pre_nombre=' . urlencode(rgpost( $id_campo_prenombre ));
	$endpoint .= '&pre_tipo=' . urlencode(rgpost( $id_campo_pretipo ));
	$endpoint .= '&pre_seccion=' . urlencode(rgpost( $id_campo_preseccion ));
	$endpoint .= '&pre_form=' . urlencode(rgpost( $id_campo_preform ));
	// $endpoint .= '&pre_autor=' . urlencode(rgpost( $id_campo_preautor ));
	$endpoint .= '&pre_autor=';
	// $endpoint .= '&pre_content=' . urlencode(rgpost( $id_campo_precontent ));
	$endpoint .= '&pre_content=';

	$endpoint .= '&modo_llegada=' . urlencode(rgpost( $id_campo_mll ));
	$endpoint .= '&idAnalytics=' . urlencode(rgpost( $id_campo_leadid ));
	$endpoint .= '&sgads=' . urlencode(rgpost( $id_campo_gclid ));
	$endpoint .= '&iduser=' . urlencode(rgpost( $id_campo_userid ));
	$endpoint .= '&idlead=' . urlencode(rgpost( $id_campo_leadid ));
	$endpoint .= '&idtransaction=' . urlencode($transaction_id);
	$endpoint .= '&ip=' . urlencode($valorEntradas['ip']);
	$endpoint .= '&idpais=591';
	$endpoint .= '&district=' . urlencode(rgpost( $id_campo_district ));

	//Le decimos al CRM si el lead viene de una página tipo landing o no
	if(rgpost($id_campo_pretipo) == 'landing')	$endpoint .= '&landing=true';
	else 														$endpoint .= '&landing=false';

	//Ahora comprobamos que la URL de petición se ha generado correctamente
	// echo '<script type="text/javascript">console.log("url de envío");console.warn("'.$endpoint.'")</script>';

	//Una vez hayamos verificado la URL anterior ya podemos hacer la llamada a la API

	//Get request
	$curl_get = curl_init($endpoint);
	curl_setopt($curl_get, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($curl_get, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($curl_get);

	//echo '<script type="text/javascript">console.log("respuesta");console.warn('.json_encode($respuesta_cambiada).')</script>';

	if($response != 'OK') {
	}
	else {
	}

	//curl_close($curl_campaignmonitor);
}

add_action( 'gform_after_submission', 'envio_datos_crm', 11, 2 );

function datos_curso(){
	$slug = get_post_field( 'post_name', get_post() );

	$cat_slug = "undefined";

	$terms = wp_get_post_terms( get_the_ID(), 'categorias_formacion', array());

	if($terms){

		foreach ($terms as $x) {

			$cat_slug = $x->slug;

		}

	}

	//



	$datos_curso["cat_curso"] = $cat_slug;

	//$datos_curso["id_curso"] = get_field("id");

	//$datos_curso["nombre_curso"] = $slug;



	return $datos_curso;

}

function getPaisId($prefijoPais) {
  switch ($prefijoPais) {
    case '0034':
      //ESPAÑA
      $idPais = 1;
    break;
    case '0057':
      //COLOMBIA
      $idPais = 1071;
    break;
    case '0056':
      //CHILE
      $idPais = 1110;
    break;
    case '0052':
      //MEXICO
      $idPais = 1015;
    break;
    case '0051':
      //PERÚ
      $idPais = 1330;
    break;
    case '00593':
      //ECUADOR
      $idPais = 1112;
    break;
    case '00351':
      //PORTUGAL
      $idPais = 591;
    break;
		case '0054':
      //ARGENTINA
      $idPais = 1106;
    break;
    case '001':
      //EEUU
      $idPais = 1244;
    break;
    case '0044':
      //REINO UNIDO
      $idPais = 1000;
    break;
    case '0058':
      //VENEZUELA
      $idPais = 1134;
    break;
    case '00502':
      //GUATEMALA
      $idPais = 1048;
    break;
    case '0033':
      //FRANCIA
      $idPais = 283;
    break;
    default:
      //DEFECTO: ESPAÑA
      $idPais = 1;
    break;
  }

  return $idPais;
}