<?php

	//Inicializar variables

	//Datos del cliente

	$GLOBALS['gclid'] = 


	$GLOBALS['user_id'] =

	$GLOBALS['user_ip'] =



	//Datos de la URL

	$GLOBALS['slots'] =



	//Varios

	$GLOBALS['error_id'] =

	$GLOBALS['test_id'] =



	//Blog

	$GLOBALS['blog_author'] =

	$GLOBALS['blog_cat'] =

	$GLOBALS['blog_date'] =

	$GLOBALS['blog_tag'] =



	//Página

	$GLOBALS['page_section'] =

	$GLOBALS['page_title'] =

	$GLOBALS['page_type'] =

	$GLOBALS['page_url'] =

	$GLOBALS['page_path'] =



	//Producto

	$GLOBALS['product_category'] =

	$GLOBALS['product_id'] =

	$GLOBALS['product_name'] =

	$GLOBALS['product_type'] = "";



	//Definición de variables

	$GLOBALS['user_ip'] = $_SERVER['REMOTE_ADDR'];

	$tipo_post = get_post_type();



	//Casos generales

	$GLOBALS['page_title'] = get_the_title();

	if(!isset($GLOBALS['page_title'])) $GLOBALS['page_title'] = get_field('nombre_de_pagina');

	if(!isset($GLOBALS['page_title'])) $GLOBALS['page_title'] = get_post_meta(get_the_ID(), '_yoast_wpseo_title', true);

	//echo '<script>console.log("el nombre de la pagina es: '.$GLOBALS['page_title'].'")</script>';







	// #####################

	// ## CASOS GENERALES ##

	// #####################

    //Buscador

	if(is_search()){

		$GLOBALS['page_section'] = "busqueda";

		if(have_posts()){

			$GLOBALS['page_type'] = "resultados";

		}else{

			$GLOBALS['page_type'] = "sin-resultados";

		}



		$GLOBALS['page_title'] = "?s=".get_search_query();

	}

	//Blog

	elseif(get_post_type()=="post"){

		$GLOBALS['page_section'] = "noticias";

		if(is_single()){ //Noticia individual

			//$GLOBALS['page_title'] = get_post_field('post_name', get_post());

			//$GLOBALS['page_type'] = "articulo/".$GLOBALS['page_title'];

			$GLOBALS['page_type'] = "articulo";



			/*$cat = get_the_category();

			if($cat){

				foreach ($cat as $x) {

					if(!isset($GLOBALS['blog_cat'])){$GLOBALS['blog_cat'] = $x->slug;}else{$GLOBALS['blog_cat'] .= ','.$x->slug;}

				}

			}*/



			$primary_cat_id = get_post_meta(get_the_ID(), '_yoast_wpseo_primary_category', true);

			if($primary_cat_id){

				$product_cat = get_term($primary_cat_id);

				if(isset($product_cat->name)) $GLOBALS['blog_cat'] = $product_cat->slug;

			}



			$tag = get_the_tags();

			if($tag){

				$GLOBALS['blog_tag'] = $tag[0]->slug;

				/*foreach ($tag as $x) {

					if(!isset($GLOBALS['blog_tag'])){$GLOBALS['blog_tag'] = $x->slug;}

					else{$GLOBALS['blog_tag'] .= ','.$x->slug;}

				}*/

			}



			//$GLOBALS['blog_author'] = get_the_author_meta('nicename');

			$GLOBALS['blog_author'] = get_the_author_meta('first_name',$post->post_author);

			$GLOBALS['blog_date'] = get_the_date("m_Y");





			//$GLOBALS['page_title'] = $GLOBALS['product_name'] = ($datos_curso["sub_cat_curso_name"] != "undefined") ? $datos_curso["cat_curso_name"].'/'.$datos_curso["sub_cat_curso_name"].'/'.$datos_curso["nombre_curso"] : $datos_curso["cat_curso_name"].'/'.$datos_curso["nombre_curso"];



			$GLOBALS['product_id'] = get_field("id");



			$posts = get_posts(array(

				'numberposts'	=> 1,

				'post_type'		=> 'formacion',

				'meta_key'		=> 'id',

				'meta_value'	=> $GLOBALS['product_id']

			));

			// var_dump($posts);

			$id_post_wordpress = $posts[0]->ID;

			$curso_tipo = get_field('tipo', $id_post_wordpress);

			$GLOBALS['product_name'] = get_the_title($id_post_wordpress);

			$GLOBALS['product_category'] = wp_get_post_terms( $id_post_wordpress, 'categorias_formacion', array())[0]->slug;

			$GLOBALS['product_type'] = wp_get_post_terms( $id_post_wordpress, 'tag_formacion', array())[0]->slug;



		}

		else if(is_category()){ //Categoría

			$queried_object = get_queried_object();

			$GLOBALS['page_type'] = "categoria";

			//$GLOBALS['page_title'] = $GLOBALS['blog_cat'] = $queried_object->slug;



		}else if(is_tag()){ //Etiqueta

			$queried_object = get_queried_object();

			$GLOBALS['page_type'] = "etiqueta";

			//$GLOBALS['page_title'] = $GLOBALS['blog_tag'] = $queried_object->slug;

		}

	}



	//Cursos

	else if($tipo_post == "formacion" || is_post_type_archive("formacion") || is_singular("formacion") || is_tax("formacion")){

		$GLOBALS['page_section'] = "formacion";

		if(is_archive() && !is_tax()){//Página principal



		}else if(is_archive()){//Categorías de curso

			$queried_object = get_queried_object();

			// if(isset($_GET['test'])) {

			// 	var_dump($queried_object);

			// }

			if($queried_object->parent == 0){

				$GLOBALS['product_type'] = $queried_object->slug;

				$GLOBALS['product_category'] = $queried_object->slug;

			}else{

				$cat_principal = $queried_object->parent;

				$cat_principal_obj = get_term_by("id",$cat_principal,"categorias_formacion");

				$GLOBALS['product_category'] = $cat_principal_obj->slug;

				//$GLOBALS['page_title'] = $queried_object->slug;

			}



			if(get_field("id_formulario",$queried_object)){

				$GLOBALS['product_id'] = get_field("id_formulario",$queried_object);

			}



			$GLOBALS['page_type'] =  "categoria";



		}else{//Página de curso individual



			$datos_curso = datos_curso();



			$GLOBALS['page_type'] = "detalle_formacion";



			//$GLOBALS['page_title'] = $GLOBALS['product_name'] = ($datos_curso["sub_cat_curso_name"] != "undefined") ? $datos_curso["cat_curso_name"].'/'.$datos_curso["sub_cat_curso_name"].'/'.$datos_curso["nombre_curso"] : $datos_curso["cat_curso_name"].'/'.$datos_curso["nombre_curso"];



			$GLOBALS['product_id'] = get_field("id");

			$GLOBALS['product_name'] = get_the_title();

			$GLOBALS['product_category'] = wp_get_post_terms( get_the_ID(), 'categorias_formacion', array())[0]->slug;

			// $GLOBALS['product_type'] = wp_get_post_terms( get_the_ID(), 'tag_formacion', array())[0]->slug;

			$GLOBALS['product_type'] = get_field('tipo');

		}

	}



	//Landings -- la pongo aquí de cara al futuro porque parece que no carga

	//los valores tan siquiera pero por tener el tema localizado más que nada

	else if($tipo_post == "landings" || $tipo_post == "landing-elementor"){

		$GLOBALS['page_section'] = "landing";

		$GLOBALS['page_type'] = "landing";

		if(is_single() ){

			
			$idProducto = get_field('id_producto');

			$GLOBALS['product_id'] = get_field("id", $idProducto);

			$GLOBALS['product_name'] = get_the_title($idProducto);

			$GLOBALS['product_category'] = wp_get_post_terms( $idProducto, 'categorias_formacion', array())[0]->slug;

			$GLOBALS['product_type'] = wp_get_post_terms( $idProducto, 'tag_formacion', array())[0]->slug;

		}

	}













	// ########################

	// ## CASOS PARTICULARES ##

	// ########################



	//---------------------------------------------------------------------- Páginas de agradecimiento

	if(is_page(array(94, 240, 1116, 2009, 2013, 2375, 2574, 4506, 4750))){

		$GLOBALS['page_section'] = "gracias";

	}

	if(is_page(array(2267, 2418))){

		$GLOBALS['page_section'] = "gracias/landing";

	}



	if(is_page(822)){

		$GLOBALS['page_section'] = "contacto";

	}







	// #####################

	// ## ERRORES Y OTROS ##

	// #####################

    ///// 404

	else if(is_404()){

		$GLOBALS['page_section'] = $GLOBALS['error_id'] = "404-error";

	}



	///// 410

	if(isset($GLOBALS["410"])){

		$GLOBALS['page_section'] = $GLOBALS['error_id'] = "410-error";

	}



	$GLOBALS['page_section'] = ($GLOBALS['page_section']!="") ? $GLOBALS['page_section'] : get_field('page_section') ;

	$GLOBALS['page_type'] = ($GLOBALS['page_type']!="") ? $GLOBALS['page_type'] : get_field('page_type') ;







	// function datos_curso(){



	// 	$slug = get_post_field( 'post_name', get_post() );

	// 	$cat_slug = "undefined";

	// 	$terms = wp_get_post_terms( get_the_ID(), 'categorias_formacion', array());

	// 	if($terms){

	// 		foreach ($terms as $x) {

	// 			$cat_slug = $x->slug;

	// 		}

	// 	}

	// 	//



	// 	$datos_curso["cat_curso"] = $cat_slug;

	// 	//$datos_curso["id_curso"] = get_field("id");

	// 	//$datos_curso["nombre_curso"] = $slug;



	// 	return $datos_curso;





	// }



?>



<script>

	/*

		MLL default

		web: 3234

		landings: 3307

	*/

	if(!sessionStorage.getItem("dl_mll")){

		if(extraeParametrosURL('MLL')){

			sessionStorage.setItem("dl_mll", extraeParametrosURL('MLL'));

		}else if(extraeParametrosURL('modo_llegada')){

			sessionStorage.setItem("dl_mll", extraeParametrosURL('modo_llegada'));

		} else{

			if(window.location.href.includes("/landings/")) {

				sessionStorage.setItem("dl_mll", 3307);

			} else if(window.location.href.includes("/landing-elementor/")) {

				sessionStorage.setItem("dl_mll", 3307);

			} else {

				sessionStorage.setItem("dl_mll", 3234);

			}

		}

	}



	if(!sessionStorage.getItem("dl_gclid")){

		if(extraeParametrosURL('gclid')){

			sessionStorage.setItem("dl_gclid", extraeParametrosURL('gclid'));

		}

	}



	var user_id = "";

	if(document.cookie.split("idgacampus=")[1]!=undefined){

		user_id = document.cookie.split("idgacampus=")[1].split(";")[0];

	}





	//var gclid = sessionStorage.getItem("dl_gclid"); gclid  = (gclid  != "" || !gclid) ? gclid  : undefined;

	var gclid = sessionStorage.getItem("dl_gclid"); gclid  = (gclid === null) ? undefined  : gclid;

	// var mll = sessionStorage.getItem("dl_mll"); mll  = (mll  != "") ? mll  : undefined;

	var mll = sessionStorage.getItem("dl_mll"); mll  = (mll  === null) ? undefined  : mll;

	user_id  = (user_id  != "") ? user_id  : undefined;

	var user_ip = "<?php echo $GLOBALS['user_ip']; ?>"; user_ip  = (user_ip  != "") ? user_ip  : undefined;



	//Blog

	var blog_author = "<?php echo $GLOBALS['blog_author']; ?>"; blog_author  = (blog_author  != "") ? blog_author  : undefined;

	var blog_cat = "<?php echo $GLOBALS['blog_cat']; ?>"; blog_cat  = (blog_cat  != "") ? blog_cat  : undefined;

	var blog_date = "<?php echo $GLOBALS['blog_date']; ?>"; blog_date  = (blog_date  != "") ? blog_date  : undefined;

	var blog_tag = "<?php echo $GLOBALS['blog_tag']; ?>"; blog_tag  = (blog_tag  != "") ? blog_tag  : undefined;



	//Página

	var page_section = "<?php echo $GLOBALS['page_section']; ?>"; page_section = (page_section != "") ? page_section : undefined;

	var page_title = "<?php echo $GLOBALS['page_title']; ?>"; page_title = (page_title != "") ? page_title : undefined;

	var page_type = "<?php echo $GLOBALS['page_type']; ?>"; page_type = (page_type != "") ? page_type : undefined;

	// var page_url = window.location.hostname + window.location.pathname; page_url = (page_url != "") ? page_url : undefined;

	var ruta_url = (window.location.pathname.substr(-1)=="/") ? window.location.pathname : window.location.pathname + "/";

	var page_url = window.location.hostname.replace("www.","") + ruta_url + window.location.hash.split("?")[0]; page_url = (page_url != "") ? page_url : undefined;



	//Producto

	var product_category = "<?php echo $GLOBALS['product_category']; ?>"; product_category = (product_category != "") ? product_category : undefined;

	var product_id = "<?php echo $GLOBALS['product_id']; ?>"; product_id = (product_id != "") ? product_id : undefined;

	var product_name = "<?php echo $GLOBALS['product_name']; ?>"; product_name = (product_name != "") ? product_name : undefined;

	var product_type = "<?php echo $GLOBALS['product_type']; ?>"; product_type = (product_type != "") ? product_type : undefined;



	//Varios

	var error_id = "<?php echo $GLOBALS['error_id']; ?>"; error_id  = (error_id  != "") ? error_id  : undefined;

	var test_id = "<?php echo $GLOBALS['test_id']; ?>"; test_id  = (test_id  != "") ? test_id  : undefined;

	var slots = "<?php echo $GLOBALS['slots']; ?>"; slots  = (slots  != "") ? slots  : undefined;



	//Otros



	//Slots

	var pagepath = window.location.pathname.substring(1, window.location.pathname.length-1);

	slots = pagepath.split("/");



    //DataLayer

    var gtm_var = {

        "gclid": gclid,

        "mll": mll,

        "user_id": user_id,

        "user_ip": user_ip,

        "blog_author": blog_author,

        "blog_cat": blog_cat,

        "blog_date": blog_date,

        "blog_tag": blog_tag,

        "page_section": page_section,

        "page_title": page_title,

        "page_type": page_type,

        "page_url": page_url,

        "product_category": product_category,

        "product_id": product_id,

        "product_name": product_name,

        "product_type": product_type,

        "error_id": error_id,

        "test_id": test_id,

        "slots": slots,

    };



	// dataLayer = [{

	window.dataLayer = window.dataLayer || [];

	window.dataLayer.push({

		'Host':	window.location.hostname,//host

		'Pathname':	window.location.pathname,//Pathname

		'Protocol':	window.location.protocol.slice(0,-1),//Protocolo de seguridad

		'Language':	'Spanish',//Idioma -> este lo doy por asumido al no tener plugin multi idioma

		'Referrer':	document.referrer,//Referencia

		'readyState': 'Complete',//Estado de carga -> este lo doy por asumido al estar en el document ready

		'ClientID': gtm_var.user_id, // client ID de Analytics

		'mll': gtm_var.mll, // Modo de llegada a la web

		'gclid': gtm_var.gclid, // Identificador de click de google ADs

		'Slots': gtm_var.slots,

		'Blog': [{

			'Autor': gtm_var.blog_author, //Autor de la noticia

			'BlogCat': gtm_var.blog_cat, // Categorias a la que pertenece la noticia

			'date': gtm_var.blog_date, // Fecha de publicacion de la noticia

			'tags': gtm_var.blog_tag, // Tags asociados a la noticia

		}],

		'Curso': [{

			'category': gtm_var.product_category, //Categoria a la que pertenece la pagina de curso

			'id': gtm_var.product_id, // Numero id correspondiente al curso

			'name': gtm_var.product_name, // Nombre del curso

			'type': gtm_var.product_type, // Tipo de curso

		}],

		'error': [{

			'Id': gtm_var.error_id, //Variable que recoge el código de error

		}],

		'page': [{

			'PageURL': gtm_var.page_url, //Url de la pagina

			'pageTitle': gtm_var.page_title, //Título de la pagina

			'section': gtm_var.page_type, //Sección a la que pertenece la pagina (formación, noticias, empresa...)

			'type': gtm_var.page_section, //Tipo de pagina (detalle curso, noticia...)

		}],

		'test': [{

			'info': gtm_var.test_id, //Identificador deltest que se está realizando

		}],

	});

	// }];



	function extraeParametrosURL(name) { //Recuperar el modo de llegada

		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");

		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),

			results = regex.exec(location.search);

		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));

	}

</script>