<?php

function registrar_custom_post() {

	//Formación
	$labels = array(
		'name'  => _x( 'Formacion', 'post type general name', 'esgalla' )
	);

	$args = array(
	'labels' => $labels,
	'description' => __( 'Formacion Tokio.', 'esgalla' ),
	'public'  => true,
	'publicly_queryable' => true,
	'show_ui'  => true,
	'show_in_menu'  => true,
	'query_var'  => true,
	'rewrite'  => array( 'slug' => 'formaciones' ),
	'capability_type'   => 'page',
	'has_archive' => true,
	'hierarchical' => true,
	'menu_position' => null,
	'supports' => array( 'title', 'editor', 'thumbnail', 'page-atributes'),
	'menu_position' => 5
	);

	register_post_type( 'formacion', $args );



	//Landing elementor
    $labels = array(
        'name'  => _x( 'Landing Elementor', 'post type general name', 'your-plugin-textdomain' )
    );

    $args = array(
    'labels' => $labels,
    'description' => __( 'Description.', 'your-plugin-textdomain' ),
    'public'  => true,
    'publicly_queryable' => true,
    'show_ui'  => true,
    'show_in_menu'  => true,
    'query_var'  => true,
    'rewrite'  => array( 'slug' => 'landing-elementor' ),
    'capability_type'   => 'page',
    'has_archive' => false,
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array( 'title', 'editor', 'thumbnail', 'page-atributes'),
    'menu_position' => 5
    );

    register_post_type( 'landing-elementor', $args );


	//Profesores
	$labels = array(
		'name'  => _x( 'Profesores', 'post type general name', 'esgalla' )
	);

	$args = array(
	'labels' => $labels,
	'description' => __( 'Profesores Tokio.', 'esgalla' ),
	'public'  => true,
	'publicly_queryable' => true,
	'show_ui'  => true,
	'show_in_menu'  => true,
	'query_var'  => true,
	'rewrite'  => array( 'slug' => 'profesor' ),
	'capability_type'   => 'page',
	'has_archive' => true,
	'hierarchical' => true,
	'exclude_from_search' => true,
	'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes'),
	'menu_position' => 5,
	'menu_icon' => 'dashicons-welcome-learn-more'
	);

	register_post_type( 'profesor', $args );


	//Testimonios
	$labels = array(
		'name'  => _x( 'Testimonios', 'post type general name', 'esgalla' )
	);

	$args = array(
	'labels' => $labels,
	'description' => __( 'Testimonios Tokio.', 'esgalla' ),
	'public'  => true,
	'publicly_queryable' => true,
	'show_ui'  => true,
	'show_in_menu'  => true,
	'query_var'  => true,
	'rewrite'  => array( 'slug' => 'testimonio' ),
	'capability_type'   => 'page',
	'has_archive' => true,
	'hierarchical' => true,
	'exclude_from_search' => true,
	'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes'),
	'menu_position' => 5,
	'menu_icon' => 'dashicons-format-chat'
	);

	register_post_type( 'testimonio', $args );


}
add_action( 'init', 'registrar_custom_post');


function registrar_taxonomias() {

	$labels = array(
			'name' => _x( 'Categorías Formación', 'taxonomy general name', 'textdomain' ),
	);
	$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'cursos-sector' ),
	);

	register_taxonomy( 'categorias_formacion', array( 'formacion'), $args );


	$labels = array(
			'name' => _x( 'Etiquetas Formación', 'taxonomy general name', 'textdomain' ),
	);
	$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'cursos-tag' ),
	);

	register_taxonomy( 'tag_formacion', array( 'formacion'), $args );


	$labels = array(
			'name' => _x( 'Categorías Profesores', 'taxonomy general name', 'esgalla' ),
	);
	$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'profesores-categoria' ),
	);

	register_taxonomy( 'categorias_profesor', array( 'profesor'), $args );


	$labels = array(
			'name' => _x( 'Categorías Testimonios', 'taxonomy general name', 'esgalla' ),
	);
	$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'testimonios-categoria' ),
	);

	register_taxonomy( 'categorias_testimonio', array( 'testimonio'), $args );


}
add_action( 'init', 'registrar_taxonomias', 0 );
