<?php
/**
* Template Name: Mapa Sitio
*
* @package esgalla
*/
get_header();
get_template_part("template-parts/tema", "header");

?>
<div class="container-fluid  bg-tokio-navyblue">
  <div class="container aviso-legal-content  py-5 py-md-6">
    <div class="row justify-content-md-center">
      <div class="col-md-6 col-md-offset-3">
        <h1 class="primary-text text-white titulo-legal">
          <? the_title(); ?>
        </h1>
        <div class="separador w-100"></div>
      </div>
    </div>

    <div class="row justify-content-md-center py-5 py-md-6">
      <div class="col-md-6 col-md-offset-3">
        <?php echo do_shortcode("[simple-sitemap exclude='3900,3536,3567']"); ?>
        <h3 class="post-type" style="font-size:1em;">Categorías formaciones</h3>
        <?php echo do_shortcode('[simple-sitemap-tax taxonomy="categorias_formacion"]'); ?>

        <?php echo do_shortcode('[simple-sitemap types="formacion" tax="categorias_formacion" ]'); ?>

        <?php echo do_shortcode('[simple-sitemap-group]'); ?>
      </div>
    </div>
  </div>
</div>


<?php



get_footer();
?>

