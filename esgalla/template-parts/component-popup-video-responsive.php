<?php

  // Component variables - Opinion alumno

  $video_url = get_query_var('video_url');

  $video_descripcion = get_query_var('video_descripcion');

?>

<?php if($video_url): ?>

  <div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" width="560" height="315" src="<?php echo get_sub_field('id_youtube'); ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>

<?php endif; ?>

<?php if($video_descripcion): ?>

  <div class="container-fluid bg-tokio-green text-white video-description">

    <div class="container bg-green-pico-top-left py-4 py-lg-5">

      <p class="text-white h5 mb-0"><?php echo $video_descripcion; ?></p>

    </div>

  </div>

<?php endif; ?>