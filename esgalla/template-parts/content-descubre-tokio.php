<?php

/**

 * Template part for displaying page content in page-home.php

 *

 * @package esgalla

 */



?>



<header id="masthead" class="site-header fullheight  descubre-tokio py-3 py-md-5">



		<div class="container-fluid full-height-container h-100 pb-5 pb-md-4 pt-4 pt-md-5">

			<div class="row row-descubre-tokio align-items-center">



				<div class="col-12 text-center" >

					<div class="">

						<img src="/wp-content/themes/esgalla/img/we-are-tokio.png" alt="We Are Tokio | New Technology School" class="img-fluid wow animate__fadeInUp" data-wow-duration="2s"/>
						<h1 class="d-none"><? echo get_the_title(); ?></h1>

					</div>

				</div>

			</div>



		</div>





</header><!-- #masthead -->





<section id="que-es-tokio-school" class="bg-white">

	<div class="container py-6 py-md-6">



		<div class="row align-items-center">

			<div class="col-md-6 pr-md-5">



				<h2 class="text-tokio-navyblue letras-chinas-1 mb-4 mb-md-5 wow animate__fadeInUp" data-wow-duration="2s">Diz konnichiwa à Tokio School</h2>

        <p class="text-tokio-black mb-4">Vamos às apresentações: somos a Tokio School, <strong>escola online com expert em tecnologia. </strong> Procuramos relacionamentos sérios com tech lovers e digital drivers, profissionais prontos para liderar a evolução do mundo digital.</p>

        <p class="text-tokio-black mb-4">Esta é uma área em contínua atualização, e nós também: estamos em constante auto-refresh e acreditamos no learn by doing. Os nossos programas formativos adaptam-se e evoluem, para estarmos sempre em movimento, sempre em frente – é esse o nosso motto,<i>#alwaysfoward</i>.</p>
		<p class="text-tokio-black mb-4"> Somos Tokio porque temos a tecnologia no sangue. Também tens?</p>
			</div>

			<div class="col-md-6 text-center">



				<img src="<?php echo get_template_directory_uri() ?>/img/descubre_tokio_persona.png" class="img-fluid border border-4 border-tokio-pink rounded-circle p-3"/>



			</div>

		</div>



	</div>

</section>





<section id="como-lo-hacemos" class="">

	<div class="container py-5 py-md-6">



		<div class="row align-items-center">



			<div class="col-md-6 order-md-last">



				<h2 class="text-tokio-navyblue letras-chinas-2 mb-4 mb-md-5 wow animate__fadeInUp" data-wow-duration="2s">Sayonara à formação convencional!</h2>

				<!-- <img src="<?php echo get_template_directory_uri() ?>/img/descubre_tokio_persona2.png" class="d-md-none img-fluid border border-4 border-tokio-pink rounded-circle p-3 mb-4"/> -->

				<div id="accordion-como-lo-hacemos" class="accordion custom-accordion d-none d-md-block">

					<div class="card border-0 bg-white mb-0">

						<div class="accordion-item active">

							<div class="card-header border-0 bg-white pl-4 pt-4" data-toggle="collapse" href="#collapseOne2" aria-expanded="true">

								<a class="card-title h5 text-tokio-navyblue"> 100% online </a>

							</div>

							<div id="collapseOne2" class="p-0 pl-1 collapse show" data-parent="#accordion-como-lo-hacemos">

								<div class="card-body pt-2">A nossa formação adapta-se a ti, aos teus horários, ao teu ritmo e a eventualidades. Com a nossa <strong>metodologia de e-learning</strong>, tu decides quando e como avançar.</div>

							</div>

						</div>

						<div class="accordion-item">

							<div class="card-header border-0 bg-white collapsed pl-4 pt-4" data-toggle="collapse" data-parent="#accordion-como-lo-hacemos" href="#collapseTwo2">

								<a class="card-title h5 text-tokio-navyblue"> Acompanhamento personalizado </a>

							</div>

							<div id="collapseTwo2" class="collapse p-0 pl-1" data-parent="#accordion-como-lo-hacemos">

								<div class="card-body pt-2">Os nossos <strong>tutores pedagógicos</strong> estão à tua disposição para te dar apoio e aquele boost de motivação, e os professores especialistas, os nossos senseis, vão partilhar contigo todo o seu conhecimento.</div>

							</div>

						</div>

						<div class="accordion-item">

							<div class="card-header border-0 bg-white collapsed pl-4 pt-4" data-toggle="collapse" data-parent="#accordion-como-lo-hacemos" href="#collapseThree2">

								<a class="card-title h5 text-tokio-navyblue"> Learn by doing </a>

							</div>

							<div id="collapseThree2" class="collapse p-0 pl-1" data-parent="#accordion-como-lo-hacemos">

								<div class="card-body pt-2">Aprendes melhor com as mãos na massa. Além da formação prática tens <strong>até 300 horas de estágios profissionais</strong> em empresas de referencia na área.</div>

							</div>

						</div>

						<div class="accordion-item">

							<div class="card-header border-0 bg-white collapsed pl-4 pt-4" data-toggle="collapse" data-parent="#accordion-como-lo-hacemos" href="#collapseFour2">

								<a class="card-title h5 text-tokio-navyblue"> English is the key </a>

							</div>

							<div id="collapseFour2" class="collapse p-0 pl-1" data-parent="#accordion-como-lo-hacemos">

								<div class="card-body pt-2">Queres passar para o próximo nível?  <strong>Ajudamos-te a obter a tua certificação de Inglês</strong>. É um extra incluído em todas as nossas formações.
</div>

							</div>

						</div>

					</div>

				</div>

				<div id="slider-como-lo-hacemos" class="d-md-none mb-5">

					<div class="" data-flickity='{ "contain": true, "pageDots": false }'>

						<div class="carousel-cell">

							<h3 class="h5 text-tokio-navyblue">100% online</h3>

							<p class="text-tokio-navyblue">A nossa formação adapta-se a ti, aos teus horários, ao teu ritmo e eventualidades. Com a nossa <strong>metodologia de e-learning,</strong> tu decides quando e como avançar.</p>

						</div>

						<div class="carousel-cell">

							<h3 class="h5 text-tokio-navyblue">Acompanhamento personalizado </h3>

							<p class="text-tokio-navyblue">Os nossos <strong>tutores pedagógicos</strong> estão à tua disposição para te dar apoio e aquele boost de motivação, e os professores especialistas, os nossos senseis, vão partilhar contigo todo o seu conhecimento.</p>

						</div>

						<div class="carousel-cell">

							<h3 class="h5 text-tokio-navyblue">Learning by doing</h3>

							<p class="text-tokio-navyblue">Aprendes melhor com as mãos na massa. Além da formação prática tens <strong>até 300 horas de estágios profissionais de qualidade</strong> em empresas da área.</p>

						</div>

						<div class="carousel-cell">

							<h3 class="h5 text-tokio-navyblue">English is the key</h3>

							<p class="text-tokio-navyblue">>Queres passar para o próximo nível?  <strong>Ajudamos-te a obter a tua certificação de Inglês</strong>. É um extra incluído em todas as nossas formações.</p>

						</div>

					</div>

				</div>



			</div>

			<div class="text-center col-md-6 mb-4">

				<img src="<?php echo get_template_directory_uri() ?>/img/descubre_tokio_persona2.png" class="img-fluid border border-4 border-tokio-pink rounded-circle p-3"/>

			</div>



		</div>



	</div>

</section>





<section id="videoyt" class="bg-white">

	<div class="container-fluid px-0">

		<div class="row">

			<div class="col-12 text-center d-none d-md-block">

				<?php

					$video =  [

						'video_url' => get_sub_field('id_youtube'),

						'video_descripcion' => (get_field('video')['descripcion']) ? get_field('video')['descripcion'] : 'omos a escola digital dos samurais tecnológicos, daqueles que querem estar #alwaysforward'

					];

				?>

				<?php ec_get_template_part('template-parts/components/component', 'popup-video', $video); ?>

			</div>

			<div class="col-12 text-center d-md-none">

				<?php ec_get_template_part('template-parts/components/component', 'popup-video-responsive', $video); ?>


			</div>

		</div>

	</div>

</section>



<section id="formacion-online" class="">

	<div class="container py-5 py-md-6">

		<div class="row mb-md-5">

			<div class="col-md-9 mx-md-auto text-md-center">

				<h2 class="text-tokio-navyblue mb-4 mb-md-5 wow animate__fadeInUp" data-wow-duration="2s">Vantagens de estudar na Tokio School</h2>

			</div>

		</div>

		<div class="row d-none d-md-flex">

			<div class="col-md-4 flex-column align-items-center text-center mb-5 px-md-4">

				<div class="bg-white h-100 metodologia-card px-4 pt-4 pb-4">

					<img src="<?php echo get_template_directory_uri() ?>/img/formacion_alwaysforward.svg" class="img-fluid mb-3"/>

					<h3 class="h4 text-tokio-navyblue mb-3">Formação <i>#alwaysfoward</i></h3>

					<p class="mb-3 px-2 px-md-2">Estamos em contínua atualização para que os nossos alunos se transformem em autênticos líderes digitais.</p>

				</div>

			</div>



			<div class="col-md-4 flex-column align-items-center text-center mb-5 px-md-4">

				<div class="bg-white h-100 metodologia-card px-4 pt-4 pb-4">

					<img src="<?php echo get_template_directory_uri() ?>/img/youre_not_alone.svg" class="img-fluid mb-3"/>

					<h3 class="h4 text-tokio-navyblue mb-3">Apoio personalizado</h3>

					<p class="mb-3 px-2 px-md-2">Durante o curso terás um tutor que acompanhará o teu percurso e professores que estarão ao teu lado para esclarecer todas  as tuas dúvidas.</p>

				</div>

			</div>

			

			<div class="col-md-4 flex-column align-items-center text-center mb-5 px-md-4">

				<div class="bg-white h-100 metodologia-card px-4 pt-4 pb-4">

					<img src="<?php echo get_template_directory_uri() ?>/img/certificaciones_oficiales.svg" class="img-fluid mb-3"/>

					<h3 class="h4 text-tokio-navyblue mb-3">Certificações oficiais</h3>

					<p class="mb-3 px-2 px-md-2">Somos certificados pela DGERT – Símbolo de Qualidade na Formação e Credibilidade no Mercado de Trabalho. Preparamos-te também para que obtenhas as certificações Oracle e Python.</p>

				</div>

			</div>



		</div>

		<div class="row d-md-none">

			<div class="carousel carousel-metodologia-mobile" data-flickity='{ "imagesLoaded": false, "prevNextButtons": true, "pageDots": false }'>

				<div class="carousel-cell">

					<div class="col-md-4 flex-column align-items-center text-center px-md-4">

						<div class="bg-white h-100 metodologia-card px-4 pt-4 pb-4">

							<img src="<?php echo get_template_directory_uri() ?>/img/formacion_alwaysforward.svg" class="img-fluid mb-3"/>

							<div class="h4 text-tokio-navyblue mb-3">Formação  <i>#alwaysfoward</i></div>

							<p class="mb-3 px-2 px-md-2">Estamos em contínua atualização para que os nossos alunos se transformem em autênticos líderes digitais.</p>

						</div>

					</div>
				</div>

				<div class="carousel-cell">

					<div class="col-md-4 flex-column align-items-center text-center px-md-4">

						<div class="bg-white h-100 metodologia-card px-4 pt-4 pb-4">

							<img src="<?php echo get_template_directory_uri() ?>/img/youre_not_alone.png" class="img-fluid mb-3"/>

							<div class="h4 text-tokio-navyblue mb-3">Atenção personalizada</div>

							<p class="mb-3 px-2 px-md-2">Durante o curso terás um tutor que acompanhará o teu percurso e professores que estarão ao teu lado para esclarecer todas  as tuas dúvidas.</p>

						</div>

					</div>

				</div>

				<div class="carousel-cell">

					<div class="col-md-4 flex-column align-items-center text-center px-md-4">

						<div class="bg-white h-100 metodologia-card px-4 pt-4 pb-4">

							<img src="<?php echo get_template_directory_uri() ?>/img/certificaciones_oficiales.png" class="img-fluid mb-3"/>

							<div class="h4 text-tokio-navyblue mb-3">Certificações oficiais</div>

							<p class="mb-3 px-2 px-md-2">Somos entidade certificada pela pela DGERT – Símbolo de Qualidade na Formação e Credibilidade no Mercado de Trabalho. Também te preparamos para que obtenhas as certificações da Oracle e Python. </p>

						</div>

					</div>

				</div>

			</div>

		</div>

		<!-- <div class="row">

			<div class="col-12 text-center">

				<a class="btn btn-tokio-navyblue rounded-pill d-block d-md-inline-block py-3 px-4 mt-3" href="https://www.tokioschool.com/descubre-tokio/">Ver metodología</a>

			</div>

		</div> -->



	</div>

</section>





<?php get_template_part('template-parts/blocks/block', 'speak-english'); ?>





<?php //get_template_part('template-parts/blocks/block', 'mejores-espana'); ?>





<section id="trabaja-con-nosotros" class="">

	<div class="container pt-5 pt-md-5 mt-4 mt-md-6">

		<?php get_template_part('template-parts/blocks/block', 'medallas'); ?>

		<div class="row align-items-center">

			<div class="col-md-5 text-center text-lg-left bg-image-circle-left order-md-2 d-none d-md-block">

				<img src="<?php echo get_template_directory_uri() ?>/img/letras_chinas_3.svg" class="img-fluid"/>

			</div>

			<div class="col-md-7 order-md-last text-left mt-4 mt-md-0 form-contacto">

				<h2 class="text-tokio-navyblue mb-4 wow animate__fadeInUp" data-wow-duration="2s">Bem-vindo à Tokio!</h2>

				<p class="text-tokio-black mb-4 pb-2">Gostavas de trabalhar na nossa escola? Fácil! Escreve-nos uma mensagem sobre ti a tua experiência. Adorávamos conhecer-te!</p>

				<?php echo do_shortcode( '[gravityform id="3" title="false" description="false" ajax="true" tabindex="500"]' ); ?>

			</div>

		</div>



	</div>

</section>

