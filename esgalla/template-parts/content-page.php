<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package esgalla
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header header-page mb-5" style="background-image: url('<?php echo the_post_thumbnail_url( 'large' ); ?>'); ">
		<div class="background-overlay"></div>
		<div class="container d-flex align-items-center text-center">
			<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
		</div>
	</header><!-- .entry-header -->

	<div class="container">

		<div class="entry-content">
				<?php
					the_content();
				?>
		</div>

	</div><!-- container -->
		
</article><!-- #post-<?php // the_ID(); ?> -->
