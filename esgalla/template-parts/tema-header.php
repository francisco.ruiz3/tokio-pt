<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'esgalla' ); ?></a>

	<!-- <div class="topbar topbar-first bg-secondary text-white border-bottom text-center px-1 py-2 py-md-4">
		<span>20% de descuento para las primeras 10 inscripciones en el curso de Data. <strong>Más información 🚀</strong></span>
	</div> -->

	<div class="topbar topbar-second bg-secondary d-none d-lg-block py-2 py-md-3">
		<div class="container d-flex justify-content-between">
			<div class="d-flex align-items-center">
				<?php echo buscador(); ?> <span class="topbar-separator">|</span> <a href="<?php echo get_home_url( )?>/noticias/" class="fs-14 font-weight-bold">Back Up Blog</a>
			</div>
			<div class="d-flex align-items-center"><a class="text-tokio-black font-weight-bold fs-14" href="<?php echo get_home_url( )?>/contacto/">Contacto</a><span class="topbar-separator">|</span><a class="font-weight-bold fs-14" href="tel:+211-654-494"><img class="icon-phone" src="https://www.tokioschool.com/wp-content/themes/esgalla/img/phone.svg">211-654-494</a></div>
		</div>
	</div>

	<?php get_template_part( 'template-parts/navbar', 'fixed' ); ?>