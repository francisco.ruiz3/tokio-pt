<section id="speak-english" class="bg-tokio-navyblue">
	<div class="container py-5 py-md-6">
		<div class="row">
			<div class="col-6 col-md-5 order-md-2 text-lg-right mb-5 align-self-center">
				<img src="<?php echo get_template_directory_uri() ?>/img/tokio_pink.svg" class="img-fluid bg-grey"/>
			</div>
			<div class="col-md-7 pt-md-4 pr-lg-5">

				<h2 class="text-tokio-green mb-4 mt-md-5 wow animate__fadeInUp" data-wow-duration="2s">Do you speak english?</h2>
				<p class="text-tokio-lightgrey titilumsemibold mb-5">Falar a língua universal é fundamental para que aprendas qualquer profissão tecnológica. Na Tokio School, <strong>vais começar a dominar o Inglês</strong> ou melhorar o teu nível para obteres uma certificação oficial. Este é um <strong>plus incluído em todas as nossas formações</strong>. Let’s go!</p>
				<div class="row justify-content-between">
					<div class="col-lg-6 mb-4 pr-2">
						<div class="media">
							<div class="rounded bg-light p-2 mr-3 my-auto"><img src="<?php echo get_template_directory_uri() ?>/img/escudo.svg" class="raw-svg img-fluid"/></div>
							<div class="media-body pt-1">
								<div class="h6 text-secondary mt-0 mb-0">Qualificação Cambridge English</div>
								<span class="text-tokio-lightgrey titilumsemibold font-weight-normal">Centro de preparação</span>
							</div>
						</div>
					</div>
					<div class="col-lg-6 mb-4 pr-2">
						<div class="media">
							<div class="rounded bg-light p-2 mr-3 my-auto"><img src="<?php echo get_template_directory_uri() ?>/img/medalla.svg" class="raw-svg img-fluid"/></div>
							<div class="media-body pt-1">
								<div class="h6 text-secondary mt-0 mb-0">Oxford University Press</div>
								<span class="text-tokio-lightgrey titilumsemibold font-weight-normal">Centro de examinação</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
