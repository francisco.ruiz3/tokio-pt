<div class="row mb-5">
  <div class="col-md-8 mx-auto text-center">
    <h2 class="text-tokio-navyblue wow animate__fadeInUp" data-wow-duration="2s">Na Tokio não somos de colocar medalhas...mas, se nos forem dadas usamos com orgulho!</h2>
  </div>
</div>
<div class="row justify-content-md-center mb-6">
  <div class="col-md-2 col-lg-2 text-center p-5 p-md-3">
    <img loading="lazy" decoding="async" width="318" height="350" src="<?php echo wp_get_attachment_url( 4313 ) ?>" class="img-fluid">
  </div>
  <div class="col-md-2 col-lg-2 text-center p-5 p-md-3">
    <img loading="lazy" decoding="async" width="422" height="380" src="<?php echo wp_get_attachment_url( 4319 ) ?>" class="img-fluid">
  </div>
  <div class="col-md-2 col-lg-2 text-center p-5 p-md-3">
    <img loading="lazy" decoding="async" width="422" height="380" src="<?php echo wp_get_attachment_url( 4320 ) ?>" class="img-fluid">
  </div>
  <div class="col-md-2 col-lg-2 text-center p-5 p-md-3">
    <img loading="lazy" decoding="async" width="422" height="380" src="<?php echo wp_get_attachment_url( 4321 ) ?>" class="img-fluid">
  </div>
  <div class="col-md-2 col-lg-2 text-center p-5 p-md-3">
    <img loading="lazy" decoding="async" width="422" height="380" src="<?php echo wp_get_attachment_url( 4322 ) ?>" class="img-fluid">
  </div>
</div>