<?php
$prod_relacionados_conf = wp_parse_args( $args );
// print_r($prod_relacionados_conf);
$productos_en_categoria = get_posts(
	array(
		'posts_per_page' => -1,
		'post_type' => 'formacion',
		'tax_query' => array(
      'relation' => 'AND',
			array(
				'taxonomy' => 'categorias_formacion',
				'field' => 'term_id',
				'terms' => $prod_relacionados_conf['categoria']->term_id,
      ),
      //Excluimos las carreras profesionales de este listado
      array(
        'taxonomy' => 'tag_formacion',
        'terms'    => 'carreras-profesionales',
        'field'    => 'slug',
        'operator' => 'NOT IN',
      )
      )
	)
);
if($productos_en_categoria && count($productos_en_categoria) > 0):
?>
	<div class="container-lg-fluid py-4 py-md-5 pl-lg-0 related-products">
		<div class="container">
			<div class="row">
				<div class="col mb-4 mb-md-5">
					<h2 class="text-tokio-navyblue h2 wow animate__fadeInUp" data-wow-duration="2s"><?php echo $prod_relacionados_conf['titulo'] ?></h2>
				</div>
			</div>

			<div class="row">
				<div class="col pl-lg-0">
					<div class="carousel posts-carousel px-1 py-3" data-flickity='{ "freeScroll": false, "contain": true, "prevNextButtons": false, "pageDots": false }'>
						<!-- <div class="carousel-cell"> -->
							<?php foreach ($productos_en_categoria as $producto) : $producto_id = $producto->ID; ?>
								<div class="carousel-cell">
									<div class="post-card m-2 card formacion-card" style="">
										<div class="card-img-top-container bg-img-holder rounded mx-3 mt-3"><img src="<?php echo get_field('imagen_tarjetas', $producto_id)['sizes']['medium']; ?>" class="img-fluid"/></div>
										<div class="card-body">
											<div class="formacion-badges d-flex my-4">
												<span class="badge badge-tokio-green text-white mr-3 "><img src="<?php echo get_template_directory_uri() ?>/img/badge-award.svg" class="img-fluid"/> <?php echo (get_field('tipo', $producto_id)) ? get_field('tipo', $producto_id) : 'Curso' ?></span>
												<span class="badge badge-tokio-green text-white mr-3 "><img src="<?php echo get_template_directory_uri() ?>/img/badge-timer.svg" class="img-fluid"/> <?php echo (get_field('duracion', $producto_id)) ? get_field('duracion', $producto_id) : '200' ?> h</span>
												<span class="badge badge-tokio-green text-white ml-auto"><?php echo (get_field('modalidad', $producto_id)) ? get_field('modalidad', $producto_id) : 'Online' ?></span>
											</div>
											<h2 class="h5 card-title text-primary"><?php echo get_field('cabecera', $producto_id)["titulo_cabecera"] ?></h2>
											<p class="card-text"><?php echo get_field('descripcion_tarjetas', $producto_id); ?></p>
											<a href="<?php echo get_the_permalink( $producto_id ) ?>" class="stretched-link"></a>
										</div>
									</div>
								</div>
							<? endforeach; ?>

						<!-- </div> -->
					</div>
				</div>
			</div>


		</div>
	</div>
<? endif;
wp_reset_query(); ?>