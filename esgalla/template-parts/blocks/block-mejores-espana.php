<section id="mejores-españa" class="bg-light p-4 p-lg-6 mt-md-6">
	<h2 class="container d-md-none my-4 px-0">Na Tokio não somos de colocar medalhas...mas se nos forem dadas, usamos com orgulho!</h2>
	<div class="container bg-white px-md-5 py-5 py-md-6 rounded">
		<div class="row">
			<div class="col-lg-7 mx-lg-auto text-lg-center">

				<h2 class="d-none d-md-block mb-6">Na Tokio não somos de colocar medalhas...mas se nos forem dadas, usamos com orgulho!</h2>
				<!-- <p class="mb-5">Esse esse occaecat ea amet culpa eiusmod in Lorem fugiat. Elit voluptate et labore laboris non aute laboris veniam eiusmod incididunt tempor nisi. In fugiat officia sunt excepteur voluptate consectetur officia voluptate dolor non.</p> -->

			</div>
		</div>
		<div class="row fila-premios">
			<div class="col-lg-4 col-premios col-xl-3 mx-auto d-flex flex-column mb-4 px-2 px-md-0">
				<img src="<?php echo get_template_directory_uri() ?>/img/premio.svg" class="raw-svg img-fluid mb-3 align-self-start"/>
				<h4 class="mb-2">Mejor innovación en centro educativo online</h4>
				<p class="mt-auto">Premios excelencia educativa 2019</p>
			</div>
			<div class="col-lg-4 col-premios col-xl-3 mx-auto d-flex flex-column mb-4 px-2 px-md-0">
				<img src="<?php echo get_template_directory_uri() ?>/img/premio.svg" class="raw-svg img-fluid mb-3 align-self-start"/>
				<h4 class="mb-2">Mejor centro de formación online</h4>
				<p class="mt-auto">Premios excelencia educativa 2019</p>
			</div>
			<div class="col-lg-4 col-premios col-xl-3 mx-auto d-flex flex-column mb-4 px-2 px-md-0">
				<img src="<?php echo get_template_directory_uri() ?>/img/premio.svg" class="raw-svg img-fluid mb-3 align-self-start"/>
				<h4 class="mb-2">Premio Platino al mejor grupo de formación</h4>
				<p class="mt-auto">Premios excelencia educativa 2019</p>
			</div>
		</div>

	</div>
</section>
