<div class="container-fluid bg-tokio-pink py-5 py-lg-6">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <h2 class="text-white mb-4">Sukiru: habilidades para samuráis digitales</h2>
        <div class="h4 text-tokio-navyblue titilumregular mb-4">Curso complementario y free con tu formación</div>
        <p class="text-white titilumregular">Sukiru significa habilidad en japonés y eso es lo que fomentamos en nuestra escuela: que nuestros alumnos se hagan con todas las habilidades necesarias para triunfar ahí fuera.</p>
        <p class="text-white titilumregular mb-4">Con tu formación disfrutarás de <span class="text-tokio-navyblue font-weight-bold">nuestro curso complementario y totalmente gratuito</span> para tokiers que lo quieren to-do.</p>
        <a class="btn btn-tokio-navyblue btn-mov-100 text-white rounded-pill py-3 px-4" href="<? echo get_permalink(4216); ?>">Tell me more</a>
      </div>
      <div class="col-lg-6 text-center px-lg-5 d-flex align-items-center">
        <img src="/wp-content/themes/esgalla/img/centro-autorizado-scrum.png" class="img-fluid">
      </div>
    </div>
  </div>
</div>