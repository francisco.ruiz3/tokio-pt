<?php
$post_relacionados_conf = wp_parse_args( $args );
// print_r($post_relacionados_conf);
?>
<section id="posts-relacionados">
	<div class="container-lg-fluid py-4 py-md-5 pl-lg-0">
		<div class="container">
			<h2 class="text-tokio-navyblue mb-4 mb-md-5 wow animate__fadeInUp" data-wow-duration="2s"><?php echo $post_relacionados_conf['titulo'] ?></h2>
		</div>
		<div class="row row-carousel">
			<div class="col pl-lg-0">
				<div class="carousel posts-carousel px-1 py-3" data-flickity='{ "freeScroll": false, "contain": true, "prevNextButtons": false, "pageDots": false }'>
					<?php
					// Si la cateogría no es un array (formacion, por ejemplo), buscamos el ID relacionado
					if(!is_array($post_relacionados_conf['categoria'])){
						// En el caso de marketing igualamos al slug que corresponde al ser diferente la categoría de formación con la categoría de entrada
						$post_relacionados_conf['categoria'] = ($post_relacionados_conf['categoria']=='marketing') ? 'marketing-digital' : $post_relacionados_conf['categoria'];
						$post_relacionados_conf['categoria'] = array(get_category_by_slug($post_relacionados_conf['categoria'])->term_id);
					}

					$relacionados = get_posts( array( 'category__in' => implode(", ", $post_relacionados_conf['categoria']), 'numberposts' => $post_relacionados_conf['limite'], 'post__not_in' => $post_relacionados_conf['excluidos'] ) );
					// $relacionados = new WP_Query(array(
					// 	'category_in' => $post_relacionados_conf['categoria'],
					// 	'post__not_in' => $post_relacionados_conf['excluidos'],
					// ));
					// echo $relacionados->request;

					if( $relacionados && $post_relacionados_conf['categoria']!='' ):
						foreach( $relacionados as $post ) :
							$post_card = [
								'post_img' => get_the_post_thumbnail_url( $post->ID, $size = 'medium' ),
								'post_title' => get_the_title( $post->ID ),
								'post_excerpt' => get_the_excerpt( $post->ID ),
								'post_link' => get_the_permalink( $post->ID ),
							];
							echo '<div class="carousel-cell">';
								ec_get_template_part('template-parts/components/component', 'post-card', $post_card);
							echo '</div>';
						endforeach;
					else: ?>
					<? $relacionados = get_posts( array( 'numberposts' => $post_relacionados_conf['limite'], 'post__not_in' => $post_relacionados_conf['excluidos'] ) ); ?>
						<?php
							foreach( $relacionados as $post ) :
								$post_card = [
									'post_img' => get_the_post_thumbnail_url( $post->ID, $size = 'medium' ),
									'post_title' => get_the_title( $post->ID ),
									'post_excerpt' => get_the_excerpt( $post->ID ),
									'post_link' => get_the_permalink( $post->ID ),
								];
								echo '<div class="carousel-cell">';
									ec_get_template_part('template-parts/components/component', 'post-card', $post_card);
								echo '</div>';
							endforeach;
						?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<? wp_reset_query(); ?>
