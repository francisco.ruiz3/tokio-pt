<!-- Cookies desplegable bot -->
<div class="ck_container">
  <div class="desplegable desplegable-lat desplegable-lat-izq" id="popup-cookies-bottom">
    <div class="desplegable-lat-bloque cookies-desplegable">
      <div class="desplegable-lat-bloque-contain">
        <div class="section d-flex d-md-block" id="">
          <div class="w-75">
          <div class="fila ck_fila ck_mb-0 ck_pb-0 ck_mt-0 ck_pt-0">
            <div class="col ck_d-f-jc-fs d-block ck_mb-0 ck_pb-0">
              <p class="ck_titulo-popup ck_font-family">AVISO DE COOKIES</p>
            </div>
          </div>
          <div class="fila ck_fila ck_mb-0 pb-0 ck_pb-0 ck_mt-0 ck_pt-0">
            <div class="w-100 d-f d-f-jc-c">
              <p class="ck_line-height ck_font-size-text ck_font-family ck_mb-0">Usamos ‘cookies’ próprios e de terceiros para fins analíticos e para mostrar-lhe publicidade relacionada com as suas preferências com base nos seus hábitos de navegação e no seu perfil. Pode configurar ou rejeitar ‘cookies’ clicando em 'Configurações de ‘cookies’'. Também pode aceitar todos os ‘cookies’ clicando no botão “Aceitar todos os ‘cookies’”. Para obter mais informações, pode consultar a nossa <a href="<? echo get_home_url() .'/politica-de-cookies/'; ?>" target="_blank" style="text-decoration:underline;">Política de cookies</a></p>
            </div>
          </div>
          </div>
            <div class="w-25 d-g align-bottom d-f-jc-c">
              <div class=" ck_w-100 ck_mb-0 ck_pb-0 ck_mt-0 text-center  d-f-jc-c">
                <button class="btn btn-cookies aceptar-cookies  text-center ck_font-family ck_w-50 mt-3 mt-md-0">ACEITAR TODAS AS COOKIES</button>
              </div>
              <div class="col ck_w-100 ck_mb-0 ck_pb-0 ck_pt-0-mob ck_mt-0 text-center">
                <button class="btn btn-cookies configurar-cookies text-center ck_font-family ck_w-50">CONFIGURAR COOKIES</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--desplegable-lat-bloque-contain-->
    </div>
    
  </div>
  <!-- Cookies desplegable bot -->

  <!-- Lateral config -->
  <div class="desplegable desplegable-config" id="config-cookies">
    <div class="desplegable-bloque background-transparent">
      <div class="desplegable-bloque-contain h-100">
        <div class="section seccion-pref-cookies h-100">
          <div class="ck_column h-100">
            <div class="head-pref-cookies w-100 d-f d-f-ac-c d-f-jc-c">
              <p class="ck_titulo-popup pt-10 px-10 ck_font-family">Configurador de cookies</p>
            </div>
            <div class="d-f d-f-jc-fs col-tabs w-100">
              <div class="w-25 ck-pestanas">
                <ul class="ck-list">
                  <li class="ck-tab borde-privacidad">
                    <a class="ck-pestana borde-pestana anim-scale w-100" href="#">Sua privacidade</a>
                  </li>
                  <li class="ck-tab cookies-configure">
                    <a class="ck-pestana anim-scale w-100" href="#">Cookies</a>
                  </li>
                </ul>
              </div>

              <div class="w-75 ck-tab-text">
                <div ><p class="texto-pestana fs-0-9 pb-15 visible">Quando um site é acessado, ele pode armazenar ou recuperar informações no navegador, principalmente na forma de cookies. Essas informações podem ser sobre o dispositivo utilizado ou sobre as preferências do usuário e são utilizadas principalmente para fazer o site funcionar da maneira ideal. As informações geralmente não identificam o usuário diretamente, mas fornecem uma navegação mais personalizada. O usuário pode então optar por excluir alguns tipos de cookies.</p>
                <p class="texto-pestana fs-0-9 visible">Você pode clicar nos diferentes grupos ou categorias para obter mais informações e alterar as configurações padrão. No entanto, se você bloquear alguns tipos de cookies, sua experiência de uso do site pode ser afetada e também os serviços que podemos oferecer a você.</p>
                </div>
                <div  class="texto-pestana fs-0-9 oculto">
                    <div class="col">
                      <div class="accordion">
                        <div class="row d-f d-f-jc-sb">
                          <div class="ck_w-60 ck_d-f-jc-fs ck_font-family">
                            <div class="ck_w-100"><span class="ck_fs-14 ck_font-weight-bold">‘Cookies’ técnicas</span></div> <a href="#" class="mas-info ck_font-family mt-10">Ver mais</a>
                          </div>
                          <div class="ck_w-30 ck_d-f-jc-fe text-right">
                            <span style="font-size: 12px;font-style: italic;">Obrigatórias</span>
                          </div>
                        </div>
                      </div>
                      <div class="panel">
                        <p class="ck_font-size-text ck_font-family">Estas ‘cookies’ mostram a página adaptada ao idioma do seu dispositivo ou navegador, entre outras configurações</p>
                      </div>
                      <div class="accordion">
                        <div class="row d-f d-f-jc-sb">
                          <div class="ck_w-60 ck_d-f-jc-fs ck_font-family">
                            <div class="ck_w-100"><span class="ck_fs-14 ck_font-weight-bold">‘Cookies’ de analítica</span></div> <a href="#" class="mas-info ck_font-family mt-10">Ver mais</a>
                          </div>
                          <div class="ck_w-30 ck_d-f-jc-fe">
                            <label class="switch">
                              <input class="switch-input" type="checkbox" id="check-cookies-analitica">
                              <span class="switch-label" data-on="Acepto" data-off="Rechazo"></span>
                              <span class="switch-handle"></span>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="panel">
                        <p class="ck_font-size-text ck_font-family">Estas ‘cookies’ recolhem informações sobre a sua navegação na internet — Podemos contar o número de visitantes da página ou os conteúdos.</p>
                      </div>

                      <div class="accordion">
                        <div class="row d-f d-f-jc-sb">
                          <div class="ck_w-60 ck_d-f-jc-fs ck_font-family">
                            <div class="ck_w-100"><span class="ck_fs-14 ck_font-weight-bold">‘Cookies’ de publicidade</span></div> <a href="#" class="mas-info ck_font-family mt-10">Ver mais</a>
                          </div>
                          <div class="ck_w-30 ck_d-f-jc-fe">
                            <label class="switch">
                              <input class="switch-input" type="checkbox" id="check-cookies-publicidad">
                              <span class="switch-label" data-on="Acepto" data-off="Rechazo"></span>
                              <span class="switch-handle"></span>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="panel">
                        <p class="ck_font-size-text ck_font-family">Estas ‘cookies’ recolhem informações sobre os anúncios exibidos para cada utilizador. - Adapte a publicidade ao tipo de dispositivo a partir do qual o utilizador se está a conectar.</p>
                      </div>
                    </div>
                </div>
              </div>
              
            </div>
            <div class="footer-pref-cookies ck_w-100 d-f d-f-jc-sb">
              <button class="btn btn-cookies guardar-cookies ck_font-family">GUARDAR A CONFIGURAÇÃO</button>
              <button class="btn btn-cookies shadow-none aceptar-cookies ck_font-family">ACEITAR TODAS</button>
            </div>
            <!-- Accordions -->
            
            <!-- Accordions -->
            
            
            <!-- <div class="col ck_w-100 ck_mb-0 ck_mt-0 ck_pt-0 ck_pb-0 ck_m-auto text-center">
              <button class="btn btn-cookies rechazar-cookies ck_font-family">RECHAZAR TODAS</button>
            </div> -->
          </div>


        </div>

      </div>
      <!--desplegable-lat-bloque-contain-->
    </div>
  </div>
  <!-- Lateral config -->
  <div class="ck_fondo"></div>
  </div>