<?php

/**

 * Template part for displaying posts

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/

 *

 * @package esgalla

 */



?>



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <section id="formacion-header">

    <div class="container pb-5 pb-md-6">

      <div class="row">

        <div class="col-lg-6">

          <div class="mb-3">

            <span class="formacion-dato mr-3"><img src="<?php echo get_template_directory_uri() ?>/img/award.svg" class="img-fluid"/> Carreira</span>

            <span class="formacion-dato mr-3"><img src="<?php echo get_template_directory_uri() ?>/img/timer.svg" class="img-fluid"/> 600h</span>

          </div>

          <h1 class="h2 text-primary mb-5">Programación de videojuegos y realidad virtual</h1>

          <p class="mb-5">Amet veniam deserunt esse do. Est veniam fugiat nostrud voluptate irure ipsum sint pariatur ex veniam excepteur quis proident culpa. Consequat qui minim velit culpa eu ipsum enim. Elit et velit incididunt ad. Ipsum culpa laboris qui excepteur velit labore. Sit et pariatur do aliquip.</p>

          <div class="d-flex flex-column flex-lg-row mb-5">

            <a class="btn btn-primary mb-3 mb-lg-0 py-3 px-4" href="#">Preinscripción</a>

            <a class="btn text-primary py-3 px-4" href="#"><img src="<?php echo get_template_directory_uri() ?>/img/download.svg" class="img-fluid icon-download"/>Descargar programa</a>

          </div>

        </div>

        <div class="col-lg-6">

          <img src="<?php echo get_template_directory_uri() ?>/img/imagen-producto.jpg" class="rounded shadow img-fluid"/>

        </div>

      </div>

    </div>

  </section>





  <section id="modulos-formacion">

    <div class="container py-5 py-md-6">

      <div class="row">

        <div class="col-12">

          <h2 class="mb-5">¿Qué aprenderás?</h2>

          <p class="mb-6 two-columns-text">

            Officia non est elit pariatur amet occaecat. Exercitation fugiat mollit culpa aliqua incididunt consectetur velit. Enim exercitation mollit amet dolore deserunt quis sint anim culpa do. Culpa commodo pariatur deserunt minim laborum non aliqua voluptate nulla ad veniam nisi. Culpa laboris ipsum id aute deserunt ea irure dolore. Veniam aliquip ex nulla id aliquip officia elit et incididunt. Sunt dolor dolore aliqua commodo voluptate aliqua.Ex minim aliquip duis sit anim tempor dolor commodo. Nulla consectetur exercitation occaecat velit ad aute cillum duis exercitation ipsum magna. Dolore anim magna dolore Lorem voluptate dolore voluptate. Cillum eu proident incididunt eu sint pariatur officia pariatur. Esse non pariatur dolor ex aliqua. Quis aliquip est duis anim cupidatat do adipisicing.

          </p>

          <div class="horizontal-scrollable">

            <div class="row d-block d-lg-flex modulos-nav mb-6">

              <? for ($i=1; $i <= 10; $i++): ?>

                <div class="col-4 col-sm-3 col-md-2 col-lg d-flex flex-column text-center modulos-nav-item <? echo ($i == 1)? 'active' : ''; ?>">

                  <span class="modulo-name mb-2">Módulo <? echo numberToRomanRepresentation($i); ?></span>

                </div>

              <? endfor; ?>

            </div>

          </div>



          <div class="row modulos-content mb-6">

            <? for ($i=1; $i <= 10; $i++): ?>

              <div class="col-12 modulos-content-item <? echo ($i == 1)? 'active' : ''; ?> px-sm-5">

                <h3 class="mb-5">Módulo <? echo numberToRomanRepresentation($i); ?>: primeros pasos en Unity 3D</h3>

                <div class="row">

                  <div class="col-md-6 py-4">

                    <div class="media">

                      <div class="px-2 mr-2">

                        <img src="<?php echo get_template_directory_uri() ?>/img/cube.svg" class="img-fluid modulo-icon"/>

                      </div>

                      <div class="media-body">

                        <h4 class="mt-0">Qué es Unity 3D</h4>

                        Velit incididunt labore pariatur aliquip officia in consequat culpa duis pariatur tempor nostrud. Dolore qui anim do cupidatat laboris. Tempor exercitation quis fugiat cillum nisi duis excepteur ex adipisicing reprehenderit fugiat laboris. Qui magna non ea incididunt in veniam minim id nostrud sit velit mollit occaecat.

                      </div>

                    </div>

                  </div>

                  <div class="col-md-6 py-4">

                    <div class="media">

                      <div class="px-2 mr-2">

                        <img src="<?php echo get_template_directory_uri() ?>/img/code-download.svg" class="img-fluid modulo-icon"/>

                      </div>

                      <div class="media-body">

                        <h4 class="mt-0">Cómo manejar Unity 3D</h4>

                        Velit incididunt labore pariatur aliquip officia in consequat culpa duis pariatur tempor nostrud. Dolore qui anim do cupidatat laboris. Tempor exercitation quis fugiat cillum nisi duis excepteur ex adipisicing reprehenderit fugiat laboris. Qui magna non ea incididunt in veniam minim id nostrud sit velit mollit occaecat.

                      </div>

                    </div>

                  </div>

                  <div class="col-md-6 py-4">

                    <div class="media">

                      <div class="px-2 mr-2">

                        <img src="<?php echo get_template_directory_uri() ?>/img/bulb.svg" class="img-fluid modulo-icon"/>

                      </div>

                      <div class="media-body">

                        <h4 class="mt-0">Conceptos básicos de iluminación</h4>

                        Velit incididunt labore pariatur aliquip officia in consequat culpa duis pariatur tempor nostrud. Dolore qui anim do cupidatat laboris. Tempor exercitation quis fugiat cillum nisi duis excepteur ex adipisicing reprehenderit fugiat laboris. Qui magna non ea incididunt in veniam minim id nostrud sit velit mollit occaecat.

                      </div>

                    </div>

                  </div>

                  <div class="col-md-6 py-4">

                    <div class="media">

                      <div class="px-2 mr-2">

                        <img src="<?php echo get_template_directory_uri() ?>/img/games.svg" class="img-fluid modulo-icon"/>

                      </div>

                      <div class="media-body">

                        <h4 class="mt-0">El mercado de Assets</h4>

                        Velit incididunt labore pariatur aliquip officia in consequat culpa duis pariatur tempor nostrud. Dolore qui anim do cupidatat laboris. Tempor exercitation quis fugiat cillum nisi duis excepteur ex adipisicing reprehenderit fugiat laboris. Qui magna non ea incididunt in veniam minim id nostrud sit velit mollit occaecat.

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            <? endfor; ?>

            <div class="col-12 text-center mt-6">

              <a class="btn text-primary py-3 px-4" href="#"><img src="<?php echo get_template_directory_uri() ?>/img/download.svg" class="img-fluid icon-download"/>Descargar programa</a>

            </div>

          </div>

        </div>

      </div>

    </div>

  </section>



  <section id="nuestros-profesores">

    <div class="container">

      <div class="row">

        <div class="col-md-7">

          <h2 class="mb-5">Nuestros profesores</h2>

          <p class="mb-6">Contamos con algunos de los mejores profesionales dle sector. Consequat qui ex reprehenderit officia ut.</p>

        </div>

      </div>

    </div>

    <div class="container">

      <div class="row">

        <div class="col-12">

          <div class="carousel-profes" data-flickity='{ "groupCells": true, "contain": true }'>

            <div class="carousel-cell">

              <div class="card rounded shadow profe-card">

                <div class="card-horizontal">

                  <div class="img-square-wrapper bg-img-holder text-center mb-3 mb-md-0 profe-foto">

                    <img src="<?php echo get_template_directory_uri() ?>/img/profesor-1.jpg" class="profe-foto img-fluid" alt="Profesor 1 foto">

                  </div>

                  <div class="card-body">

                    <h5 class="profe-nombre mb-3">Jumaima Al Nour</h5>

                    <h6 class="profe-materia mb-4">Profesor de Unity 3D</h6>

                    <p class="profe-descripcion h5">Proident consequat in quis ea id. Proident consequat in quis ea id. Proident consequat in quis ea id.</p>

                  </div>

                </div>

              </div>

            </div>

            <div class="carousel-cell">

              <div class="card rounded shadow profe-card">

                <div class="card-horizontal">

                  <div class="img-square-wrapper bg-img-holder text-center mb-3 mb-md-0 profe-foto">

                    <img src="<?php echo get_template_directory_uri() ?>/img/profesor-1.jpg" class="profe-foto img-fluid" alt="Profesor 1 foto">

                  </div>

                  <div class="card-body">

                    <h5 class="profe-nombre mb-3">Jumaima Al Nour</h5>

                    <h6 class="profe-materia mb-4">Profesor de Unity 3D</h6>

                    <p class="profe-descripcion h5">Proident consequat in quis ea id. Proident consequat in quis ea id. Proident consequat in quis ea id.</p>

                  </div>

                </div>

              </div>

            </div>

            <div class="carousel-cell">

              <div class="card rounded shadow profe-card">

                <div class="card-horizontal">

                  <div class="img-square-wrapper bg-img-holder text-center mb-3 mb-md-0 profe-foto">

                    <img src="<?php echo get_template_directory_uri() ?>/img/profesor-1.jpg" class="profe-foto img-fluid" alt="Profesor 1 foto">

                  </div>

                  <div class="card-body">

                    <h5 class="profe-nombre mb-3">Jumaima Al Nour</h5>

                    <h6 class="profe-materia mb-4">Profesor de Unity 3D</h6>

                    <p class="profe-descripcion h5">Proident consequat in quis ea id. Proident consequat in quis ea id. Proident consequat in quis ea id.</p>

                  </div>

                </div>

              </div>

            </div>

            <div class="carousel-cell">

              <div class="card rounded shadow profe-card">

                <div class="card-horizontal">

                  <div class="img-square-wrapper bg-img-holder text-center mb-3 mb-md-0 profe-foto">

                    <img src="<?php echo get_template_directory_uri() ?>/img/profesor-1.jpg" class="profe-foto img-fluid" alt="Profesor 1 foto">

                  </div>

                  <div class="card-body">

                    <h5 class="profe-nombre mb-3">Jumaima Al Nour</h5>

                    <h6 class="profe-materia mb-4">Profesor de Unity 3D</h6>

                    <p class="profe-descripcion h5">Proident consequat in quis ea id. Proident consequat in quis ea id. Proident consequat in quis ea id.</p>

                  </div>

                </div>

              </div>

            </div>

            <div class="carousel-cell">

              <div class="card rounded shadow profe-card">

                <div class="card-horizontal">

                  <div class="img-square-wrapper bg-img-holder text-center mb-3 mb-md-0 profe-foto">

                    <img src="<?php echo get_template_directory_uri() ?>/img/profesor-1.jpg" class="profe-foto img-fluid" alt="Profesor 1 foto">

                  </div>

                  <div class="card-body">

                    <h5 class="profe-nombre mb-3">Jumaima Al Nour</h5>

                    <h6 class="profe-materia mb-4">Profesor de Unity 3D</h6>

                    <p class="profe-descripcion h5">Proident consequat in quis ea id. Proident consequat in quis ea id. Proident consequat in quis ea id.</p>

                  </div>

                </div>

              </div>

            </div>

          </div>

          <div class="d-flex justify-content-end carousel-profes-custom-nav py-5 py-lg-6">

            <span class="prev"><img src="<?php echo get_template_directory_uri() ?>/img/arrow-ios-left.svg" class="fluid-image raw-svg mr-3"></span>

            <span class="next active"><img src="<?php echo get_template_directory_uri() ?>/img/arrow-ios-right.svg" class="fluid-image raw-svg ml-3"></span>

          </div>

        </div>

      </div>

    </div>

  </section>



  <section id="metodologia-tokio" class="">

    <div class="container py-5 py-md-6">



      <div class="row">

        <div class="col-md-6">

          <h2 class="mb-5">La metodología de Tokio</h2>

          <div id="accordion-metodologia-tokio" class="accordion custom-accordion">

            <div class="card border-0 bg-white mb-0">

              <div class="accordion-item">

                <div class="card-header border-0 bg-white collapsed p-3" data-toggle="collapse" href="#collapseOne2">

                  <a class="card-title h5"> Clases en directo y asíncronas </a>

                </div>

                <div id="collapseOne2" class="collapse p-0 collapse" data-parent="#accordion-metodologia-tokio">

                  <div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. samus labore sustainable VHS. </div>

                </div>

              </div>

              <div class="accordion-item">

                <div class="card-header border-0 bg-white collapsed p-3" data-toggle="collapse" data-parent="#accordion-metodologia-tokio" href="#collapseTwo2">

                  <a class="card-title h5"> Tutorizaciones </a>

                </div>

                <div id="collapseTwo2" class="collapse p-0 collapse" data-parent="#accordion-metodologia-tokio">

                  <div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. samus labore sustainable VHS. </div>

                </div>

              </div>

              <div class="accordion-item">

                <div class="card-header border-0 bg-white collapsed p-3" data-toggle="collapse" data-parent="#accordion-metodologia-tokio" href="#collapseThree2">

                  <a class="card-title h5"> Grupos reducidos </a>

                </div>

                <div id="collapseThree2" class="collapse p-0" data-parent="#accordion-metodologia-tokio">

                  <div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. samus labore sustainable VHS. </div>

                </div>

              </div>

              <div class="accordion-item">

                <div class="card-header border-0 bg-white collapsed p-3" data-toggle="collapse" data-parent="#accordion-metodologia-tokio" href="#collapseFour2">

                  <a class="card-title h5"> Flexibilidad </a>

                </div>

                <div id="collapseFour2" class="collapse p-0" data-parent="#accordion-metodologia-tokio">

                  <div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. samus labore sustainable VHS. </div>

                </div>

              </div>

            </div>

          </div>



        </div>

        <div class="col-md-6 bg-image-circle-right">

          <img src="<?php echo get_template_directory_uri() ?>/img/ilustracion-como-lo-hacemos.svg" class="raw-svg img-fluid"/>

        </div>



      </div>



    </div>

  </section>





  <section id="video" class="bg-white">

    <div class="container py-5 py-md-6">

      <div class="row">

        <div class="col-12 text-center">

          <?php

            $video =  [

              'video_url' => 'https://www.youtube.com/embed/Jfrjeg26Cwk',

              'video_descripcion' => 'Cupidatat et fugiat dolor pariatur. Nisi consequat in in pariatur ad id ex amet excepteur laboris officia. Cillum sunt qui sit tempor amet aliquip reprehenderit officia irure esse et tempor fugiat. Duis reprehenderit consectetur mollit elit elit tempor aliquip proident quis in. Lorem veniam ullamco nisi fugiat. Fugiat reprehenderit dolore aliquip id. Consectetur et pariatur labore consequat.'

            ];

          ?>

          <?php ec_get_template_part('template-parts/components/component', 'popup-video', $video); ?>

        </div>

      </div>

    </div>

  </section>



  <section id="empresas-lideres" class="bg-light">

    <div class="container bg-light py-5 py-md-6">



      <div class="row">

        <div class="col-md-6 pr-md-6">



          <h2 class="mb-5">Trabaja en las empresas líderes en innovación</h2>

          <p class="mb-5">Incididunt exercitation irure ea eiusmod eu aliquip in nisi. Sit eu tempor irure commodo dolore nisi minim proident. Sit velit reprehenderit aute deserunt tempor et cupidatat velit excepteur aliquip exercitation cupidatat. Proident labore ad sit ipsum laborum eu ut.</p>

          <a class="btn btn-primary d-none d-md-inline-block py-3 px-4" href="#">Procure empresas</a>



        </div>

        <div class="col-md-6 bg-secondary d-flex justify-content-between rounded p-3 p-md-4">

          <div class="row text-center">

            <div class="col-4 p-3 d-flex justify-content-center"><img class="raw-svg img-fluid" src="<?php echo get_template_directory_uri() ?>/img/logo_1.svg" /></div>

            <div class="col-4 p-3 d-flex justify-content-center"><img class="raw-svg img-fluid" src="<?php echo get_template_directory_uri() ?>/img/logo_2.svg" /></div>

            <div class="col-4 p-3 d-flex justify-content-center"><img class="raw-svg img-fluid" src="<?php echo get_template_directory_uri() ?>/img/logo_3.svg" /></div>

            <div class="col-4 p-3 d-flex justify-content-center"><img class="raw-svg img-fluid" src="<?php echo get_template_directory_uri() ?>/img/logo_4.svg" /></div>

            <div class="col-4 p-3 d-flex justify-content-center"><img class="raw-svg img-fluid" src="<?php echo get_template_directory_uri() ?>/img/logo_5.svg" /></div>

            <div class="col-4 p-3 d-flex justify-content-center"><img class="raw-svg img-fluid" src="<?php echo get_template_directory_uri() ?>/img/logo_6.svg" /></div>

            <div class="col-4 p-3 d-flex justify-content-center"><img class="raw-svg img-fluid" src="<?php echo get_template_directory_uri() ?>/img/logo_7.svg" /></div>

            <div class="col-4 p-3 d-flex justify-content-center"><img class="raw-svg img-fluid" src="<?php echo get_template_directory_uri() ?>/img/logo_8.svg" /></div>

            <div class="col-4 p-3 d-flex justify-content-center"><img class="raw-svg img-fluid" src="<?php echo get_template_directory_uri() ?>/img/logo_9.svg" /></div>

          </div>

        </div>

      </div>



    </div>

  </section>



  <section id="opiniones" class="bg-white">

    <div class="container py-5 py-md-6">



      <div class="row">

        <div class="col-md-12 text-center">



          <?php

            $alumno = [

              'img' => 'https://www.tokioschool.com/wp-content/themes/esgalla/img/alumno_juan_sanchez.jpg',

              'cita' => 'Dolor sunt magna eu proident dolore in dolore cupidatat labore in elit voluptate magna ipsum. Dolor sunt magna eu proident dolore in dolore cupidatat labore in elit voluptate magna ipsum.',

              'nom' => 'Juan Sánchez',

              'puesto' => 'Programador de videojuegos',

            ];

          ?>

          <?php ec_get_template_part('template-parts/components/component', 'opiniones-alumnos', $alumno); ?>



        </div>

      </div>



    </div>

  </section>



<?php get_template_part('template-parts/blocks/block', 'speak-english'); ?>



  <section id="titulacion-oficial">

    <div class="container py-5 py-md-6">

      <div class="row">

        <div class="col text-center">

            <h2 class="mb-3 mb-md-4">Titulación oficial, pero con muchas garantías</h2>

            <p class="mb-5 w-md-50 mx-auto">Contamos con algunos de los mejores profesionales del sector. Quis do proident deserunt reprehenderit esse est ut sit eiusmod anim est commodo.</p>

        </div>

      </div>

      <div class="row text-center">

          <div class="col-6 col-md my-3">

            <img src="<?php echo get_template_directory_uri() ?>/img/cisco.svg" class="img-fluid"/>

          </div>

          <div class="col-6 col-md my-3">

            <img src="<?php echo get_template_directory_uri() ?>/img/universidad-nebrija.svg" class="img-fluid"/>

          </div>

          <div class="col-6 col-md my-3">

            <img src="<?php echo get_template_directory_uri() ?>/img/cisco.svg" class="img-fluid"/>

          </div>

          <div class="col-6 col-md my-3">

            <img src="<?php echo get_template_directory_uri() ?>/img/universidad-nebrija.svg" class="img-fluid"/>

          </div>

      </div>

    </div>

  </section>



  <section id="ayudas-financiaciones" class="bg-light">

    <div class="container bg-light py-5 py-md-6">

      <div class="row">

        <div class="col-lg-4 bg-white shadow rounded p-4 mb-4 mx-2 mx-lg-0 p-lg-5">

          <p class="formacion-nombre mini-text mb-3">Carreira en Programação de Videojogos e Realidade Virtual</p>

          <span class="h3 d-block">Desde</span>

          <span class="h2 d-block">2.000 €</span>

          <p class="iva-incluido mini-text mb-4">I.V.A. incluido</p>

          <ul class="list-group listado-formacion px-4">

            <li class="list-group-item mb-3">2 años para acabar tu formación</li>

            <li class="list-group-item mb-3">5 años para realizar las prácticas</li>

            <li class="list-group-item mb-3">Formación 100% online</li>

            <li class="list-group-item mb-3">Flexibilidad de horarios</li>

            <li class="list-group-item mb-3">Formación en inglés</li>

            <li class="list-group-item mb-3">Tutorizaciones individuales</li>

            <li class="list-group-item mb-3">Bolsa de empresas</li>

          </ul>

          <hr class="mt-3 mb-3">

          <p class="text-tokio-black">Fechas del curso</p>

          <p class="">Las fdechas del curso se determinarán una vez que se establezcan los miembros que conformarán cada grupo</p>

          <a class="btn text-primary py-3 px-0" href="#"><img src="<?php echo get_template_directory_uri() ?>/img/download.svg" class="img-fluid icon-download"/>Descargar programa</a>

        </div>

        <div class="col-lg-8 px-4 px-lg-6 py-4">

          <h2 class="mb-3 mb-md-4">Ayudas y financiaciones</h2>

          <p class="mb-3 mb-md-4">Pequeño texto sobre ayudas y financiaciones. Minim dolor aliqua laborum exercitation et sunt laboris esse excepteur.</p>

          <form>

            <div class="form-group">

              <input class="form-control form-control-md" type="text" placeholder="Nombre y apellidos">

            </div>

            <div class="form-group">

              <input class="form-control form-control-md" type="text" placeholder="Correo electrónico">

            </div>

            <div class="form-group">

              <input class="form-control form-control-md" type="text" placeholder="Teléfono">

            </div>

            <div class="form-group">

              <textarea class="form-control form-control-md" id="exampleFormControlTextarea1" rows="5" placeholder="Mensaje"></textarea>

            </div>

            <div class="form-group">

              <div class="form-check">

                <input class="form-check-input" type="checkbox" value="" id="legal-check">

                <label class="form-check-label" for="legal-check">

                  He leído y acepto la <a href="#">Política de Privacidad</a>

                </label>

              </div>

              <div class="form-check">

                <input class="form-check-input" type="checkbox" value="" id="nl-check">

                <label class="form-check-label" for="nl-check">

                  Quiero recibir información sobre cursos y promociones

                </label>

              </div>

            </div>

            <div class="form-group">

              <button type="submit" class="btn btn-primary">Enviar solicitud</button>

            </div>

          </form>

        </div>

      </div>

    </div>

  </section>



  <section id="preguntas-frecuentes">

    <div class="container py-5 py-md-6">

      <div class="row">

        <div class="col">

          <span class="h2 d-block mb-5">Preguntas frecuentes</span>

          <div id="accordion-faqs-titulos-y-certificados" class="accordion accordion-faqs">

            <div class="card border-0 rounded-0 mb-0">

              <div class="card-header bg-white collapsed px-1 py-4" data-toggle="collapse" href="#collapseTitulos1">

                <a class="card-title h5 font-weight-normal"> ¿Aliquip eiusmod Lorem ex commodo veniam nisi occaecat dolore in aute et.? </a>

              </div>

              <div id="collapseTitulos1" class="collapse p-0 bg-white collapse" data-parent="#accordion-faqs-titulos-y-certificados">

                <div class="card-body font-weight-normal px-1 py-3">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. samus labore sustainable VHS. </div>

              </div>

              <div class="card-header bg-white collapsed px-1 py-4" data-toggle="collapse" data-parent="#accordion-faqs-titulos-y-certificados" href="#collapseTitulos2">

                <a class="card-title h5 font-weight-normal"> ¿Aliquip eiusmod Lorem ex commodo veniam nisi occaecat dolore in aute et.? </a>

              </div>

              <div id="collapseTitulos2" class="collapse p-0 bg-white collapse" data-parent="#accordion-faqs-titulos-y-certificados">

                <div class="card-body px-1 py-3">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. samus labore sustainable VHS. </div>

              </div>

              <div class="card-header bg-white collapsed px-1 py-4" data-toggle="collapse" data-parent="#accordion-faqs-titulos-y-certificados" href="#collapseTitulos3">

                <a class="card-title h5 font-weight-normal"> ¿Aliquip eiusmod Lorem ex commodo veniam nisi occaecat dolore in aute et.? </a>

              </div>

              <div id="collapseTitulos3" class="collapse p-0 bg-white" data-parent="#accordion-faqs-titulos-y-certificados">

                <div class="card-body px-1 py-3">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. samus labore sustainable VHS. </div>

              </div>

              <div class="card-header bg-white collapsed px-1 py-4" data-toggle="collapse" data-parent="#accordion-faqs-titulos-y-certificados" href="#collapseTitulos4">

                <a class="card-title h5 font-weight-normal"> ¿Aliquip eiusmod Lorem ex commodo veniam nisi occaecat dolore in aute et.? </a>

              </div>

              <div id="collapseTitulos4" class="collapse p-0 bg-white" data-parent="#accordion-faqs-titulos-y-certificados">

                <div class="card-body px-1 py-3">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. samus labore sustainable VHS. </div>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </section>



</article><!-- #post-<?php the_ID(); ?> -->





<?php

function numberToRomanRepresentation($number) {

  $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

  $returnValue = '';

  while ($number > 0) {

      foreach ($map as $roman => $int) {

          if($number >= $int) {

              $number -= $int;

              $returnValue .= $roman;

              break;

          }

      }

  }

  return $returnValue;

}

?>
