<?php

/**

 * Template part for displaying page content in page-home.php

 *

 * @package esgalla

 */



?>



<header id="masthead" class="site-header fullheight position-relative bg-white pt-5 pt-md-5">


		<div class="container full-height-container h-100 pt-4 ">

			<div class="row h-100 align-items-end">

				<div class="col-lg-7 text-md-center align-self-center text-lg-left pb-4 pr-lg-5">

					<div class="">

						<span class="h5 text-tokio-navyblue mb-4 wow animate__fadeInUp" data-wow-duration="2s">TOKIO NEW TECHNOLOGY SCHOOL</span>

						<h1 class="masthead-title text-tokio-pink mb-3  wow animate__fadeInUp"  data-wow-duration="2s">A escola digital dos samurais tecnológicos</h1>

						<p class="masthead-lead mb-3 text-tokio-navyblue titilumsemibold">Na Tokio School oferecemos a melhor formação digital especializada e totalmente online. Formamos aqueles que querem saber sempre mais nas áreas da programação e dos videojogos, transformando-os em autênticos samurais digitais. Como o mundo, estamos em constante atualização e sempre em movimento.</p>

						<p class="masthead-lead mb-5 text-tokio-navyblue titilumsemibold">Se queres ir <strong><i>#alwaysfoward</i></strong>, chegaste ao teu destino. Yatta!</p>

						<div class="masthead-links d-flex flex-row">

						<a class="btn btn-tokio-navyblue btn-mov-100 text-white rounded-pill py-3 px-4" href="<?php echo get_site_url(); ?>/descubre-tokio/">Descobre Tokio School</a>

						</div>

					</div>

				</div>

				<div class="col-lg-5" >

					<img src="<?php echo get_template_directory_uri() ?>/img/home-hero.jpg" class="img-fluid"/>

				</div>

			</div>

			<div class="row flex-column align-items-center pb-5 p-a-c d-none d-lg-flex">

				<img class="mx-auto raw-svg mouse-icon-scroll" src="/wp-content/themes/esgalla/img/mouse.svg" alt="mouse">

				<div class="w-100"></div>

				<span class="fs-14 text-tokio-navyblue text-center font-weight-bold pt-2">Desce para descobrir Tokio</span>

			</div>

		</div>


</header><!-- #masthead -->



<?php

// Hay dos opciones: mapear o hacer consulta en cada "cachito" de los siguientes...

$i_af = 0;

while ( have_rows('areas_de_formacion') ) : the_row();

	$areas_formacion[$i_af]["icono"] = get_sub_field("icono");

	$areas_formacion[$i_af]["nombre"] = get_sub_field("nombre");

	$areas_formacion[$i_af]["imagen"] = get_sub_field("imagen");

	$areas_formacion[$i_af]["titulo"] = get_sub_field("titulo");

	$areas_formacion[$i_af]["subtitulo"] = get_sub_field("subtitulo");

	$areas_formacion[$i_af]["enlace"] = get_sub_field("enlace");

$i_af++;

endwhile;

?>



<section id="areas-formacion" class="bg-tokio-navyblue">

	<div class="container bg-white-pico-bottom-left pb-5 pb-md-6 pt-md-6 pt-5">

		<div class="row">

			<div class="col-md-9 mx-md-auto text-md-center wow animate__fadeInUp" data-wow-duration="2s"  >

				<h2 class="text-white mb-4 mb-md-6">Está na hora de dar &lt;enter&gt; ao teu futuro!</h2>

			</div>

		</div>



		<div class="row container-controls w-90 m-auto mb-md-4">

			<div class="col-md-7 d-md-none pb-5 custom-carousel-controls-mobile">

				<?php

					foreach ($areas_formacion as $indice => $area) {

						if($indice==0) echo '<span class=" d-inline-block mr-3 link-button active"><i class="fas '.$area["icono"].' px-3 mr-0"></i>'.$area["nombre"].'</span>';

						else echo '<span class=" d-inline-block mr-3 link-button"><i class="fas '.$area["icono"].' px-3 mr-0"></i>'.$area["nombre"].'</span>';

					}

				?>

			</div>

			<div class="col-md-7 d-none d-md-flex flex-column custom-carousel-controls carousel-categories justify-content-between">

				<?php

					foreach ($areas_formacion as $indice => $area) {

						if($indice==0) $activo = " active";

						else $activo = "";

						$item = '<div class="custom-controls-button px-4 my-2'.$activo.'">

						<i class="fas '.$area["icono"].' text-gray bg-light rounded p-3 mr-2 my-0"></i> '.$area["nombre"].'</div>';

						echo $item;

					}

				?>

				<div class="active-element-bar"></div>

			</div>



			<div class="col-md-5">



				<div class="carousel carousel-home-areas-formacion" data-flickity='{ "imagesLoaded": false, "fade": true }'>



					<?php

						foreach ($areas_formacion as $indice => $area) {

							if($indice==0) $activo = " active";

							else $activo = "";

							echo '<div class="carousel-cell mx-2 px-2">

										<img class="img-fluid rounded mb-4" src="'.wp_get_attachment_image_src($area["imagen"])[0].'" srcset="'.wp_get_attachment_image_srcset($area["imagen"]).'"  />

										<h3 class="h3 text-white mb-4">'.$area["titulo"].'</h3>

										<p class="text-white mb-4 titilumsemibold">'.$area["subtitulo"].'</p>

										<a class="btn btn-outline-tokio-green rounded-pill btn-mov-100 mt-3 py-3 px-4" href="'.$area["enlace"].'">Pede Informações</a>

									</div>';

						}

					?>

				</div>

			</div>

		</div>



	</div>

</section>





<?php if(have_rows('metodologia')): ?>

<section id="formacion-online" class="">

	<div class="container py-5 py-md-6">

		<div class="row mb-md-5">

			<div class="col-md-9 mx-md-auto text-md-center">

				<h2 class="text-tokio-green font-weight-bold h6 mb-4 mb-md-4 wow animate__fadeInUp" data-wow-duration="2.5s">O método Tokio</h2>

				<h2 class="text-tokio-navyblue mb-4 mb-md-5 wow animate__fadeInUp" data-wow-duration="2s">Formação especializada 100% online</h2>

			</div>

		</div>



		<div class="row d-md-none">

			<div class="carousel carousel-metodologia-mobile" data-flickity='{ "imagesLoaded": false, "prevNextButtons": true, "pageDots": false }'>

				<?php while ( have_rows('metodologia') ) : the_row(); ?>

				<div class="carousel-cell">

					<div class="col-md-4 flex-column align-items-center text-center px-md-4">

						<div class="bg-white h-100 metodologia-card px-4 pt-4 pb-4">

							<img src="<?php echo get_sub_field('imagen') ?>" class="img-fluid"/>

							<span class="h3 text-tokio-navyblue mb-3"><?php echo get_sub_field('titulo') ?></span>

							<p class="mb-3 px-2 px-md-2 titilumsemibold"><?php echo get_sub_field('subtitulo') ?></p>

						</div>

					</div>

				</div>

				<?php endwhile ?>

			</div>

		</div>



		<div class="row d-none d-md-flex">

			<?php while ( have_rows('metodologia') ) : the_row(); ?>

			<div class="col-md-4 flex-column align-items-center text-center mb-5 px-md-4">

				<div class="bg-white h-100 metodologia-card px-4 pt-4 pb-4">

					<img src="<?php echo get_sub_field('imagen') ?>" class="img-fluid"/>

					<h3 class="h3 text-tokio-navyblue mb-3"><?php echo get_sub_field('titulo') ?></h3>

					<p class="mb-3 px-2 px-md-2 titilumsemibold"><?php echo get_sub_field('subtitulo') ?></p>

				</div>

			</div>

			<?php endwhile ?>

		</div>

		<div class="row">

			<div class="col-12 text-center">

				<a class="btn btn-tokio-navyblue rounded-pill d-block d-md-inline-block py-3 px-4 mt-3" href="<?php echo get_site_url(); ?>/descubre-tokio/">Metodologia</a>

			</div>

		</div>



	</div>

</section>

<?php endif; ?>


<?php get_template_part('template-parts/blocks/block', 'speak-english'); ?>


<div class="bg-green-arrow">

	<section id="empresas-lideres" class="">

		<div class="container py-5 py-md-6">


			<?php get_template_part('template-parts/blocks/block', 'medallas'); ?>


			<div class="row d-flex justify-content-between align-items-center">

				<div class="col-md-6 pr-md-6">

					<h2 class="text-tokio-navyblue mb-4 wow animate__fadeInUp" data-wow-duration="2s">É no tatami que mostras o que vales</h2>

					<p class="text-tokio-black titilumsemibold mb-5">O teu tatami são as empresas, líderes no setor tecnológico e digital, nas quais podes aplicar os teus conhecimentos de samurai. Na Tokio School tens <strong>até 300 horas de estágios qualificados</strong> para dar um push ao teu networking e um boost diferenciador ao teu CV. </p>

					<a class="btn btn-tokio-navyblue rounded-pill d-none d-md-inline-block py-3 px-4" href="<?php echo get_site_url(); ?>/para-empresas/">Explorar empresas</a>

				</div>

				<div class="col-md-6 d-flex justify-content-center p-3 p-md-4 innovacion">

					<div class="row text-center">

						<?php while(have_rows('empresas_imagenes')): the_row(); ?>
							<div class="col-4 d-flex justify-content-center">
								<img class="img-fluid" src="<?php echo wp_get_attachment_url( get_sub_field('imagenes'), 'large');?>" />
							</div>
                		<?php endwhile; ?>

					</div>

				</div>

			</div>



		</div>

	</section>

</div>







<?php get_template_part('template-parts/blocks/block', 'alwaysforward'); ?>