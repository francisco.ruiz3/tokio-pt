<?php



/**



 * Template part for displaying page content in page-faqs.php



 *



 * @package esgalla



 */







?>







<header id="masthead" class="site-header fullheight position-relative ">



    <div class="container-fluid bg-tokio-navyblue bg-pink-arrow-v3 pt-5 pt-md-5">

      <nav aria-label="breadcrumb">

        <div class="container">

          <ol class="breadcrumb">

            <li class="breadcrumb-item"><a class="text-white" href="<?php echo get_home_url( ) ?>">Inicio</a></li>

            <li class="breadcrumb-item text-tokio-pink active" aria-current="page"><?php echo get_the_title() ?></li>

          </ol>

        </div>

      </nav>

      <div class="container full-height-container h-100 py-5 py-md-6 pb-md-7 bg-white-pico-bottom-left">

        <div class="row align-items-center h-100 pt-md-3">

          <div class="col-lg-6 order-lg-last align-self-center text-center text-lg-right" >

            

          </div>

          <div class="col-lg-6 align-self-center text-left">

            <h1 class="text-tokio-pink masthead-title mb-5">Dúvidas?</h1>

            <p class="text-white masthead-lead mb-5">

            É normal que as tenhas, mas aqui encontras as respostas às perguntas mais frequentes dos nossos alunos. Se não encontrares a resposta que procuras, entra em contacto connosco. Estamos aqui para te ajudar!
            </p>

          </div>

        </div>

      </div>

    </div>



</header><!-- #masthead -->


<?php if(have_rows('seccion')): while(have_rows('seccion')): the_row(); $contadorPR=1; $seccion_tit = get_sub_field('titulo'); $seccion_tit_san = sanitize_title($seccion_tit); ?>

	<section id="faqs-titulos-y-certificados">

		<div class="container py-5 py-md-5">

			<div class="row">

				<div class="col">

					

					<div id="accordion-faqs-<?php echo $seccion_tit_san ?>" class="accordion accordion-faqs mx-auto">

            <h4 class="text-primary mb-3"><?php echo $seccion_tit ?></h4>

					<?php while(have_rows('preguntas')): the_row(); ?>

						<div class="card border-0 rounded-0 mb-0">

							<div class="card-header bg-white collapsed px-1 pb-4 pt-5" data-toggle="collapse" data-parent="#accordion-faqs-<?php echo $seccion_tit_san ?>" href="#collapse<?php echo $seccion_tit_san.$contadorPR ?>">

								<a href="javascript: void(0)" class="card-title text-tokio-black h5 font-weight-bold"> <?php echo get_sub_field('pregunta') ?> </a>

							</div>

							<div id="collapse<?php echo $seccion_tit_san.$contadorPR++ ?>" class="collapse p-0 bg-white" data-parent="#accordion-faqs-<?php echo $seccion_tit_san ?>">

								<div class="card-body px-1 py-3"><?php echo nl2br(get_sub_field('respuesta')) ?></div>

							</div>

              <div class="acordion-item-separator w-100"></div>

						</div>

					<?php endwhile; ?>

					</div>

				</div>

			</div>

		</div>

	</section>

<?php endwhile; endif; ?>



<section id="mas-info">



  <div class="container bg-tokio-navyblue py-5 py-md-5">

    <div class="col text-center">

      <h3 class="text-white mb-5">Precisas de mais informações?</h3>

      <a class="btn btn-tokio-pink rounded-pill mb-3 mb-lg-0 py-3 px-4" href="https://www.tokioschool.com/contacto/">Contacta a Tokio School</a>

    </div>

  </div>



</section>

