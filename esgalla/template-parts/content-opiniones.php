<?php

/**

 * Template part for displaying page content in page-home.php

 *

 * @package esgalla

 */



get_template_part("template-parts/tema", "header");



?>



<div>

	<header id="masthead" class="site-header fullheight position-relative">

		<div class="container-fluid bg-tokio-navyblue pt-5 pt-md-5">

			<nav aria-label="breadcrumb">

				<div class="container">

					<ol class="breadcrumb">

						<li class="breadcrumb-item"><a class="text-white" href="<?php echo get_home_url( ) ?>">Inicio</a></li>

						<li class="breadcrumb-item text-secondary active" aria-current="page"><?php echo get_the_title() ?></li>

					</ol>

				</div>

			</nav>

			<div class="container full-height-container h-100 pt-4 pt-md-5">

				<div class="row align-items-center h-100">

					<div class="col-lg-6 align-self-center text-md-center text-lg-left">

						<h1 class="masthead-title text-secondary mb-4 mt-3 wow animate__fadeInUp" data-wow-duration="2s">La opinión de nuestros tokiers</h1>

						<p class="masthead-lead text-white mb-5">

              Nuestros alumnos son nuestro orgullo, por eso nos importa tanto su opinión y hacemos todo lo posible para visibilizar su trabajo. 

              Aquí podrás conocer las <strong>opiniones sobre Tokio School</strong>, echarle un ojo a sus proyectos y descubrir hacia dónde se dirigen sus carreras profesionales.

            </p>

					</div>

					<div class="col-lg-6 order-lg-last align-self-center text-lg-right" >

						<!-- <div id="parallaxhoverx">

							<div data-depth="0.2"> -->

								<img src="<?php echo get_template_directory_uri() ?>/img/hero-opiniones.png" class="img-fluid"/>

							<!-- </div>

						</div> -->

					</div>

				</div>

			</div>

		</div>

	</header><!-- #masthead -->

</div>



<section class="bg-white mt-1 mt-lg-5">



	<div class="container py-4 py-md-6">

		<div class="row pt-lg-3">



			<div class="col-sm-4 text-center mb-5">

				<div class="h1 text-secondary pb-2 pb-sm-3 pb-md-4 animated-counter" data-counter="1000" data-counter-prefix="+">0</div>

				<div class="h4 text-secondary">alumnos </br>graduados al año</div>

			</div>

			<div class="col-sm-4 text-center mb-5">

				<div class="h1 text-secondary pb-2 pb-sm-3 pb-md-4 animated-counter" data-counter="25" data-counter-prefix="+"></div>

				<div class="h4 text-secondary">formaciones</div>

			</div>

			<div class="col-sm-4 text-center mb-5">

				<div class="h1 text-secondary pb-2 pb-sm-3 pb-md-4 animated-counter" data-counter="1500">0</div>

				<div class="h4 text-secondary">ofertas de empleo </br>publicadas al año</div>

			</div>


		</div>

	</div>



</section>



<section id="opiniones" class="bg-white">

	<div class="container-fluid pb-5 pb-md-6 pt-md-3 px-lg-6">



		<div class="row">

			<div class="col-md-12">

				<?php

					$testimonios_1_args = array(

						'post_type'					=> array( 'testimonio' ),

						'post__in'					=> get_field('testimonios_1'),

					);

					$testimonios_1 = new WP_Query( $testimonios_1_args );



					// print_r($testimonios_1);



					if ( $testimonios_1->have_posts() ) {

						$ttt=0;

						while ( $testimonios_1->have_posts() ) {

							$testimonios_1->the_post();

							$testimonio_id_wp = get_the_id();

							$opiniones_alumnos_1[$ttt++] = [

								'nombre' => get_field('nombre', $testimonio_id_wp),

								'cargo' => get_field('cargo', $testimonio_id_wp),

								'texto' => get_field('texto', $testimonio_id_wp),

								'imagen' => get_field('imagen', $testimonio_id_wp),

							];

						}

					} else{

						$opiniones_alumnos_1[0] = [

							//'img' => 'https://www.tokioschool.com//wp-content/themes/esgalla/img/alumno_juan_sanchez.jpg',

							'texto' => 'Dolor sunt magna eu proident dolore in dolore cupidatat labore in elit voluptate magna ipsum. Dolor sunt magna eu proident dolore in dolore cupidatat labore in elit voluptate magna ipsum.',

							'nombre' => 'Juan Sánchez',

							'cargo' => 'Programador de videojuegos',

						];

						$opiniones_alumnos_1[0]["imagen"]["sizes"]["medium"]='https://www.tokioschool.com//wp-content/themes/esgalla/img/alumno_juan_sanchez.jpg';

					}



					wp_reset_postdata();





				?>

				<?php get_template_part('template-parts/components/component', 'opiniones-alumnos', $opiniones_alumnos_1); ?>



			</div>

		</div>



	</div>

</section>



<section id="trabajos" class="bg-white">



	<div class="container-fluid pl-lg-0 py-5">

		<div class="container">

			<h2 class="text-tokio-navyblue mb-5 wow animate__fadeInUp" data-wow-duration="2s">Sus trabajos hablan por sí solos</h2>

		</div>



		<div class="row row-carousel">

			<div class="col pl-lg-0">

				<div class="carousel carousel-proyectos" data-flickity='{ "freeScroll": true, "contain": true, "prevNextButtons": true, "pageDots": false }'>

						<?php

						if(have_rows('trabajos')){

							while(have_rows('trabajos')){ the_row();

								$trabajo = [

									'post_avatar' => get_sub_field('foto')['sizes']['thumbnail'],

									'post_nombre' => get_sub_field('nombre'),

									'post_subtitle' => get_sub_field('cargo'),

									'post_title' => get_sub_field('titulo_proyecto'),

									'post_img' => get_sub_field('portada_proyecto')['url'],

									'post_description' => get_sub_field('descripcion_proyecto'),



								];



								echo '<div class="carousel-cell">';

								ec_get_template_part('template-parts/components/component', 'trabajo', $trabajo);

								echo '</div>';

							}

						}

						else{

							$trabajo = [

								'post_img' => 'https://www.tokioschool.com/wp-content/themes/esgalla/img/trabajo1.svg',

								'post_avatar' => 'https://www.tokioschool.com/wp-content/themes/esgalla/img/antonio.svg',

								'post_nombre' => 'Antonio Pérez',

								'post_subtitle' => 'Programador Java',

								'post_title' => 'Sostenibilidad nombre del proyecto',

								'post_description' => 'Lorem ipsum dolor set. Vel orci diam consequat enim volutpat commodo aenean id. Velit vulputate purus elementum.Lorem ipsum dolor set. Vel orci diam consequat enim volutpat commodo aenean id. Velit vulputate purus elementum.

								Lorem ipsum dolor set. Vel orci diam consequat enim volutpat commodo aenean id. Velit vulputate purus elementum.',



							];



							echo '<div class="carousel-cell">';

							ec_get_template_part('template-parts/components/component', 'trabajo', $trabajo);

							echo '</div>';

						}

						?>

				</div>

			</div>

		</div>



	</div>



</section>



<!-- Modal de cada uno de los trabajos del carrusel -->

<?php

if(have_rows('trabajos')){

	while(have_rows('trabajos')){ the_row();

		$trabajo = [

			'post_avatar' => get_sub_field('foto')['sizes']['thumbnail'],

			'post_nombre' => get_sub_field('nombre'),

			'post_cargo' => get_sub_field('cargo'),

			'post_titulo' => get_sub_field('titulo_proyecto'),

			'post_descripcion' => get_sub_field('descripcion_proyecto'),

			'post_galeria' => get_sub_field('galeria'),

			'post_video' => get_sub_field('id_video'),

		];

		ec_get_template_part('template-parts/components/component', 'trabajo-modal', $trabajo);

	}

}

?>

</div>





<?php

	$post_relacionados_conf['titulo'] = 'Notícias relacionadas';

	$post_relacionados_conf['categoria'] = 'opiniones-tokio-school';

	$post_relacionados_conf['limite'] = 10;

	$post_relacionados_conf['excluidos'] = array();

	get_template_part('template-parts/blocks/block', 'post-relacionados', $post_relacionados_conf);



