<?php
/**
 * Template part for displaying page content in page-contacto.php
 *
 * @package esgalla
 */

?>

<header id="masthead" class="site-header fullheight position-relative header-contacto">
	<div class="container-fluid bg-tokio-navyblue  bg-green-arrow-v3 pt-5 pt-md-5">
		<nav aria-label="breadcrumb">
			<div class="container">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a class=" text-white" href="<?php echo get_home_url( ) ?>">Inicio</a></li>
					<li class="breadcrumb-item text-tokio-green"><?php echo get_the_title() ?></li>
				</ol>
			</div>
		</nav>
		<div class="container full-height-container h-100 py-5 py-md-6 bg-white-pico-bottom-left">
			<div class="row align-items-center h-100">
				<div class="col-lg-6 order-lg-last pr-0 mb-5 align-self-center text-lg-right" >
					
				</div>
				<div class="col-lg-6 align-self-center">
					<h1 class="text-tokio-green masthead-title mb-4 wow animate__fadeInUp" data-wow-duration="2s">Olá, olá, konnichiwa!</h1>
					<p class="text-white masthead-lead mb-4"> Nós sabemos: o primeiro passo é o mais importante. Estamos deste lado para começar uma caminhada que vai levar os nossos alunos mais longe. Falar connosco é simples, por telefone, e-mail ou chat – sim, connosco, sem mensagens automáticas ou bots pelo meio. Banzai!</p>
					<a class="h3 text-tokio-green py-2 px-0" href="tel:+211654494">
						<img src="<?php echo get_template_directory_uri() ?>/img/phone-fill-green.svg" class="img-fluid mb-2 mr-3"/>211 654 494
					</a>
				</div>
			</div>
		</div>
	</div>
</header><!-- #masthead -->


<section id="formulario-contacto" class="contacto-contacto background-contacto-1">
	<div class="container mt-md-5 py-5 py-md-6  ">
		<div class="row">
      <div class="col-lg-7 text-left form-contacto">
        <h2 class="text-tokio-navyblue mb-4 wow animate__fadeInUp" data-wow-duration="2s">Como podemos ajudar-te?</h2>
        <p class="text-tokio-black mb-4">Numa mensagem podes colocar uma questão, escrever um haiku ou treinar o teu código. Vamos responder-te tão rápido que nem precisas de fazer ping.</p>

        <?php
          gravity_form( 10, false, false, false, array(
            "nombrecurso" => $GLOBALS['product_name'], "idcurso" => $GLOBALS['product_id'], "idpais" => "1", "modollegada" => $MLL,
            "producto_tipo" => $GLOBALS['product_type'], "producto_categoria" => $GLOBALS['product_category'], "prelead_name" => $GLOBALS['product_name'], "prelead_tipo" => $GLOBALS['page_type'],
            "pdf" => get_field("catalogo")
          ), true, 1000);
        ?>

      </div>
		  <div class="col-lg-5 order text-center">

			</div>
		</div>
	</div>
</section>

<section id="redes-sociales" class="bg-tokio-navyblue">
	<div class="container text-center py-5 py-md-6 bg-navyblue-pico-top-left">
    <div class="row">
      <div class="col-lg-9 mx-auto">
        <h2 class="text-tokio-green mb-4 wow animate__fadeInUp" data-wow-duration="2s">Tokio School sempre on</h2>
        <p class="text-white mb-5">Segue a Tokio School nas redes sociais e conecta-te à nossa comunidade de profissionais do setor tecnológico. Comenta e partilha!</p>
      </div>
      <div class="col-lg-6 mx-auto">
				<div class="row justify-content-center">
					<div class="col-2 col-md-2 text-center px-3">
						<a href="https://www.facebook.com/tokioschoolportugal  " target="_blank" class=""><img src="<?php echo get_template_directory_uri() ?>/img/facebook-pink.svg" class="img-fluid"/></a>
					</div>
					<div class="col-2 col-md-2 text-center px-3">
						<a href=" https://www.instagram.com/tokioschoolportugal" target="_blank" class=""><img src="<?php echo get_template_directory_uri() ?>/img/instagram-pink.svg" class="img-fluid"/></a>
					</div>
					<div class="col-2 col-md-2 text-center px-3">
						<a href="https://www.linkedin.com/company/tokioschoolportugal" target="_blank" class=""><img src="<?php echo get_template_directory_uri() ?>/img/linkedin-pink.svg" class="img-fluid"/></a>
					</div>
					<div class="col-2 col-md-2 text-center px-3">
						<a href="https://www.youtube.com/channel/UC2y3CnU6sV6bRkBSDZOWfvw" target="_blank" class=""><img src="<?php echo get_template_directory_uri() ?>/img/youtube-pink.svg" class="img-fluid"/></a>
					</div>
				</div>
			</div>
    </div>
	</div>
</section>



<section id="trabaja-con-nosotros" class="background-contacto-2">
	<div class="container py-5 py-md-6">
		<div class="row">
			<div class="col-lg-5 text-center">
			</div>
			<div class="col-lg-7 text-left form-contacto">

				<h2 class="text-tokio-navyblue mb-4 wow animate__fadeInUp" data-wow-duration="2s">Trabalha connosco</h2>
				<p class="text-tokio-black mb-4">Gostavas de trabalhar na nossa escola? Simples: deixa-nos uma mensagem em que fales de ti e da tua experiência. Vamos adorar conhecer-te!</p>
				<?php echo do_shortcode( '[gravityform id="3" title="false" description="false" ajax="true" tabindex="300"]' ); ?>

			</div>
		</div>
	</div>
</section>

<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAo5wQLFXDyTcr-ZXSE2fvElmZ4K1kURsg&amp;callback=googleMap"></script> -->
