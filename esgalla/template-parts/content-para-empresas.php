<?php

/**

* Template part for displaying page content in page-para-empresas.php

*

* @package esgalla

*/



?>



<div>

<header id="masthead" class="site-header fullheight position-relative header-empresas">

	<div class="container-fluid bg-tokio-navyblue full-height-container h-100 pt-5">
		<nav aria-label="breadcrumb">
			<div class="container">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a class="text-white" href="<?php echo get_home_url( ) ?>">Inicio</a></li>
					<li class="breadcrumb-item text-secondary active" aria-current="page"><?php echo get_the_title() ?></li>
				</ol>
			</div>
		</nav>
		<div class="container full-height-container h-100 pt-4 pt-md-5">
			<div class="row align-items-center h-100">
				<div class="col-lg-6 align-self-center text-left">
					<h1 class="text-secondary masthead-title mb-4 wow animate__fadeInUp" data-wow-duration="2s">Somos criadores de talento digital</h1>
          <p class="text-white masthead-lead mb-4">Apresentamos-te a nossa rede de redes: a <strong>Tokio Net</strong>. É que a nova era digital não tem limites e, para quem souber aproveitar as suas oportunidades, estas também não têm limites! Na nossa Tokio Net <strong>pomos os nossos alunos e alumnis em contacto com empresas que estão desejosas de incorporar talento</strong>. Também abrimos as nossas portas (e janelas!) a todos aqueles que desejem <strong>colaborar connosco</strong>: colaborações pontuais, patrocínios, influencers, profissionais tech com muito para acrescentar… 
          </p>
          <p class="text-white masthead-lead">
		  Porque em Tokio não criamos redes: somos a rede.
          </p>
				</div>
				<div class="col-lg-6 text-center text-lg-right" >
					<!-- <div id="parallaxhoverx">
						<div data-depth="0.2"> -->
							<img src="<?php echo get_template_directory_uri() ?>/img/hero_empresas.jpg" class="img-fluid"/>
						<!-- </div>
					</div> -->
				</div>
			</div>
		</div>
	</div>

</header><!-- #masthead -->

</div>







<section class="bg-white mt-1 mt-lg-5">
	<div class="container py-4 py-md-6">
		<div class="row pt-lg-3">

			<!--<div class="col-md-4 text-center mb-4">
				<div class="h1 text-secondary pb-2 pb-md-4 animated-counter" data-counter="+1500" data-counter-prefix="+">0</div>
				<div class="h4 text-secondary">alunos ao ano</div>
			</div>-->

			<div class="col-md-4 text-center ml-auto mb-4">
				<div class="h1 text-secondary pb-2 pb-md-4 animated-counter" data-counter="15" data-counter-prefix="">0</div>
				<div class="h4 text-secondary">especialidades em novas tecnologias</div>
			</div>

			<div class="col-md-4 text-center mr-auto mb-4">
				<div class="h1 text-secondary pb-2 pb-md-4 animated-counter" data-counter="+3000" data-counter-prefix="+">0</div>
				<div class="h4 text-secondary">empresas parceiras</div>
			</div>

		</div>
	</div>
</section>





<section id="encuentra-profesionales" class="">
	<div class="container py-5 py-md-6">
		<div class="row d-flex align-items-center">

			<div class="col-lg-6 pr-lg-6">
				<h2 class="text-tokio-navyblue letras-chinas-4 mb-4 mb-md-5 wow animate__fadeInUp" data-wow-duration="2s">Procuras novos talentos?</h2>
				<p class="text-tokio-black mb-5">Precisas de <strong>contratar um profissional do setor TIC?</strong> Na Tokio School conectamos as empresas aos perfis que mais se adequam às suas necessidades. Os nossos alunos contam com um alto nível de formação especializada e estão prontos para iniciar a sua carreira profissional.</p>

				<p>Mais de 3.000 empresas já colaboram connosco e todos temos um objetivo comum: valorizar o talento e o emprego de qualidade no setor das novas tecnologias.</p>

				<p>A nossa escola está sempre à frente e por isso a nossa formação está em contínua atualização.</p>

				<a class="btn btn-tokio-navyblue rounded-pill py-3 px-4 mb-5" href="/tokio/contacto/">Entra em contacto connosco</a>
			</div>

			<div class="col-lg-6 text-center">
				<img src="<?php echo get_template_directory_uri() ?>/img/buscas_incorporar_talento.jpg" class="img-fluid border border-4 border-tokio-pink rounded-circle p-3"/>
			</div>

		</div>
	</div>
</section>





<section id="hiring-partners" class="">
	<div class="container text-center py-5 py-md-6">
		<div class="row">
			<div class="col-lg-8 mx-auto">
				<h2 class="text-tokio-navyblue mb-4 wow animate__fadeInUp" data-wow-duration="2s">Tokio & Co.</h2>
				<p class="text-tokio-black mb-5">
				Mais de 3.000 empresas já colaboram connosco. Todos temos um objetivo comum: valorizar o talento e o <strong>emprego de qualidade no setor das novas tecnologias.</strong>.
				</p>
			</div>
			<div class="col-12">
				<div class="row">
					<div class="col-12">
						<div class="carousel-logos-empresas" data-flickity='{ "wrapAround": true,"contain": true, "pageDots": false, "freeScroll": false, "autoPlay":"500","imagesLoaded": true, "prevNextButtons": true}'>
							<?php
							if(get_field('hiring_partners')):
								foreach (get_field('hiring_partners') as $logo) {
									echo '<div class="carousel-cell">'.wp_get_attachment_image($logo['ID'], 'thumbnail', null, array('class' => 'logo-empresa img-fluid', 'alt' => $logo['title'])).'</div>';
								}
							?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section id="formacion-online" class="">
	<div class="container py-5 py-md-6">
		<div class="row mb-md-4">
			<div class="col-md-9 mx-md-auto text-md-center">
				<h2 class="text-tokio-navyblue mb-4 mb-md-5 wow animate__fadeInUp" data-wow-duration="2s">Razões para contratar os nossos alunos</h2>
			</div>
		</div>

		<?php

			if(have_rows('motivos_alumnos')): while(have_rows('motivos_alumnos')): the_row(); ?>

			<div class="row">

				<div class="col-md-4 flex-column align-items-md-center text-center mb-5 px-md-4">
					<div class="bg-white h-100 metodologia-card px-4 pt-6 pb-4">
						<?php echo wp_get_attachment_image(get_sub_field('imagen'), 'medium', null, array('class' => 'img-fluid mb-5')) ?>
						<h3 class="h4 text-tokio-navyblue mb-3"><?php echo get_sub_field('titulo') ?></h3>
						<p class="mb-3 px-2 px-md-2"><?php echo get_sub_field('descripcion') ?></p>
					</div>
				</div>

			</div>

		<?php endwhile; else:	?>

				<!--- Escritorio -->
				<div class="row d-none d-md-flex">
					<div class="col-md-4 flex-column align-items-md-center text-center mb-5 px-md-4">
						<div class="bg-white h-100 metodologia-card px-4 pt-4 pb-4">
							<img src="<?php echo get_template_directory_uri() ?>/img/samurais_digitales.svg" class="img-fluid mb-3 mw-200"/>
							<h3 class="h4 text-tokio-navyblue mb-3">Samurais digitais</h3>
							<p class="mb-3 px-2 px-md-2">Os nossos alunos acabam a formação com projetos reais que põem à prova as suas capacidades e aprendizagem</p>
						</div>
					</div>
					<div class="col-md-4 flex-column align-items-md-center text-center mb-5 px-md-4">
						<div class="bg-white h-100 metodologia-card px-4 pt-4 pb-4">
							<img src="<?php echo get_template_directory_uri() ?>/img/formacion_especializada.svg" class="img-fluid mb-3 mw-200"/>
							<h3 class="h4 text-tokio-navyblue mb-3">Formação especializada</h3>
							<p class="mb-3 px-2 px-md-2">Os nossos professores são especialistas reputados no setor. Especialistas que formam especialistas.</p>
						</div>
					</div>
					<div class="col-md-4 flex-column align-items-md-center text-center mb-5 px-md-4">
						<div class="bg-white h-100 metodologia-card px-4 pt-4 pb-4">
							<img src="<?php echo get_template_directory_uri() ?>/img/always_forward_net.svg" class="img-fluid mb-3 mw-200"/>
							<h3 class="h4 text-tokio-navyblue mb-3">#alwaysfoward</h3>
							<p class="mb-3 px-2 px-md-2">A nossa escola está sempre à frente e por isso a nossa formação está em atualização contínua.</p>
						</div>
					</div>
				</div>

				<!--- Móvil - Carrousel -->
				<div class="row d-md-none">
					<div class="carousel carousel-metodologia-mobile" data-flickity='{ "imagesLoaded": false, "prevNextButtons": true, "pageDots": false }'>
						<div class="carousel-cell">
							<div class="col-md-4 flex-column align-items-md-center text-center mb-5 px-md-4">
								<div class="bg-white h-100 metodologia-card px-4 pt-4 pb-4">
									<img src="<?php echo get_template_directory_uri() ?>/img/samurais_digitales.svg" class="img-fluid mb-3 mw-200"/>
									<div class="h4 text-tokio-navyblue mb-3">Samurais digitais</div>
									<p class="mb-3 px-2 px-md-2">Os nossos alunos acabam a formação com projetos reais que põem à prova as suas capacidades e aprendizagem.</p>
								</div>
							</div>
						</div>
						<div class="carousel-cell">
							<div class="col-md-4 flex-column align-items-md-center text-center mb-5 px-md-4">
								<div class="bg-white h-100 metodologia-card px-4 pt-4 pb-4">
									<img src="<?php echo get_template_directory_uri() ?>/img/formacion_especializada.svg" class="img-fluid mb-3 mw-200"/>
									<div class="h4 text-tokio-navyblue mb-3">Formação especializada</div>
									<p class="mb-3 px-2 px-md-2">Os nossos professores são especialistas reputados no setor. Especialistas que formam especialistas.</p>
								</div>
							</div>
						</div>
						<div class="carousel-cell">
							<div class="col-md-4 flex-column align-items-md-center text-center mb-5 px-md-4">
								<div class="bg-white h-100 metodologia-card px-4 pt-4 pb-4">
									<img src="<?php echo get_template_directory_uri() ?>/img/always_forward_net.svg" class="img-fluid mb-3 mw-200"/>
									<div class="h4 text-tokio-navyblue mb-3">#alwaysfoward</div>
									<p class="mb-3 px-2 px-md-2">A nossa escola está sempre à frente e por isso a nossa formação está em atualização contínua</p>
								</div>
							</div>
						</div>
					</div>
				</div>

		<?php endif; ?>

		</div>
	</div>
</section>



<section id="alcanza-objetivos" class="">
	<div class="container py-5 pb-6 py-md-6">
		<div class="row align-items-center">
			<div class="col-md-6 order-md-last">
				<h2 class="text-tokio-navyblue letras-chinas-5 mb-4 mb-md-5 wow animate__fadeInUp" data-wow-duration="2s">O que podemos fazer pela tua empresa?</h2>
				<div id="accordion-como-lo-hacemos" class="accordion custom-accordion d-none d-md-block">
					<div class="card border-0 bg-white mb-0">
						<div class="accordion-item active">
							<div class="card-header border-0 bg-white pl-4 pt-4" data-toggle="collapse" href="#collapseOne2" aria-expanded="true">
								<a class="card-title h5"> Contratar profissionais tech </a>
							</div>
							<div id="collapseOne2" class="collapse p-0 collapse show" data-parent="#accordion-como-lo-hacemos">
								<div class="card-body titilumregular">
								Através da nossa <strong>Tokio Net</strong> conectamos empresas e profissionais procurando perfis que se adaptem a cada necessidade.
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<div class="card-header border-0 bg-white collapsed pl-4 pt-4" data-toggle="collapse" data-parent="#accordion-como-lo-hacemos" href="#collapseTwo2">
								<a class="card-title h5"> Encontrar profissionais para realizar estágios  </a>
							</div>
							<div id="collapseTwo2" class="collapse p-0 collapse" data-parent="#accordion-como-lo-hacemos">
								<div class="card-body titilumregular">
								Ao finalizar a sua formação, os nossos estudantes  estão preparados para realizar entre 60 a 300 horas de estágios relacionados com a área de estudos em que se formaram.
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<div class="card-header border-0 bg-white collapsed pl-4 pt-4" data-toggle="collapse" data-parent="#accordion-como-lo-hacemos" href="#collapseThree2">
								<a class="card-title h5">Intercâmbios publicitários </a>
							</div>
							<div id="collapseThree2" class="collapse p-0" data-parent="#accordion-como-lo-hacemos">
								<div class="card-body titilumregular"> 
								Fazemos colaborações publicitárias. A nossa equipa de marketing entrará em contacto contigo para a realização de inserções ou outro tipo de colaborações.
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<div class="card-header border-0 bg-white collapsed pl-4 pt-4" data-toggle="collapse" data-parent="#accordion-como-lo-hacemos" href="#collapseFour2">
								<a class="card-title h5">Ajudas personalizadas </a>
							</div>
							<div id="collapseFour2" class="collapse p-0" data-parent="#accordion-como-lo-hacemos">
								<div class="card-body titilumregular">
								Conta-nos como podemos ajudar a tua empresa e colocar o nosso expertise profissional e formativo ao vosso serviço.
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="slider-como-lo-hacemos" class="d-md-none">
					<div class="" data-flickity='{ "contain": true, "pageDots": false }'>
						<div class="carousel-cell">
							<h3 class="h5">Contratar profissionais tech</h3>
							<p class="">
							Conectamos as empresas e os profissionais procurando os perfis que mais se adaptem a cada necessidade através da nossa <strong>Tokio Net</strong>.
							</p>
						</div>
						<div class="carousel-cell">
							<h3 class="h5">Encontrar profissionais para realizar estágios</h3>
							<p class="">
							Os nossos estudantes, ao finalizar a sua formação, estão preparados para realizar de 60 a 300 horas de estágio relacionado com a área de estudos em que se tenham formado.
							</p>
						</div>
						<div class="carousel-cell">
							<h3 class="h5">Intercâmbios publicitários </h3>
							<p class="">
							Fazemos colaborações publicitárias. A nossa equipa de marketing entrará em contacto contigo para a realização de inserções ou outro tipo de colaborações.
							</p>
						</div>
						<div class="carousel-cell">
							<h3 class="h5">Ajudas personalizadas</h3>
							<p class="">
							Fazemos colaborações publicitárias. A nossa equipa de marketing entrará em contacto contigo para a realização de inserções ou outro tipo de colaborações.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 mb-5 mb-md-0 text-center text-lg-left pt-5 pt-md-3">
				<img src="<?php echo get_template_directory_uri() ?>/img/hacer_por_tu_empresa.jpg" class="img-fluid border border-4 border-tokio-pink rounded-circle p-3"/>
			</div>
		</div>
	</div>
</section>

<!-- <section id="opiniones" class="bg-white">

	<div class="container-fluid py-5 pb-6 px-lg-6 ">



		<div class="row">

			<div class="col-md-12 text-center">



				<?php

				$empresarios[0] = [

					// 'img' => 'https://www.tokioschool.com/wp-content/themes/esgalla/img/juan-sanchez-ceo-repsol.jpg',

					'texto' => 'Dolor sunt magna eu proident dolore in dolore cupidatat labore in elit voluptate magna ipsum. Dolor sunt magna eu proident dolore in dolore cupidatat labore in elit voluptate magna ipsum.',

					'nombre' => 'Juan Sánchez',

					'cargo' => 'CEO de Repsol',

				];

				$empresarios[0]["imagen"]["sizes"]["medium"]='https://www.tokioschool.com/wp-content/themes/esgalla/img/juan-sanchez-ceo-repsol.jpg';

				?>

				<?php get_template_part('template-parts/components/component', 'opiniones-alumnos', $empresarios); ?>



			</div>

		</div>



	</div>

</section> -->



<?php get_template_part( 'template-parts/components/component', 'form-empresas') ?>

<section id="conecta-empresa" class="">

	<div class="container pt-5 pt-md-6">
		<div class="row align-items-center">
			<div class="col-md-5 text-center text-lg-left bg-image-circle-left d-none d-md-block">
				<img src="<?php echo get_template_directory_uri() ?>/img/letras_chinas_3.svg" class="img-fluid"/>
			</div>
			<div class="col-lg-6 text-left">
				<h2 class="text-tokio-navyblue mb-4 wow animate__fadeInUp" data-wow-duration="2s">Vamos trabalhar juntos?</h2>
				<p class="titilumregular mb-4">Encontrar o perfil perfeito, transmitir uma formação ou encontrar apoio ajustado ao que pretendes é agora mais simples. Partilha connosco o que procuras através deste formulário. 
</p>
				<form>
					<div class="form-group sf-field sf-text sf-nombre-empresa" data-fitgt="gf-nombre-empresa" data-formtgt="11">
						<input class="form-control form-control-lg" type="text" name="empresas-nombre" id="empresas-nombre" placeholder="Nome da empresa*">
						<div class="sf-error"></div>
					</div>
					<div class="form-group sf-field sf-text sf-persona-contacto" data-fitgt="gf-persona-contacto" data-formtgt="11">
						<input class="form-control form-control-lg" type="text" name="empresas-persona" id="empresas-persona" placeholder="Nome*">
						<div class="sf-error"></div>
					</div>
					<div class="form-group sf-field sf-mail sf-mail-empresa" data-fitgt="gf-mail-empresa" data-formtgt="11">
						<input class="form-control form-control-lg" type="email" name="empresas-correo" id="empresas-correo" placeholder="Email*">
						<div class="sf-error"></div>
					</div>
					<div class="form-group sf-field sf-phone sf-tlf-empresa" data-fitgt="gf-tlf-empresa" data-formtgt="11">
						<input class="form-control form-control-lg" type="number" name="empresas-telefono" id="empresas-telefono" placeholder="Telefone*">
						<div class="sf-error"></div>
					</div>
					<div class="form-group sf-field sf-select sf-prestacion">
						<select name="empresas-prestacion" id="empresas-prestacion" class="form-control form-control-lg">
							<option value="" selected disabled>O que procuras*</option>
							<option value="experiencia">Procuro alguém com experiência</option>
							<option value="practicas">Procuro alguém para estagiar </option>
							<option value="otro">Outras colaborações</option>
						</select>
						<div class="sf-error"></div>
					</div>
					<div class="form-group mt-4">
						<div class="form-check sf-field sf-checkbox sf-legal mb-2">
							<input class="form-check-input" type="checkbox" value="" id="legal-check">
							<label class="form-check-label" for="legal-check">
							Li e aceito a <a href="#">Política de Privacidade</a>
							</label>
							<div class="sf-error"></div>
						</div>
						<div class="form-check sf-promociones mb-2">
							<input class="form-check-input" type="checkbox" value="" id="nl-check">
							<label class="form-check-label" for="nl-check">
							Quero receber informação sobre os cursos e promoções
							</label>
							<div class="sf-error"></div>
						</div>
					</div>
					<div class="form-group mt-4">
						<?php /* <button type="submit" class="btn btn-lg rounded-pill btn-primary">Continuar</button> */ ?>
						<a class="btn rounded-pill btn-lg btn-tokio-navyblue sf-paraempresas-abrir" role="button" href="javascript:void(0)">Enviar formulário</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>