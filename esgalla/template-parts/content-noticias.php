<?php

/**

 * Template part for displaying page content in page-noticias.php

 *

 * @package esgalla

 */



?>



<header id="masthead" class="site-header fullheight position-relative bg-tokio-navyblue">



		<div class="container full-height-container h-100 py-4 pb-5 pt-md-6 pb-md-5 big-post">

			<div class="row align-items-center h-100">

				<div class="col d-flex rounded align-self-center text-center big-post-col" >

          <div class="big-post-col-header p-4 wow animate__fadeInUp" data-wow-duration="2s">

            <h1 class="h2 text-white">Notícias</h1>

            <span class="h3 text-tokio-navyblue font-italic">#alwaysfoward</span>

          </div>



				</div>

			</div>

		</div>

    <div class="container">

      <div class="row">

        <div class="col-sm-9 mx-auto mb-5 mb-lg-0">

          <div class="rounded shadow bg-white text-tokio-navyblue font-weight-bold text-center big-post-excerpt p-5 p-lg-5">

            Assim como nossos alunos, também as nossas novidades estão sempre à frente. Descobre tendências, avanços e curiosidades sobre o sector das tecnologias e o mundo que está por vir.

          </div>

        </div>

      </div>

    </div>





</header><!-- #masthead -->






<? $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>

  <section id="posts-destacados">

    <div class="container-lg-fluid py-5 py-md-6 pt-lg-7 pl-lg-0">
      <? if(1 == $paged): ?>
        <div class="container">

          <h2 class="h3 text-tokio-navyblue mb-4">Notícias em destaque</h2>

        </div>

        <div class="row row-carousel">

          <div class="col pl-lg-0">

            <div class="carousel posts-carousel py-3" data-flickity='{ "freeScroll": false, "contain": true, "prevNextButtons": false, "pageDots": false }'>



            <?php

              $last_posts_ids = [];

              $args = array(

                'posts_per_page' => 6,

                'post_type'      => 'post',

                'post__not_in'   => '',

              );

              $last_posts = new WP_Query( $args );

              while ( $last_posts->have_posts() ) : $last_posts->the_post(); ?>

                <div class="carousel-cell">

                  <?php

                    $last_posts_ids[] = get_the_ID();

                    $post_card = [

                      'post_img' => get_the_post_thumbnail_url(null, 'medium'),

                      'post_title' => get_the_title(),

                      'post_excerpt' => get_the_excerpt(),

                      'post_link' => get_the_permalink(),

                    ];

                  ?>

                  <?php ec_get_template_part('template-parts/components/component', 'post-card', $post_card); ?>

                </div>

              <?php endwhile; ?>
              <? wp_reset_query(); ?>		


            </div>

          </div>

        </div>
      <? endif; ?>

    </div>

  </section>




<section>

	<div class="container mb-4">

		<div class="row">

			<div class="col">

				<div class="h3 text-tokio-navyblue mb-4">Procura notícias por...</div>

				<div class="row">

					<div class="col-md-4 col-lg-3">

						<div class="form-group ">

							<?php echo do_shortcode('[facetwp facet="noticias_categorias"]') ?>

						</div>

					</div>

					<div class="col-md-4 col-lg-3">

						<div class="form-group ">

							<?php echo do_shortcode('[facetwp facet="noticias_meses"]') ?>

						</div>

					</div>


					<div class="col-md-4 col-lg-3">

						<!-- <div class="form-group search-bar">

							<input type="text" class="form-control" id="search" placeholder="Palabra clave...">

							<img src="<?php echo get_template_directory_uri() ?>/img/search.svg" class="img-fluid icon-search">

            </div> -->

            <div class="form-group ">

              <?php echo do_shortcode('[facetwp facet="searchbar"]') ?>

            </div>

					</div>

				</div>

			</div>

		</div>

	</div>

</section>



<section id="posts-grid">

  <div class="container">

    <div class="row posts-container">

		 <?php

		 $args = array(

			 'posts_per_page' => 12,

			 'post_type'      => 'post',

			 'paged'          => get_query_var( 'paged' ),

			 'post__not_in'   => $last_posts_ids,

			 'cat' => get_queried_object()->term_id,

			 'facetwp' => true,

		 );

		 $wp_query = new WP_Query( $args );

		 while ( $wp_query->have_posts() ) : $wp_query->the_post();

	 			$post_card = [

	 			  'post_img' => get_the_post_thumbnail_url(null, 'medium'),

	 			  'post_title' => get_the_title(),

	 			  'post_excerpt' => get_the_excerpt(),

	 			  'post_link' => get_the_permalink(),

	 			];

	 			echo '<div class="col-md-6 col-lg-4 mb-5">';

	 			ec_get_template_part('template-parts/components/component', 'post-card', $post_card);

	 		echo '</div>';

	 	endwhile; ?>




    </div>


    <div class="row">

      <div style="display:none!important;" class="hidden-pagination">
        <? wpbeginner_numeric_posts_nav(); ?>
      </div>
      <? wp_reset_query(); ?>

      <div class="col text-center mt-5 mt-md-6">

        <!-- <a class="btn btn-primary py-3 px-4" href="#">Ver más noticias</a> -->

		  <?php echo do_shortcode( '[facetwp facet="noticias_ver_ms"]' ); ?>

      </div>

    </div>

  </div>

</section>

<section id="nube-tags">
  <div class="container pt-5 pt-lg-6 pb-lg-5">
    <h2 class="text-tokio-navyblue mb-4 wow animate__fadeInUp" data-wow-duration="2s">Em Tóquio falamos sobre...</h2>
    <div class="row">
      <div class="col">
        <div class="single-post-badges py-1">
          <?php
            foreach (get_categories() as $categoria) {
              echo '<span class="badge badge-tokio-green mr-3 mt-3"><a class="text-white" href="' . get_category_link( $categoria ) . '">'.$categoria->cat_name.'</a></span>';
            }
          ?>
        </div>
      </div>
    </div>
  </div>
</section>