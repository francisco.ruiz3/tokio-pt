<?php

  // Component variables - Post card

  $post_img = (get_query_var('post_img')!='') ? get_query_var('post_img') : get_template_directory_uri().'/img/post-image-sample.jpg' ;

  $post_title = get_query_var('post_title');

  $post_excerpt = get_query_var('post_excerpt');

  $post_link = get_query_var('post_link');

?>



<div class="post-card shadow m-2">

  <div class="post-image-bg bg-img-holder">

    <img src="<?php echo $post_img; ?>" class="img-fluid"/>

  </div>

  <div class="post-content bg-white d-flex flex-column px-4 py-4">

    <div class="">

      <h4 class="post-title"><?php echo $post_title; ?></h4>

      <p class="post-excerpt" style="max-height:3rem; overflow:hidden"><?php echo $post_excerpt; ?></p>

    </div>

  </div>

</div>
