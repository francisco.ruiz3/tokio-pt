<?php
// print_r(get_query_var( 0 ));
  $opiniones = wp_parse_args( $args );
  // Component variables - Opinion alumno
  $alumno_img = get_query_var('img');
  $alumno_cita = get_query_var('cita');
  $alumno_nom = get_query_var('nom');
  $alumno_puesto = get_query_var('puesto');
?>
<div class="component component-opiniones-alumnos bg-tokio-navyblue px-4 px-lg-6 py-5">
  <div class="carousel carousel-opiniones-alumnos" data-flickity>
<?php foreach ($opiniones as $opinion) { ?>
	<div class="carousel-cell text-center text-sm-left px-4">
		<div class="row">
		  <div class="col-md-3 text-center text-md-left d-none  d-md-flex align-items-center">
			 <img src="<?php echo $opinion["imagen"]["sizes"]["medium"]; ?>" class="img-fluid rounded" />
		  </div>
		  <div class="col-md-9 col-lg-7 pr-2 pl-4 pt-5 pb-2 content-opinion">
			 <div class="h4 mb-4 text-white cita-alumno"><?php echo $opinion["texto"]; ?></div>
			 <span class="text-header text-secondary nom-alumno"><?php echo $opinion["nombre"]; ?>.</span>&nbsp;<span class="text-white"><?php echo $opinion["cargo"]; ?></span>
		  </div>
		</div>
	 </div>
<? } ?>
  </div>
</div>
