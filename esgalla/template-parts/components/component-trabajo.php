<?php

  // Component variables - Trabajo

  $post_img = get_query_var('post_img');

  $post_avatar = get_query_var('post_avatar');

  $post_nombre = get_query_var('post_nombre');

  $post_title = get_query_var('post_title');

  $post_description = get_query_var('post_description');

  $post_excerpt = get_query_var('post_subtitle');


?>



<div class="card trabajo-card bg-transparent shadow mb-3 mr-3">

  <img src="<?php echo $post_img; ?>" class="card-img-top" style="height: 270px; object-fit: cover"/>

  <div class="card-body d-flex flex-column flex-md-row justify-content-between align-items-center px-4 pt-2 pb-3 py-md-4">

    <div class="d-flex justify-content-between align-items-center align-self-start align-self-md-center py-3 py-md-0">
      <img src="<?php echo $post_avatar; ?>" class="img-fluid rounded-circle" width="34"/>

      <div class="pl-3 font-weight-light"><span class="text-dark"><?php echo $post_nombre; ?> </span> <?php echo $post_subtitle; ?></div>
    </div>


    <a class="text-tokio-green font-weight-bold align-self-end align-self-md-center" href="javascript:void(0)" data-toggle="modal" data-target="#project-<?php echo sanitize_title( $post_nombre ) ?>">Ver proyecto</a>

  </div>

</div>
