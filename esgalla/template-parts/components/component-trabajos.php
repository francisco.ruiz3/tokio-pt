<?php

  // Component variables - Trabajo

  $post_img = get_query_var('post_img');

  $post_title = get_query_var('post_title');

  $post_excerpt = get_query_var('post_subtitle');

  $post_link = get_query_var('post_link');

?>



<div class="post-card shadow mb-3">

  <div class="post-image-bg bg-img-holder">

    <img src="<?php echo $post_img; ?>" class="img-fluid"/>

  </div>

  <div class="post-content bg-white d-flex flex-column px-4 py-4">

    <div class="">

      <h4 class="post-title"><?php echo $post_title; ?></h4>

      <p class="post-excerpt"><?php echo $post_excerpt; ?></p>

    </div>

    <div class="mt-auto">

      <a href="<?php echo $post_link; ?>" class="text-primary font-weight-bold">Ver proyecto</a>

    </div>

  </div>

</div>