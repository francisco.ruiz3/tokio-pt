<?php
  // Component variables - Trabajo
  $post_img = get_query_var('post_img');
  $post_avatar = get_query_var('post_avatar');
  $post_nombre = get_query_var('post_nombre');
  $post_title = get_query_var('post_title');
  $post_description = get_query_var('post_description');
  $post_excerpt = get_query_var('post_subtitle');
?>


    <a class="text-primary font-weight-bold align-self-end align-self-md-center" href="javascript:void(0)" data-toggle="modal" data-target="#project-<?php echo sanitize_title( $post_nombre ) ?>">Ver proyecto</a>
