<? foreach(get_field('carrera_profesional_asociada') as $carrera_id):

  $banner_data = get_field('banner_carreras_profesionales', $carrera_id);

?>

<div class="container py-5 py-md-6 ">

  <div class="row align-items-center">

    <div class="col-md-6">

      <div class="mb-3 mb-lg-5">
        <span class="formacion-dato mr-3"><img src="<?php echo get_template_directory_uri() ?>/img/award.svg" class="img-fluid"/> <?php echo (get_field('tipo', $carrera_id)!='') ? get_field('tipo', $carrera_id) : 'Carreira' ?></span>
        <span class="formacion-dato mr-3"><img src="<?php echo get_template_directory_uri() ?>/img/timer.svg" class="img-fluid"/> <?php echo (get_field('duracion', $carrera_id)!='') ? get_field('duracion', $carrera_id) : '300' ?>h</span>
      </div>

      <h2 class="h2 text-tokio-navyblue mb-4 mb-md-5"><?php echo $banner_data['titulo_banner']; ?></h2>

      <p class="text-tokio-black mb-5 mt-4 mt-md-0"><?php echo $banner_data['descripcion_banner']; ?></p>

      <div class="d-flex flex-column flex-lg-row mb-5">

        <a class="btn btn-tokio-navyblue rounded-pill mb-3 mb-lg-0 py-3 px-4 mr-lg-4" href="<?php echo get_permalink($carrera_id); ?>">Mais Informações</a>

      </div>

    </div>

    <div class="col-md-6 text-lg-right">

      <img src="<?php echo $banner_data['imagen_banner']['url'] ?>" class="img-fluid border border-4 border-tokio-pink rounded-circle p-3"/>

    </div>

  </div>

</div>

<? endforeach; ?>