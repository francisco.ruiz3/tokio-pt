<?php



  // Component variables - Post card



  $post_img = (get_query_var('post_img')!='') ? get_query_var('post_img') : get_template_directory_uri().'/img/post-image-sample.jpg' ;



  $post_title = get_query_var('post_title');



  $post_excerpt = get_query_var('post_excerpt');



  $post_link = get_query_var('post_link');



?>







<div class="post-card h-100 m-2">



  <div class="post-image-bg bg-img-holder">



    <img src="<?php echo $post_img; ?>" class="img-fluid" alt="<?php echo $post_title; ?>"/>



  </div>



  <div class="post-content bg-white d-flex flex-column px-4 py-4">



    <div class="post-content-first">



      <h3 class="h4 post-title text-primary"><?php echo $post_title; ?></h3>



      <p class="post-excerpt" style="max-height:3rem; overflow:hidden"><?php echo $post_excerpt; ?></p>



    </div>



    <div class="mt-auto">



      <a href="<?php echo $post_link; ?>" class="text-tokio-green font-weight-bold stretched-link">Continuar a ler</a>



    </div>



  </div>



</div>

