<?php
  // Component variables - Opinion alumno
  $video_url = get_query_var('video_url');
  $video_descripcion = get_query_var('video_descripcion');
?>

<div class="component component-popup-video">
  <a class="video-btn" data-toggle="modal" data-src="<?php echo $video_url; ?>" data-target="#videoModal">
    <div class="video-cover d-flex align-items-center justify-content-center">
      <img src="https://www.tokioschool.com/wp-content/themes/esgalla/img/icon_play.png" class="play-icon" alt="Play video">
    </div>
  </a>
  <!-- Modal -->
  <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <!-- 16:9 aspect ratio -->
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://vimeo.com/500847752/626c576d05" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php if($video_descripcion): ?>
    <div class="container-fluid bg-tokio-green text-white video-description">
      <div class="container bg-green-pico-top-left py-4 py-lg-5">
        <p class="text-white h5 mb-0"><?php echo $video_descripcion; ?></p>
      </div>
    </div>
  <?php endif; ?>
</div>
