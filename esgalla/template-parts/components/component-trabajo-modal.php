<?php
$post_avatar = get_query_var('post_avatar');
$post_nombre = get_query_var('post_nombre');
$post_cargo = get_query_var('post_cargo');
$post_titulo = get_query_var('post_titulo');
$post_descripcion = get_query_var('post_descripcion');
$post_galeria = get_query_var('post_galeria');
$post_video = get_query_var('post_video');
?>
<!-- Modal -->
<div class="modal modal-proyectos fade" id="project-<?php echo sanitize_title( $post_nombre ) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content pb-5">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<?php
				if($post_video!=''){
					echo '<div class="carousel carousel-project-'.sanitize_title( $post_nombre ).'" data-flickity=\'{ "freeScroll": true, "imagesLoaded": false, "contain": true, "prevNextButtons": false, "pageDots": false }\'>';
					echo '<div class="carousel-cell"><iframe class="embed-responsive-item" src="https://vimeo.com/500847752/626c576d05" id="video"  allowscriptaccess="always" allow="autoplay" width="100%" height="100%"></iframe></div>';
				}
				else{
					echo '<div class="carousel carousel-project-'.sanitize_title( $post_nombre ).'" data-flickity=\'{ "freeScroll": true, "imagesLoaded": false, "contain": true, "prevNextButtons": true, "pageDots": true }\'>';
					foreach ($post_galeria as $imagen) {
						echo '<div class="carousel-cell text-center">';
							echo '<img class="img-fluid" src="'.$imagen['url'].'" />';
						echo '</div>';
					}
				}
				?>
				</div>

				<div class="h3 text-tokio-navyblue mt-5 py-3 tit-modal-project"><?php echo $post_titulo ?></div>
				<p class="text-tokio-black"><?php echo $post_descripcion ?></p>
					<div class="d-flex justify-content-start align-items-center pt-3">
						<img src="<?php echo $post_avatar; ?>" class="img-fluid rounded-circle" width="34"/>
						<div class="pl-3"><span class="text-tokio-green"><?php echo $post_nombre ?> </span><span class="text-tokio-navyblue"><?php echo $post_cargo ?></span></div>
					</div>
				</div>
			</div>

		</div>
	</div>
