<?php
	$form = isset($GLOBALS["form"]) ? $GLOBALS["form"] : "";
	$producto_banner = isset($GLOBALS["producto_banner"]) ? $GLOBALS["producto_banner"] : "";
	$producto_id = isset($GLOBALS["producto_id"]) ? $GLOBALS["producto_id"] : "";
	$producto_id_wp = isset($GLOBALS["producto_id_wp"]) ? $GLOBALS["producto_id_wp"] : "";
	$producto_name = isset($GLOBALS["producto_name"]) ? $GLOBALS["producto_name"] : "";
	$producto_cat = isset($GLOBALS["producto_cat"]) ? $GLOBALS["producto_cat"] : "";
	$pagina_name = isset($GLOBALS["pagina_name"]) ? $GLOBALS["pagina_name"] : "";

	// $id_formulario = ($producto_id=="opos-000") ? 13 : 11;
	$id_formulario = 11;

	$paso_num = 1;
?>

<div class="target-sf" style="position:absolute;width:0;height:0;overflow:hidden">
<!-- <div class="target-sf"> -->
	<?php
	gravity_form($id_formulario, false, false, false, array(
    "producto_name" => $producto_name, 
    "producto_cat" => $producto_cat, 
    "producto_id" => $producto_id, 
    "producto_id_wp" => $producto_id_wp, 
    "producto_extension" => $GLOBALS["producto_extension"],
		"form_variant" => "form-empresas", "form_seccion" => $form,
		"pagina_name" => $pagina_name,
	), true, ($id_formulario*100), true);
	?>
</div>
<style><?php echo "#gform_".$id_formulario ?> .gfield_label{display: none}</style>

<div class="popup-sf modal fade modal-contacto form-contacto show" id="modal-paraempresas-experiencia">
	<div class="popup-sf-contenedor modal-dialog modal-dialog-centered" role="document">
		<div class="popup-sf-contain modal-content d-flex align-items-center">
			<div class="modal-header align-self-end">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="sf sf-formlead modal-body" data-parent="modal-paraempresas-experiencia" data-formtgt="<?php echo $id_formulario ?>">
				<div class="sf-container">
					<!--Timeline-->
					<div class="sf-timeline d-none">
						<div class="sf-timeline-container">
						</div>
					</div>

					<!--form-->
					<div class="sf-forms">
						<div class="sf-forms-container">

							<!--step 1-->
							<div class="sf-form" data-buscas="experiencia">

								<div class="sf-form-container">

									<div class="sf-form-header">
										<div class="h3 mb-4">Detalles sobre el servicio</div>
									</div>

									<div class="sf-form-form">
										<form>
											<div class="sf-fields">
												<ul class="sf-fields-container">

													<li class="sf-field sf-text sf-puesto sf-required mb-2" data-fitgt="gf-puesto">
														<div class="sf-field-container">
															<input name="input_puesto" id="input_puesto" type="text" value="" placeholder="Puesto a ocupar*" tabindex="9001">
														</div>
														<div class="sf-error"></div>
													</li>

													<li class="sf-field sf-text sf-requisitos sf-required mb-2" data-fitgt="gf-requisitos">
														<div class="sf-field-container">
															<input name="input_requisitos" id="input_requisitos" type="text" value="" placeholder="Requisitos principales*" tabindex="9002">
														</div>
														<div class="sf-error"></div>
													</li>

													<li class="sf-field sf-text sf-funciones sf-required mb-2" data-fitgt="gf-funciones">
														<div class="sf-field-container">
															<input name="input_funciones" id="input_funciones" type="text" value="" placeholder="Funciones a desempeñar*" tabindex="9003">
														</div>
														<div class="sf-error"></div>
													</li>

													<li class="sf-field sf-text sf-jornada sf-required mb-2" data-fitgt="gf-jornada">
														<div class="sf-field-container">
															<input name="input_jornada" id="input_jornada" type="text" value="" placeholder="Tipo de jornada*" tabindex="9004">
														</div>
														<div class="sf-error"></div>
													</li>

													<li class="sf-field sf-text sf-ciudad sf-required mb-2" data-fitgt="gf-ciudad">
														<div class="sf-field-container">
															<input name="input_ciudad" id="input_ciudad" type="text" value="" placeholder="Ciudad del puesto vacante*" tabindex="9005">
														</div>
														<div class="sf-error"></div>
													</li>
												</ul>
											</div>

											<div class="sf-nav">
												<div class="sf-nav-container">
													<input class="sf-nav-next sf-nav-btn btn btn-lg btn-primary rounded-pill" type="submit" value="Siguiente">
												</div>
											</div>
										</form>
									</div>

								</div>

							</div>


							<div class="sf-form">

								<div class="sf-form-container">

									<div class="sf-form-header">
										<div class="h3 mb-4">Otros requisitos</div>
										<p class="my-4">¿Necesitas contarnos algo más sobre el perfil que estás buscando?</p>
									</div>

									<div class="sf-form-form">
										<form>
											<div class="sf-fields">
												<ul class="sf-fields-container">

													<li class="sf-field sf-text sf-observaciones sf-required mb-2" data-fitgt="gf-observaciones">
														<div class="sf-field-container">
															<textarea name="text_requisitos" id="text_requisitos" placeholder="Puesto a ocupar" tabindex="9001"></textarea>
														</div>
														<div class="sf-error"></div>
													</li>
												</ul>
											</div>

											<div class="sf-nav">
												<div class="sf-nav-container">
													<input class="sf-nav-prev sf-nav-btn btn btn-lg rounded-pill" type="submit" value="&laquo; Anterior">
													<input class="sf-nav-submit sf-nav-btn btn btn-lg btn-primary rounded-pill" type="submit" data-tipoenvio="inline" value="Enviar">
												</div>
											</div>
										</form>
									</div>

								</div>

							</div>

							<!--step 5-->
							<div class="sf-form d-none" data-accion="thankyou">

								<div class="sf-form-container">

									<!-- <div class="sf-thx-plane">
										<svg width="185px" height="91px" viewBox="0 0 185 91" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										        <g id="paperplane-icon" transform="translate(-2.000000, 2.000000)" stroke="#84B5BC" stroke-width="4">
										            <polygon id="Path-3" stroke-linejoin="round" points="122 46.9318631 129 72 135 52 185 0.564516129"></polygon>
										            <polygon id="Path-4" stroke-linejoin="round" points="135 51.8422939 170.507246 65.5645161 185 0.564516129"></polygon>
										            <polyline id="Path-5" stroke-linejoin="round" points="185 0.564516129 93 37 121.854839 46.5645161"></polyline>
										            <path d="M129,72 L142,55" id="Path-6" stroke-linejoin="round"></path>
										            <path d="M122,76 C105.165312,92 86.498645,90.3333333 66,71 C45.501355,51.6666667 23.501355,53.3333333 0,76" id="Path-7" stroke-linecap="round" stroke-dasharray="6,12"></path>
										        </g>
										    </g>
										</svg>
									</div>

									<div class="sf-thx-spinner">
										<i class="fas fa-spinner fa-pulse"></i>
									</div> -->

									<div class="sf-form-header sf-thx-msj">
										<div class="sf-form-header-title">¡Enhorabuena, tu formulario se ha enviado correctamente!</div>
										<div class="sf-form-header-description">Se pondrán en contacto contigo muy pronto para informarte<br /><br /><strong>¡Gracias por confiar en Tokio School!</strong></div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>
			</div><!--form-->
		</div><!--Popup contain-->
	</div><!--Popup container-->
</div>





<div class="popup-sf modal fade modal-contacto form-contacto show" id="modal-paraempresas-practicas">
	<div class="popup-sf-contenedor modal-dialog modal-dialog-centered" role="document">
		<div class="popup-sf-contain modal-content d-flex align-items-center">
			<div class="modal-header align-self-end">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="sf sf-formlead modal-body" data-parent="modal-paraempresas-practicas" data-formtgt="<?php echo $id_formulario ?>">
				<div class="sf-container">
					<!--Timeline-->
					<div class="sf-timeline d-none">
						<div class="sf-timeline-container">
						</div>
					</div>

					<!--form-->
					<div class="sf-forms">
						<div class="sf-forms-container">

							<div class="sf-form d-none" data-buscas="practicas">

								<div class="sf-form-container">

									<div class="sf-form-header mb-4">
										<div class="h3">Detalles sobre las practicas</div>
									</div>

									<div class="sf-form-form">
										<form>
											<div class="sf-fields">
												<ul class="sf-fields-container">

													<li class="sf-field sf-text sf-puesto sf-required mb-2" data-fitgt="gf-puesto">
														<div class="sf-field-container">
															<input name="input_puesto" id="input_puesto" type="text" value="" placeholder="Puesto a ocupar*" tabindex="9001">
														</div>
														<div class="sf-error"></div>
													</li>

													<li class="sf-field sf-text sf-funciones sf-required mb-2" data-fitgt="gf-funciones">
														<div class="sf-field-container">
															<input name="input_funciones" id="input_funciones" type="text" value="" placeholder="Funciones principales*" tabindex="9003">
														</div>
														<div class="sf-error"></div>
													</li>

													<li class="sf-field sf-text sf-jornada sf-required mb-2" data-fitgt="gf-jornada">
														<div class="sf-field-container">
															<input name="input_jornada" id="input_jornada" type="text" value="" placeholder="Tipo de jornada*" tabindex="9004">
														</div>
														<div class="sf-error"></div>
													</li>

													<li class="sf-field sf-text sf-ciudad sf-required mb-2" data-fitgt="gf-ciudad">
														<div class="sf-field-container">
															<input name="input_ciudad" id="input_ciudad" type="text" value="" placeholder="Ciudad del puesto vacante*" tabindex="9005">
														</div>
														<div class="sf-error"></div>
													</li>
												</ul>
											</div>

											<div class="sf-nav">
												<div class="sf-nav-container">
													<input class="sf-nav-next sf-nav-btn btn btn-lg btn-primary rounded-pill" type="submit" value="Siguiente">
												</div>
											</div>
										</form>
									</div>

								</div>

							</div>


							<div class="sf-form">

								<div class="sf-form-container">

									<div class="sf-form-header">
										<div class="h3 mb-4">Observaciones</div>
										<p class="my-4">¿Necesitas contarnos algo más sobre el perfil que estás buscando?</p>
									</div>

									<div class="sf-form-form">
										<form>
											<div class="sf-fields">
												<ul class="sf-fields-container">

													<li class="sf-field sf-text sf-observaciones sf-required mb-2" data-fitgt="gf-observaciones">
														<div class="sf-field-container">
															<textarea name="text_requisitos" id="text_requisitos" placeholder="Busco a alguien que..." tabindex="9001"></textarea>
														</div>
														<div class="sf-error"></div>
													</li>
												</ul>
											</div>

											<div class="sf-nav">
												<div class="sf-nav-container">
													<input class="sf-nav-prev sf-nav-btn btn btn-lg rounded-pill" type="submit" value="&laquo; Anterior">
													<input class="sf-nav-submit sf-nav-btn btn btn-lg btn-primary rounded-pill" type="submit" data-tipoenvio="inline" value="Enviar">
												</div>
											</div>
										</form>
									</div>

								</div>

							</div>

							<!--step 5-->
							<div class="sf-form d-none" data-accion="thankyou">

								<div class="sf-form-container">

									<!-- <div class="sf-thx-plane">
										<svg width="185px" height="91px" viewBox="0 0 185 91" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										        <g id="paperplane-icon" transform="translate(-2.000000, 2.000000)" stroke="#84B5BC" stroke-width="4">
										            <polygon id="Path-3" stroke-linejoin="round" points="122 46.9318631 129 72 135 52 185 0.564516129"></polygon>
										            <polygon id="Path-4" stroke-linejoin="round" points="135 51.8422939 170.507246 65.5645161 185 0.564516129"></polygon>
										            <polyline id="Path-5" stroke-linejoin="round" points="185 0.564516129 93 37 121.854839 46.5645161"></polyline>
										            <path d="M129,72 L142,55" id="Path-6" stroke-linejoin="round"></path>
										            <path d="M122,76 C105.165312,92 86.498645,90.3333333 66,71 C45.501355,51.6666667 23.501355,53.3333333 0,76" id="Path-7" stroke-linecap="round" stroke-dasharray="6,12"></path>
										        </g>
										    </g>
										</svg>
									</div>

									<div class="sf-thx-spinner">
										<i class="fas fa-spinner fa-pulse"></i>
									</div> -->

									<div class="sf-form-header sf-thx-msj">
										<div class="sf-form-header-title">¡Enhorabuena, tu formulario se ha enviado correctamente!</div>
										<div class="sf-form-header-description">Se pondrán en contacto contigo muy pronto para informarte<br /><br /><strong>¡Gracias por confiar en Tokio School!</strong></div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>
			</div><!--form-->
		</div><!--Popup contain-->
	</div><!--Popup container-->
</div>







<div class="popup-sf modal fade modal-contacto form-contacto show" id="modal-paraempresas-otro">
	<div class="popup-sf-contenedor modal-dialog modal-dialog-centered" role="document">
		<div class="popup-sf-contain modal-content d-flex align-items-center">
			<div class="modal-header align-self-end">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="sf sf-formlead modal-body" data-parent="modal-paraempresas-otro" data-formtgt="<?php echo $id_formulario ?>">
				<div class="sf-container">
					<!--Timeline-->
					<div class="sf-timeline d-none">
						<div class="sf-timeline-container">
						</div>
					</div>

					<!--form-->
					<div class="sf-forms">
						<div class="sf-forms-container">
							<div class="sf-form" data-buscas="otro">

								<div class="sf-form-container">

									<div class="sf-form-header">
										<div class="h3 mb-4">¿Te gustaría colaborar con nosotros?</div>
										<p class="my-4">Estamos abiertos a todo tipo de colaboraciones con empresas. Cuéntanos en qué estás pensando y buscaremos la forma de ayudarte.</p>
									</div>

									<div class="sf-form-form">
										<form>
											<div class="sf-fields">
												<ul class="sf-fields-container">

													<li class="sf-field sf-text sf-observaciones sf-required mb-2" data-fitgt="gf-observaciones">
														<div class="sf-field-container">
															<textarea name="text_requisitos" id="text_requisitos" placeholder="Me gustaría que..." tabindex="9001"></textarea>
														</div>
														<div class="sf-error"></div>
													</li>
												</ul>
											</div>

											<div class="sf-nav">
												<div class="sf-nav-container text-right">
													<input class="sf-nav-submit sf-nav-btn btn btn-lg rounded-pill btn-tokio-navyblue rounded-pill" type="submit" data-tipoenvio="inline" value="Enviar formulario">
												</div>
											</div>
										</form>
									</div>

								</div>

							</div>



							<!--step 2-->
							<div class="sf-form d-none">

								<div class="sf-form-container">

									<div class="sf-form-header">
										<div class="sf-form-header-title">Paso <?php echo $paso_num++; ?>: Datos personales</div>
										<div class="sf-form-header-description">Por favor, introduce tus datos personales.</div>
									</div>

									<div class="sf-form-form">
										<form>
											<div class="sf-fields">
												<ul class="sf-fields-container">

													<li class="sf-field sf-text sf-name sf-required" data-fitgt="gf-nombre">
														<div class="sf-field-container">
															<input name="input_name" id="input_name" type="text" value="" placeholder="Escribe aquí tu nombre" tabindex="9102">
														</div>
														<div class="sf-error"></div>
													</li>

													<li class="sf-field sf-input sf-phone sf-required" data-fitgt="gf-tlf">
														<div class="sf-field-container">
															<input name="input_phone" id="input_phone" type="tel" value="" placeholder="Escribe aquí tu teléfono" tabindex="9103">
														</div>
														<div class="sf-error"></div>
													</li>

													<li class="sf-field sf-input sf-mail sf-required" data-fitgt="gf-email">
														<div class="sf-field-container">
															<input name="input_email" id="input_email" type="email" value="" placeholder="Escribe aquí tu email" tabindex="9104">
														</div>
														<div class="sf-error"></div>
													</li>

													<li class="sf-field sf-number sf-age sf-required" data-fitgt="gf-edad">
														<div class="sf-field-container">
															<input name="input_age" id="input_age" type="number" min="18" max="90" value="" placeholder="Escribe aquí tu edad -18 a 90 años-" tabindex="9105">
														</div>
														<div class="sf-error"></div>
													</li>

													<li class="sf-field sf-select sf-study sf-required" data-fitgt="gf-estudios">
														<div class="sf-field-container">
															<select tabindex="9106" name="input_studies" id="input_studies"><option value="" selected="selected">Nivel de estudios</option><option value="41">GRADUADO ESCOLAR</option><option value="42">ESO</option><option value="43">FP GRADO MEDIO</option><option value="44">BACHILLER / COU</option><option value="45">FP GRADO SUPERIOR</option><option value="46">DIPLOMATURA / GRADO</option><option value="47">LICENCIATURA / GRADO</option></select>
														</div>
														<div class="sf-error"></div>
													</li>
												</ul>
											</div>

											<div class="sf-nav">
												<div class="sf-nav-container">
													<div class="sf-nav-prev sf-nav-btn rounded-pill">Anterior</div>
													<input class="sf-nav-next sf-nav-btn rounded-pill" type="submit" value="Siguiente" tabindex="9107">
												</div>
											</div>
										</form>
									</div>

								</div>

							</div>



							<!--step 5-->
							<div class="sf-form d-none" data-accion="thankyou">

								<div class="sf-form-container">

									<!-- <div class="sf-thx-plane">
										<svg width="185px" height="91px" viewBox="0 0 185 91" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										        <g id="paperplane-icon" transform="translate(-2.000000, 2.000000)" stroke="#84B5BC" stroke-width="4">
										            <polygon id="Path-3" stroke-linejoin="round" points="122 46.9318631 129 72 135 52 185 0.564516129"></polygon>
										            <polygon id="Path-4" stroke-linejoin="round" points="135 51.8422939 170.507246 65.5645161 185 0.564516129"></polygon>
										            <polyline id="Path-5" stroke-linejoin="round" points="185 0.564516129 93 37 121.854839 46.5645161"></polyline>
										            <path d="M129,72 L142,55" id="Path-6" stroke-linejoin="round"></path>
										            <path d="M122,76 C105.165312,92 86.498645,90.3333333 66,71 C45.501355,51.6666667 23.501355,53.3333333 0,76" id="Path-7" stroke-linecap="round" stroke-dasharray="6,12"></path>
										        </g>
										    </g>
										</svg>
									</div>

									<div class="sf-thx-spinner">
										<i class="fas fa-spinner fa-pulse"></i>
									</div> -->

									<div class="sf-form-header sf-thx-msj">
										<div class="sf-form-header-title">¡Enhorabuena, tu formulario se ha enviado correctamente!</div>
										<div class="sf-form-header-description">Se pondrán en contacto contigo muy pronto para informarte<br /><br /><strong>¡Gracias por confiar en Tokio School!</strong></div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>
			</div><!--form-->
		</div><!--Popup contain-->
	</div><!--Popup container-->
</div>
