jQuery(document).ready(function(){
    jQuery(".button-masterclass .elementor-button-link").removeAttr("href");
	if(jQuery(".sf-formlead").length){

		let sf_pasos_completados = sf_datos_rellenos = [];

		jQuery(".sf-formlead").each(function(index){

			var sf_id_contenedor = jQuery(this).data("parent");

			var $sf_contenedor = jQuery("#"+sf_id_contenedor);

			var sf_total_pasos = jQuery("#"+sf_id_contenedor+" .sf-forms .sf-form").length;

			var sf_formulario_objetivo = jQuery(this).data("formtgt");



			// Vaciamos el contenido del timeline en caso de existir

			$sf_contenedor.find(".sf-timeline-container").html("");



			// Recorremos cada uno de los contenedores para añadir y completar diferentes partes

			jQuery("#"+sf_id_contenedor+" .sf-forms .sf-form").each(function(index){

				//Lo primero que hacemos será añadir los pasos a .sf-timeline-container

				//Si es el primer paso, añadimos .current para que se rellene y forzamos mostrar la capa

				if(index == 0){

					$sf_contenedor.find(".sf-timeline-container").append('<li class="current" data-tlstep="'+(index+1)+'">'+(index+1)+'</li>');

					jQuery(this).removeClass("d-none");

				}

				//Si es el último paso no lo añadimos a la lista

				else if ((index+1) == sf_total_pasos) {

					jQuery(this).addClass("d-none");

				}

				//En cualquier otro caso, añadimos el número a la lista y ocultamos la capa

				else{

					$sf_contenedor.find(".sf-timeline-container").append('<li data-tlstep="'+(index+1)+'">'+(index+1)+'</li>');

					jQuery(this).addClass("d-none");

				}



				//El index parte desde 0 y nuestros pasos desde 1, por eso siguiente es +2 y anterior es index a secas

				//Añadimos el data-step al contenedor del formulario para localizarlo

				jQuery(this).attr("data-step", (index+1));

				//Añadimos el data-formtgt a cada contendor de campo para tenerlo a mano

				jQuery(this).find("li.sf-field").attr("data-formtgt", sf_formulario_objetivo);

				jQuery(this).find("li.sf-field").attr("data-formcrr", sf_id_contenedor);

				//Añadimos el data-next y data-prev a los botones correspondientes

				jQuery(this).find(".sf-nav-next").attr({"data-next": (index+2), "data-parent": sf_id_contenedor, "data-accion":jQuery(this).data("accion")});

				jQuery(this).find(".sf-nav-prev").attr({"data-prev": index, "data-parent": sf_id_contenedor});

				jQuery(this).find(".sf-nav-submit").attr({"data-next": (index+2), "data-parent": sf_id_contenedor, "data-formtgt": sf_formulario_objetivo});

				jQuery(this).find(".sf-nav-submit-check").attr({"data-next": (index+2), "data-parent": sf_id_contenedor, "data-formtgt": sf_formulario_objetivo});

				jQuery(this).find(".sf-nav-submit-empresas").attr({"data-next": (index+2), "data-parent": sf_id_contenedor, "data-formtgt": sf_formulario_objetivo});

			})



			//Comprobamos si el formulario objetivo tiene un campo .gf-cp

			if(jQuery("#gform_"+sf_formulario_objetivo+" li.gf-cp")){

				var $sf_formulario_objetivo_cp = jQuery("#gform_"+sf_formulario_objetivo+" li.gf-cp");



				// Añadimos el botón para poder generar Academias mediante API y su evento escuchador

				$sf_formulario_objetivo_cp.append('<div class="gfield-typeform-aceptar">generaClientes</a></div>');

				$sf_formulario_objetivo_cp.find(".gfield-typeform-aceptar").on('click',function(){

					recogerDatos(jQuery(this));

				});

			}

		});

	}





	jQuery(".sf-nav-next").on("click", function(e){

		// Prevenimos el comportamiento por defecto para evitar el envío del formulario

		e.preventDefault();



		var $contenedor_secciones = jQuery("#"+jQuery(this).data("parent"));

		var sf_paso_siguiente = jQuery(this).data("next");

		var sf_paso_actual = sf_paso_siguiente - 1;

		//Número de campos, en este contenedor, que no pasan validación

		var sf_campos_error = 0;



		//Para validar primero recorreremos los diferentes li y validaremos su contenido

		jQuery($contenedor_secciones).find(".sf-form[data-step='"+sf_paso_actual+"'] .sf-field").each(function(){

			sf_campos_error += sf_validar_campos(jQuery(this));

		})



		//Si no hay errores (sf_campos_error se mantiene a 0), pasamos de página

		if(sf_campos_error == 0){

			//Si todo ha salido bien, añadimos clase de verificación a este formulario, copiamos datos en gf lo escondemos y pasamos al siguiente



			// Enviamos analítica en los diferentes pasos

			if(jQuery(this).data("accion")=="sel-prod" && jQuery(this).data("tipoenvio")!="inline"){

				egtm_ecom_checkout_1();

				gtm_evento("gtm-event","Lead","Pasos Form Lateral","Paso 01 - Código Postal");

			}



			else if(jQuery(this).data("accion")=="userdata"){

				sf_gtm_evento('wAnalyticsEvent', 'formulario_lead', 'paso_07', "Academias".replace(/-/g, '_'));

				gtm_evento("gtm-event","Lead","Pasos Form Lateral","Paso 07 - Academias");

			}



			//Escondemos el paso actual, esperando a que acabe para iniciar la siguiente animación

			jQuery($contenedor_secciones).find(".sf-form[data-step='"+sf_paso_actual+"']").addClass("d-none").fadeOut().promise().done(function() {

				//Añadimos fondo al número del nuevo paso en .sf-timeline-container

				jQuery($contenedor_secciones).find(".sf-timeline li[data-tlstep='"+sf_paso_siguiente+"']").addClass("current");

				//Mostramos el paso nuevo

				jQuery($contenedor_secciones).find(".sf-form[data-step='"+sf_paso_siguiente+"']").removeClass("d-none");

			});

		}

	});



	jQuery(".sf-nav-prev").on("click", function(e){

		e.preventDefault();

		var $contenedor_secciones = jQuery("#"+jQuery(this).data("parent"));

		var sf_paso_anterior = jQuery(this).data("prev");

		var sf_paso_actual = sf_paso_anterior + 1;



		//Ir hacia atrás no implica ningún tipo de validación, por lo que no se incluye

		//Escondemos el paso actual, esperando a que acabe para iniciar la siguiente animación

		jQuery($contenedor_secciones).find(".sf-form[data-step='"+sf_paso_actual+"']").addClass("d-none").fadeOut("slow").promise().done(function() {

			//Eliminamos el fondo del paso en .sf-timeline-container

			jQuery($contenedor_secciones).find(".sf-timeline li[data-tlstep='"+sf_paso_actual+"']").removeClass("current");

			//Mostramos el paso nuevo

			jQuery($contenedor_secciones).find(".sf-form[data-step='"+sf_paso_anterior+"']").removeClass("d-none");

		});

	});





	jQuery(".sf-nav-submit").on("click", function(e){

		e.preventDefault();

		// return;

		sf_enviar_formulario(jQuery(this));

	});



	// Escuchamos los cambios en las academias ofrecidas, para cambiarlas en Gravity

	jQuery(document).on('change', '.sf-client input', function(e) {

		var sf_formulario_objetivo = jQuery(this).parent().parent().data("formtgt");

		var sf_cliente_en_gf = "#gform_"+sf_formulario_objetivo+" input[type=checkbox][value="+jQuery(this).val()+"]";

		jQuery(sf_cliente_en_gf).prop("checked",jQuery(this).prop("checked")).change();

	});







	// Escuchamos la pulsación en el botón que abre el formulario popup

	jQuery(document).on('click', '.form-popup-inicio', function (e) {



		gtm_var.form_variant = "form-popup";

		gtm_var.form_id = jQuery(this).data("formid");

		if(jQuery(this).data("pasoinicio")==0){

			gtm_var.producto_cat = "generica";

			gtm_var.producto_id = "opos-000";

			gtm_var.producto_name = "producto_generico";

			sf_gtm_evento('wAnalyticsEvent', 'formulario_lead', 'abrir_lead_form', gtm_var.producto_name.replace(/-/g, '_'));

			sf_gtm_evento('wAnalyticsEvent', 'formulario_lead', 'paso_00-01', "Categoría".replace(/-/g, '_'));

			gtm_evento("gtm-event","Lead","Pasos Form Lateral","Paso 00-01 - Categoría");

		}

		else if(jQuery(this).data("pasoinicio")==1){

			gtm_evento("gtm-event","Lead","Pasos Form Lateral","Paso 01 - Código Postal");

			sf_gtm_evento('wAnalyticsEvent', 'formulario_lead', 'abrir_lead_form', gtm_var.producto_name.replace(/-/g, '_'));

			sf_gtm_evento('wAnalyticsEvent', 'formulario_lead', 'paso_01', "Código Postal".replace(/-/g, '_'));

			egtm_ecom_checkout_1();

		}

	});









	// Escuchamos el cambio de categoría para: cambiarlo en gravity, rellenar el desplegable de productos y enviar analítica

	jQuery(".sf-categories select").on('change', function(){

		let sf_objetivo = jQuery(this).parent().parent().data("fitgt");

		let gf_objetivo = jQuery(this).parent().parent().data("formtgt");

		let $sf_objetivo = jQuery("."+sf_objetivo);



		let $gf_objetivo = jQuery("#gform_"+gf_objetivo+" .gf-select-product-cat select");

		$gf_objetivo.val(jQuery(this).val()).change();



		sf_rellena_selector_productos($sf_objetivo, jQuery(this).val());

		// $sf_objetivo.find("p").removeClass("d-none");

		$sf_objetivo.removeClass("d-none");

		// sf4_gtm_evento(gtm_tipo,gtm_ev_cat,gtm_ev_act,gtm_ev_lab, gtm_ecomm_obj = undefined)



		// gtm_var.producto_cat = jQuery(this).find("option[selected=selected]").html();

		gtm_var.producto_cat = slugify(jQuery(this)[0].options[jQuery(this)[0].selectedIndex].text.toLowerCase().replace(" ", "-"));





		if(jQuery(this).data("tipoenvio")=="inline") sf_gtm_evento('wAnalyticsEvent', 'formulario_lead', 'abrir_lead_form', gtm_var.producto_name.replace(/-/g, '_'));

		sf_gtm_evento('wAnalyticsEvent', 'formulario_lead', 'paso_00-01', "Categoría".replace(/-/g, '_'));

		gtm_evento("gtm-event","Lead","Pasos Form Lateral","Paso 00-01 - Categoría");

	});



	// Escuchamos el cambio de producto para cambiarlo en gravity y enviar analítica

	jQuery(".sf-selectedproduct select").on('change', function(){

		let gf_objetivo = jQuery(this).parent().parent().data("formtgt");

		let gf_objetivo_campo = jQuery(this).parent().parent().data("fitgt");

		let $gf_objetivo = jQuery("#gform_"+gf_objetivo);

		let $gf_objetivo_select = jQuery("#gform_"+gf_objetivo+" ."+gf_objetivo_campo+" select");

		let sf_producto_val = jQuery(this).val().split("|");



		$gf_objetivo_select.val(jQuery(this).val()).change();

		$gf_objetivo.find(".gf-producto-id input").val(sf_producto_val[1]);

		gtm_var.producto_id = jQuery(this).val().split("|")[1];



		$gf_objetivo.find(".gf-producto-name input").val(sf_producto_val[2]);

		gtm_var.producto_name = jQuery(this).val().split("|")[2];



		sf_gtm_evento('wAnalyticsEvent', 'formulario_lead', 'paso_00-02', "Producto".replace(/-/g, '_'));

		gtm_evento("gtm-event","Lead","Pasos Form Lateral","Paso 00-02 - Producto");

	});



	// Escuchamos los cambios de los diferentes campos de datos del usuario para enviar analítica

	jQuery(".sf-form[data-accion='userdata'] input").on("change", function(e){

		let nombre_campo = "";

		switch (jQuery(this).attr("id")) {

			case 'input_name':

			nombre_campo = 'Nombre'

			break;

			case 'input_phone':

			nombre_campo = 'Teléfono'

			break;

			case 'input_email':

			nombre_campo = 'Mail'

			break;

			case 'input_age':

			nombre_campo = 'Edad'

			break;

			default:

			break;

		}



		if(!sf_datos_rellenos.includes(jQuery(this).attr("id"))){

			sf_datos_rellenos.push(jQuery(this).attr("id"));

			// let paso_campo = parseInt(jQuery(this).attr("tabindex").substr(-1))+1;

			let paso_campo = jQuery(this).attr("tabindex").substr(-1);



			sf_gtm_evento('wAnalyticsEvent', 'formulario_lead', 'paso_0'+paso_campo, nombre_campo.replace(/-/g, '_'));

			gtm_evento("gtm-event","Lead","Pasos Form Lateral","Paso 0"+paso_campo+" - "+nombre_campo);

		}

	});



	jQuery(".sf-form[data-accion='userdata'] select").on("change", function(e){

		let nombre_campo = "Nivel de estudios";



		if(!sf_datos_rellenos.includes(jQuery(this).attr("id"))){

			sf_datos_rellenos.push(jQuery(this).attr("id"));

			// let paso_campo = parseInt(jQuery(this).attr("tabindex").substr(-1))+1;

			let paso_campo = jQuery(this).attr("tabindex").substr(-1);



			sf_gtm_evento('wAnalyticsEvent', 'formulario_lead', 'paso_0'+paso_campo, nombre_campo.replace(/-/g, '_'));

			gtm_evento("gtm-event","Lead","Pasos Form Lateral","Paso 0"+paso_campo+" - "+nombre_campo);

		}

	});



});



















function sf_rellena_selector_productos($sf_objetivo,val){

	let elegida = limpiar_cadena(val);

	let sf_selector_productos = $sf_objetivo.find("select");



	//Mapeamos las categorías

	if(elegida == "oposiciones-administracion-y-archivo"){

		productos = catalogo_productos.oposiciones_administracion_y_archivo;

	}else if(elegida == "oposiciones-educacion"){

		productos = catalogo_productos.oposiciones_educacion;

	}else if(elegida == "oposiciones-salud"){

		productos = catalogo_productos.oposiciones_salud;

	}else if(elegida == "oposiciones-justicia"){

		productos = catalogo_productos.oposiciones_justicia;

	}else if(elegida == "oposiciones-hacienda"){

		productos = catalogo_productos.oposiciones_hacienda;

	}else if(elegida == "oposiciones-prisiones-y-fuerzas-de-seguridad"){

		productos = catalogo_productos.oposiciones_prisiones_y_fuerzas_de_seguridad;

	}else if(elegida == "oposiciones-union-europea"){

		productos = catalogo_productos.oposiciones_union_europea;

	}else if(elegida == "oposiciones-forestales"){

		productos = catalogo_productos.oposiciones_forestales;

	}else if(elegida == "oposiciones-aduanas"){

		productos = catalogo_productos.oposiciones_aduanas;

	}else if(elegida == "otras-oposiciones"){

		productos = catalogo_productos.otras_oposiciones;

	}



	var productos_ordenados =  ordernar_productos(productos);



	//Rellenamos los campos

	sf_selector_productos.val("");

	sf_selector_productos.html("");

	sf_selector_productos.append('<option value="" selected="selected">Selecciona una opo</option>');

	productos_ordenados.forEach(function(producto) {

		var producto_split = producto.split("|");

		sf_selector_productos.append('<option value="'+producto+'">'+producto_split[2]+'</option>');

	});



	//Añadimos la info a sus campos

	// gf_producto_cat.val(select);

	gtm_var.producto_cat = elegida;

}







function sf_validar_campos($sf_contenedor_campo){

	//Por defecto no validamos, por si se nos escapa algún campo

	var error = 1;



	if($sf_contenedor_campo.hasClass("sf-cp")){

		var msj_error = "Este código postal no parece válido";

		var pattern =  "^\\d{5}$";

		error = sf_validar_regex($sf_contenedor_campo,msj_error,pattern);



		//Si el campo se ha validado, llevamos el valor a GF y obtenemos el resultado de las Academias

		if(error == 0){

			sf_enviar_cp($sf_contenedor_campo);

		}

	}



	else if($sf_contenedor_campo.hasClass("sf-phone")){

		var msj_error = "Este número de teléfono no parece válido";

		var pattern = "[0-9]{9}";

		error = sf_validar_regex($sf_contenedor_campo,msj_error,pattern);

	}



	else if($sf_contenedor_campo.hasClass("sf-mail")){

		var msj_error = "Este email no parece válido";

		var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

		error = sf_validar_regex($sf_contenedor_campo,msj_error,pattern);

	}



	else if($sf_contenedor_campo.hasClass("sf-age")){

		var msj_error = "Esta edad no parece válida";

		error = sf_validar_num($sf_contenedor_campo,msj_error);

	}



	else if($sf_contenedor_campo.hasClass("sf-number")){

		error = sf_validar_num($sf_contenedor_campo,msj_error);

	}



	else if($sf_contenedor_campo.hasClass("sf-text")){

		error = sf_validar_txt($sf_contenedor_campo,msj_error);

	}



	else if($sf_contenedor_campo.hasClass("sf-select")){

		error = sf_validar_select($sf_contenedor_campo,msj_error);

	}



	else if($sf_contenedor_campo.hasClass("sf-checkbox")){

		msj_error = "Por favor, selecciona al menos una opción";

		if($sf_contenedor_campo.hasClass("sf-client")) var msj_error = "Por favor, elige alguna de estas academías";

		error = sf_validar_check($sf_contenedor_campo,msj_error);

	}



	else if($sf_contenedor_campo.hasClass("sf-radio")){

		error = sf_validar_radio($sf_contenedor_campo,msj_error);

	}



	// Mensaje de depuración en caso de necesidad

	if(error == 1){

		let nombre_campo = $sf_contenedor_campo.find("input").attr("id");

		// if(nombre_campo==undefined) nombre_campo = $sf_contenedor_campo.find("select").attr("id");

		// if(nombre_campo==undefined) nombre_campo = $sf_contenedor_campo.find("checkbox").attr("id").substr(-2);



		// sf_gtm_evento("gtm-event","Lead","solicitud_error","error_"+$sf_contenedor_campo.data("formcrr").replace(/-/g, '_')+"_"+nombre_campo);

		// sf_gtm_evento('wAnalyticsEvent', 'formulario_lead', 'solicitud_error', "error_"+$sf_contenedor_campo.data("formcrr").replace(/-/g, '_')+"_"+nombre_campo);

		console.log("el campo del contenedor '"+$sf_contenedor_campo[0].className+"' no ha pasado la validación");

	}



	//Devolvemos el valor del error

	return error;

}



function sf_validar_txt($field_container, msj_error){

	//Extraemos el valor del campo

	var val = ($field_container.find("input").length>0) ? $field_container.find("input").val() : $field_container.find("textarea").val();



	//Comprobamos que el input tenga contenido

	if(val){

		sf_rellenar_gf($field_container, "input", val)

		var error = sf_error_hide($field_container);

	}else{

		var error = sf_error_show($field_container, msj_error, val);

	}



	//Devolvemos el valor del error

	return error;

}



function sf_validar_num($field_container, msj_error){

	//Extraemos el valor del campo y los valores máximo y mínimo

	var val = $field_container.find("input").val();

	var max = $field_container.find("input").attr("max");

	var min = $field_container.find("input").attr("min");



	//Comprobamos que el número sea mayor que el valor mínimo y menor que el valor máximo

	if(parseInt(val) <= parseInt(max) && parseInt(val) >= parseInt(min)){

		sf_rellenar_gf($field_container, "input", val)

		var error = sf_error_hide($field_container);

	}else{

		var error = sf_error_show($field_container, msj_error, val);

	}



	//Devolvemos el valor del error

	return error;

}



function sf_validar_regex($field_container, msj_error, pattern){

	//Extraemos el valor del campo

	var val = $field_container.find("input").val();



	//Comprobamos que el valor del campo concuerda con el patrón de validación

	if(val.match(pattern)){

		sf_rellenar_gf($field_container, "input", val)

		var error = sf_error_hide($field_container);

	}

	else{

		var error = sf_error_show($field_container, msj_error, val);

	}



	//Devolvemos el valor del error

	return error;

}



function sf_validar_select($field_container, msj_error){

	//Extraemos el valor del campo

	var val = $field_container.find("select").val();



	//Comprobamos que el input tenga contenido

	if(val){

		sf_rellenar_gf($field_container, "select", val)

		var error = sf_error_hide($field_container);

	}else{

		var error = sf_error_show($field_container, msj_error, val);

	}



	//Devolvemos el valor del error

	return error;

}



function sf_validar_check($field_container,msj_error){

	let val = "";

	//Comprobamos que al menos hayas elegido 1 opción

	if($field_container.find("input:checked").length){

		//Guardamos cada una de las opciones marcadas para rellenar el campo

		$field_container.find("input:checked").each(function(){

			val += jQuery(this).val()+",";

		});

		//Eliminamos la coma final con .slice(0,-1)

		sf_rellenar_gf($field_container, "input", val.slice(0,-1));

		var error = sf_error_hide($field_container);

	}

	else{

		var error = sf_error_show($field_container, msj_error);

	}



	return error;

}



function sf_validar_radio($field_container,msj_error){

	var msj_error = "Selecciona una opción";

	//Comprobamos que al menos hayas elegido 1 opción

	if($field_container.find("input:checked").length){

		var error = sf_error_hide($field_container);

	}

	else{

		var error = sf_error_show($field_container, msj_error);

	}



	return error;

}



function sf_error_hide($field_container){

	// Escondemos el mensaje de error en caso de estar visible

	$field_container.find(".sf-error").removeClass("sf-error-on text-danger").addClass("d-none").html();

	return error = 0;

}



function sf_error_show($field_container, msj_error, val){

	// Definimos mensaje de error para campos vacíos

	msj_error = (val == "" || val == null) ? "Este campo es obligatorio" : msj_error;

	// Mostramos el mensaje de error

	$field_container.find(".sf-error").addClass("sf-error-on text-danger").removeClass("d-none").html(msj_error);

	return error = 1;

}



function sf_rellenar_gf($field_container, field_type, val){

	console.log("rellenando!");

	console.log($field_container);

	console.log(val);

	// Recogemos la clase del contenedor objetivo

	var target_field = $field_container.data("fitgt");

	if(target_field){

		// Recogemos el ID del formulario objetivo

		var target_form = $field_container.data("formtgt");



		var $target_container = jQuery("#gform_"+target_form+" ."+target_field);

		// Si existe el contenedor objetivo dentro del formulario objetivo...

		if($target_container.length>0){

			// Asignamos el valor en el formulario de Gravity

			$target_container.find(field_type).val(val);

			// Simulamos el comportamiento de cambio para que Gravity sea consciente y pueda validar

			$target_container.find(field_type).trigger("change");

		}

	}

}





function sf_enviar_cp($field_container){

	var val = $field_container.find("input").val();

	var current_form = $field_container.data("formcrr");

	var target_form = $field_container.data("formtgt");

	var target_field = $field_container.data("fitgt");

	var $target_container = jQuery("#gform_"+target_form+" ."+target_field);



	// Evitamos que haga el procesado si hay varios producto-id (separados por ,)

	if(jQuery("#gform_"+target_form).find(".gf-producto-id input").val().indexOf(",")<0){

		jQuery("#gform_"+target_form).find(".gf-clientes ul").removeClass("clientes-modificados");



		// Si existe el contenedor objetivo...

		if($target_container.length>0){

			// El campo ya está relleno con la función sf_rellenar_gf()

			// Esperamos 500ms y simulamos click en el botón que hace la llamada a la API de academias

			setTimeout(function(){

				$target_container.find(".gfield-typeform-aceptar").click();

			},500);



			// Iniciamos el proceso para buscar cambios en el contenedor de las academias en el formulario real

			sf_recuperar_academias(current_form, target_form, 0);

		}

	}

}



function sf_recuperar_academias(current_form, target_form, pruebas_clientes){

	var sf_contenedor_clientes_modificados = jQuery("#gform_"+target_form+" .gf-clientes .clientes-modificados");

	var $sf_contenedor_spinner_clientes = jQuery("#"+current_form+" .sf-client-spinner");

	var $sf_contenedor_clientes = jQuery("#"+current_form+" .sf-client");



	$sf_contenedor_spinner_clientes.removeClass("d-none");

	$sf_contenedor_clientes.addClass("d-none");

	// Buscamos el contenedor de clientes cuando se ha modificado

	if(sf_contenedor_clientes_modificados.length>0){

		var valor_cliente = nombre_cliente = sf_contenido_clientes = "";

		var sf_index_cliente = 0;

		var $sf_contenedor_clientes = jQuery("#"+current_form+" .sf-client");

		var $sf_contenedor_error_clientes = jQuery("#"+current_form+" .sf-form-header-client-error");



		// Devolvemos la visibilidad al contenedor de las opciones

		$sf_contenedor_clientes.removeClass("d-none");

		// Devolvemos la visibilidad a la descripción

		$sf_contenedor_error_clientes.parent().find(".sf-form-header-description").removeClass("d-none");

		// Ocultamos el mensaje de error

		$sf_contenedor_error_clientes.addClass("d-none");

		// Ocultamos el spinner de la animación de carga

		$sf_contenedor_spinner_clientes.addClass("d-none");

		$sf_contenedor_clientes.removeClass("d-none");



		// Recorremos cada una de sus opciones

		sf_contenedor_clientes_modificados.find("li input").each(function(index){

			valor_cliente = jQuery(this).val();

			nombre_cliente = jQuery(this).attr("placeholder");

			sf_index_cliente = index + 1;

			// En caso de haber generado un "sin-academias"

			if(valor_cliente=="sin-academias"){

				// Mostramos el contenedor del error

				$sf_contenedor_error_clientes.removeClass("d-none");

				// Añadimos el código postal correspondiente al mensaje de error

				$sf_contenedor_error_clientes.find("strong span").html(jQuery("#"+current_form+" .sf-cp input").val());

				// Escondemos el contenedor de las academias

				$sf_contenedor_clientes.addClass("d-none");

				// Escondemos el mensaje de descripción que nos dice que seleccionemos una de la lista

				$sf_contenedor_error_clientes.parent().find(".sf-form-header-description").addClass("d-none");

				// Creamos una única opción en la lista de academias con el valor "sin-academias"

				sf_contenido_clientes += '<div class="sf-checkbox-container"><input name="input_client.'+index+'" id="option_client_'+index+'" type="checkbox" value="'+valor_cliente+'" checked="checked" tabindex="920'+index+'"><label for="option_client_'+index+'">'+valor_cliente+'</label></div>';

			}

			// En cualquier otro caso

			else{

				// Añadimos esta opción al listado de academias

				sf_contenido_clientes += '<div class="sf-checkbox-container"><input name="input_client.'+index+'" id="option_client_'+index+'" type="checkbox" value="'+valor_cliente+'" checked="checked" placeholder="" tabindex="920'+index+'"><label for="option_client_'+index+'">'+nombre_cliente+'</label></div>';

			}

		});

		// Añadimos el contenedor del error, por si el visitante no selecciona academias

		sf_contenido_clientes += '<div class="sf-error"></div>';

		$sf_contenedor_clientes.html(sf_contenido_clientes);

	}

	// Si, por cualquier motivo, no se han generado los cliente modificados volvemos a intentar cada 800ms hasta 5 intentos

	else{

		pruebas_clientes++;

		if(pruebas_clientes<5) setTimeout(sf_recuperar_academias, 800, current_form, target_form, pruebas_clientes);

	}

}







function sf_enviar_formulario($sf_boton_envio, sf_paso_siguiente){

	console.log($sf_boton_envio);

	var $contenedor_secciones = jQuery("#"+$sf_boton_envio.data("parent"));

	console.log($contenedor_secciones);

	// sf_paso_siguiente = (sf_paso_siguiente!="") sf_paso_siguiente ? : $sf_boton_envio.data("next");

	sf_paso_siguiente = $sf_boton_envio.data("next");

	var sf_paso_actual = sf_paso_siguiente - 1;

	var sf_formulario_objetivo = $sf_boton_envio.data("formtgt");

	console.log(sf_formulario_objetivo);

	var sf_tipo_envio = $sf_boton_envio.data("tipoenvio");



	// Creamos un user-id en caso de no existir para usar como ID de transaccion

	if(jQuery("#gform_"+sf_formulario_objetivo+" .gf-user-id input").val()=="0000"){

		var d = new Date();

		var sf_fecha = d.getFullYear().toString()+(d.getMonth()+1).toString()+d.getDate().toString()+"-"+d.getHours().toString()+d.getMinutes().toString()+d.getSeconds().toString();

		var sf_user_id_alternativo = gtm_var.producto_id+"-"+jQuery("#gform_"+sf_formulario_objetivo+" .gf-tlf input").val().substr(jQuery("#gform_"+sf_formulario_objetivo+" .gf-tlf input").val().length - 5)+"-"+sf_fecha;

		var sf_user_id = (gtm_var.user_id != undefined) ? gtm_var.user_id : sf_user_id_alternativo;

		jQuery("#gform_"+sf_formulario_objetivo+" .gf-user-id input").val(sf_user_id);

		gtm_var.user_id = sf_user_id;

	}



	// Simulamos el click en el formulario de Gravity

	var $form = jQuery("#gform_"+sf_formulario_objetivo);

	console.log($form);

	$form.trigger('submit');



	//Escondemos el paso actual, esperando a que acabe para iniciar la siguiente animación

	jQuery($contenedor_secciones).find(".sf-form[data-step='"+sf_paso_siguiente+" .sf-thx-msj']").addClass("d-none");

	// Escondemos el timeline de pasos

	// jQuery($contenedor_secciones).find(".sf-timeline").addClass("d-none");

	jQuery($contenedor_secciones).find(".sf-timeline").fadeOut().addClass("d-none");



	//Escondemos el paso actual, esperando a que acabe para iniciar la siguiente animación

	jQuery($contenedor_secciones).find(".sf-form[data-step='"+sf_paso_actual+"']").addClass("d-none").fadeOut().promise().done(function() {

		//Mostramos el paso nuevo

		jQuery($contenedor_secciones).find(".sf-form[data-step='"+sf_paso_siguiente+"']").removeClass("d-none");



		//Comprobamos que el formulario se ha enviado en el Gravity Form real

		// console.warn("tipo de envío");

		// console.log(sf_tipo_envio);

		if(sf_tipo_envio=="inline"){

			sf_comprobar_envio($contenedor_secciones, sf_formulario_objetivo, sf_paso_siguiente, 0, gtm_var.multiecommerce);

		}

		else{

			// Al usar formularios con página de gracias eliminamos la comprobación enlinea

			egtm_ecom_checkout_2();

			egtm_ecom_purchaseProduct();

		}

	});

}







function sf_comprobar_envio($contenedor_secciones, id_formulario_objetivo, paso_gracias, reintentos_comprobar_envio, $productos = undefined){

	var $sf_contenedor_gracias = $contenedor_secciones.find(".sf-form[data-pos="+paso_gracias+"]");

	// console.log("comprobación ok " + reintentos_comprobar_envio);

	// console.log(jQuery("#gform_confirmation_message_18").length);

	if(jQuery("#gform_confirmation_message_"+id_formulario_objetivo).length>0){

		//El formulario se ha enviado con éxito

		gtm_var.form_variant = "popup_lateral";



		if($productos==undefined){

			$productos = [{

				'name': slugify(gtm_var.producto_name.replace(" ", "_")),

				'id': gtm_var.producto_id,

				'category': gtm_var.producto_cat,

				'variant': gtm_var.form_variant,

				'brand': gtm_var.form_seccion,

				'quantity': 1,

				'price': 0,

				'dimension9': gtm_var.clientes_seleccionados,

			}]

		}



		$ecommerce_envio = {

			currencyCode: 'EUR',

			checkout: {

				actionField: {

					step: 2

				},

				products: $productos

			}

		};



		let mll = jQuery('.gf-mll input').val();

		let tlf = jQuery('.sf-phone input').val();

		var now = new Date();

		let user_id_js = gtm_var.producto_id.split(",")[0] + '-' + mll + '-' + tlf.substr(-4) + '-' + now.toISOString().split('T')[0].replace(/-/g,'') + '-' + String(now.getHours()) + String(now.getMinutes()) + String(now.getSeconds());



		$ecommerce_exito = {

			currencyCode: 'EUR',

			purchase: {

				actionField: {

					id: user_id_js,

					revenue: 1,

				},

				products: $productos

			}

		};





		sf_gtm_evento('wAnalyticsEvent', 'formulario_lead', 'envio_lead_form', gtm_var.producto_name.replace(/-/g, '_'), $ecommerce_envio);

		sf_gtm_evento('wAnalyticsEvent', 'formulario_lead', 'exito_lead_form', gtm_var.producto_name.replace(/-/g, '_'), $ecommerce_exito);



		// var currentScroll = jQuery(window).scrollTop();

		jQuery($contenedor_secciones).find(".sf-form[data-step='"+paso_gracias+"'] .sf-thx-spinner").addClass("d-none");

		jQuery($contenedor_secciones).find(".sf-form[data-step='"+paso_gracias+"'] .sf-thx-msj").removeClass("d-none").fadeIn();

		window.scrollTo({top: $contenedor_secciones.find(".sf-form[data-step='"+paso_gracias+"']")[0].offsetParent.offsetTop});

		// jQuery(window).scrollTop(currentScroll);

	}



	else{

		reintentos_comprobar_envio++;

		if(reintentos_comprobar_envio<10) setTimeout(sf_comprobar_envio, 500, $contenedor_secciones, id_formulario_objetivo, paso_gracias, reintentos_comprobar_envio, $productos);

	}

}









function slugify(texto, separador = "_") {

	return texto

	.toString()								// Forzamos el paso a string

	.toLowerCase()							// Convertimos todo en minúsculas

	.normalize('NFD')						// El método normalize() retorna la Forma de Normalización Unicode

	.trim()									// Eliminamos espacios en blanco a izquierda y derecha

	.replace(/\s+/g, separador)		// Sustituimos espacios en blanco intermedios por <separador>

	.replace(/[^\w\-]+/g, '')			// Eliminamos todos los caracteres extraños

	.replace(/\-\-+/g, separador);	// Reemplazamos múltiples <separadores> seguidos por uno solo

}







// #########################

// ## FUNCIONES ANALITICA ##

// #########################

function sf_gtm_evento(gtm_tipo,gtm_ev_cat,gtm_ev_act,gtm_ev_lab, gtm_ecomm_obj = undefined){

	// let form_name = "cuerpo_alto";

	let form_name = gtm_var.form_name;



	if(gtm_ecomm_obj==undefined){

		gtm_ecomm_obj = [{

			'name': slugify(gtm_var.producto_name.replace(" ", "_")),

			'id': gtm_var.producto_id,

			'category': gtm_var.producto_cat,

			'variant': gtm_var.form_variant,

			'brand': gtm_var.form_seccion,

			'quantity': 1,

			'price': 0,

			'dimension9': gtm_var.clientes_seleccionados,

		}]

	}



	dataLayer.push({

		event: gtm_tipo,

		event_data: {

			category: gtm_ev_cat,

			action: gtm_ev_act,

			label: gtm_ev_lab

		},

		user: {

			client_id: getCookie('_ga')

		},

		course: {

			category: (typeof gtm_var.producto_cat != 'undefined') ? gtm_var.producto_cat.replace(/-/g, '_') : gtm_var.producto_cat,

			id: (typeof gtm_var.producto_id !== 'undefined') ? gtm_var.producto_id.replace(/-/g, '_') : gtm_var.producto_id,

			name: (typeof gtm_var.producto_name !== 'undefined') ? gtm_var.producto_name.replace(/-/g, '_') : gtm_var.producto_name,

		},

		form: {

			type: gtm_var.form_seccion,

			id: gtm_var.form_id,

			position: form_name,

			category: 'lead'

		},

		academy: {

			name: gtm_var.academia_name

		},

		ecommerce: gtm_ecomm_obj

	});

}





function sf_gtm_ecom_addToCart($sf4_objProducts){

	let ecommerce = {

		currencyCode: 'EUR',

		add: {

			actionField: {

				list: gtm_var.pagina_tipo

			},

			products: $sf4_objProducts

		}

	};



	sf_gtm_evento('wAnalyticsEvent', 'formulario_lead', 'inicio_lead_form', gtm_var.producto_name.replace(/-/g, '_'), ecommerce);



}



// function sf_gtm_evento(gtm_tipo,gtm_ev_cat,gtm_ev_act,gtm_ev_lab){

// 	dataLayer.push({

// 		'event': gtm_tipo,

// 		'event_data': {

// 			'category': gtm_ev_cat,

// 			'action': gtm_ev_act,

// 			'label': gtm_ev_lab

// 		}

// 	});

// }





//Obtener valor de una cookie por su nombre

function getCookie(cname) {

	var name = cname + "=";

	var ca = document.cookie.split(';');

	for(var i = 0; i < ca.length; i++) {

		var c = ca[i];

		while (c.charAt(0) == ' ') {

			c = c.substring(1);

		}

		if (c.indexOf(name) == 0) {

			return c.substring(name.length, c.length);

		}

	}

	return "";

}

