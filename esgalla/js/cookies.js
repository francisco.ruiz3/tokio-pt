//Cookies js functionality

/**
 * Tipos de cookies
 * 1. Analítica
 * 2. Publicidad
 */

 var cookies = [];
 var cookie_legal = undefined;

 jQuery(document).on('ajaxCookiesLoaded', function() {
   jQuery(document).ready(function(){

    var cookielocalstorage = localStorage.getItem('cookie_legal');
    if(cookielocalstorage === null) {
      // localStorage.setItem('cookie_legal', 'undefined');
      cookielocalstorage = 'undefined';
    }

     //Inicializamos el estado de los switches según el valor de las cookies
    if(cookielocalstorage.indexOf('analitica') != -1) {
      jQuery('#check-cookies-analitica').prop('checked', true);
     }

    if(cookielocalstorage.indexOf('publicidad') != -1) {
      jQuery('#check-cookies-publicidad').prop('checked', true);
     }

    if(cookielocalstorage != 'undefined' && cookielocalstorage != 'rechazadas') {
      cookie_legal = cookielocalstorage;

      dataLayer.push({
        'event': 'ckgPageView',
        'cookies_legal': cookielocalstorage,
      });
      console.log(cookielocalstorage);
    }



     //Activate cookies accordions
     jQuery('.accordion .mas-info').click(function(e) {
       e.preventDefault();
       let accordion = jQuery(this).parent().parent().parent()[0];
       accordion.classList.toggle("active");
       var panel = accordion.nextElementSibling;
       if (panel.style.maxHeight) {
         panel.style.maxHeight = null;
       } else {
         panel.style.maxHeight = panel.scrollHeight + "px";
       }
     });

     //Pestañas configurador
     jQuery('.ck-pestana').click(function(e) {
       e.preventDefault();
       var $texto = jQuery('.texto-pestana');
       $texto.toggleClass('oculto');
       jQuery('.ck-pestana').toggleClass('borde-pestana');
     });
   });


   // var $popupCookies = jQuery('#popup-cookies');
   var $popupCookies = jQuery('#popup-cookies-bottom');
   var $popupConfigCookies = jQuery('#config-cookies');
  //  if(!getCookie('e_cookies_configured') && !sessionStorage.getItem('e_cookies_session_configured')) {



  if(localStorage.getItem('cookie_legal') == null) {
    console.log(localStorage.getItem('cookie_legal'));
    // setTimeout(function() {
      jQuery('.ck_container').fadeIn();
      console.log('desplegar banner');
      ck_desplegar_desplegable($popupCookies, false);
    // }, 100);
   }


   //Botones del popup
   jQuery('.ck_container .aceptar-cookies, #popup-cookies-bottom .aceptar-cookies').click(function() {
     aceptarTodasCookies();
     ck_plegar_desplegable(jQuery('.ck_container'));
   });

   jQuery('.ck_container .configurar-cookies').click(function() {
     jQuery('#popup-cookies-bottom').removeClass('desplegable-lat-visible');
     ck_desplegar_desplegable($popupConfigCookies, false);
   });

   jQuery('#popup-cookies .configurar-cookies, #popup-cookies-bottom .configurar-cookies, #pol-cookies .configurar-cookies').click(function() {
     ck_plegar_desplegable($popupCookies);
     ck_desplegar_desplegable($popupConfigCookies, false);
   });


   //Botones del configurador
   jQuery('#config-cookies .aceptar-cookies').click(function() {
     aceptarTodasCookies();
     //Cerramos el desplegable lateral
     setTimeout(function() {
       ck_plegar_desplegable($popupConfigCookies);
     }, 500);
   });

   jQuery('#config-cookies .guardar-cookies').click(function() {
     //Cerramos el desplegable lateral
     jQuery('.ck_container').fadeOut();
     guardarCookies();
   });
   jQuery('.btn-cerrar.ck_cerrar').click(function() {
     console.log('cierraconfig');
     ck_plegar_desplegable($popupConfigCookies);
   });

 });

 function aceptarTodasCookies() {
  //Aceptamos todas las cookies (cuando el usuario le da al botón "Aceptar todas")
  jQuery('#config-cookies input[type="checkbox"]').prop('checked', true);

  console.log(cookie_legal);
  var cookielocalstorage = localStorage.getItem('cookie_legal');
  cookies = [];
  cookies.push('analitica');
  cookies.push('publicidad');

  cookie_legal = cookies.join('|');

  console.log(cookie_legal);

  if(cookielocalstorage == null || cookielocalstorage == 'rechazadas') {
    pageview('ckgPageView' , cookie_legal);
  }
  //Variable de sesion, se borra al cerrar pestaña
  localStorage.setItem('cookie_legal', cookie_legal);

 }

 function guardarCookies() {
  cookies = [];
  var cookielocalstorage = localStorage.getItem('cookie_legal');

  if(jQuery('#check-cookies-analitica').is(':checked')) {
    cookies.push('analitica');
  }

  if(jQuery('#check-cookies-publicidad').is(':checked')) {
    cookies.push('publicidad');
  }

  if(cookies.length == 0) {
    cookie_legal = 'rechazadas';
  } else {
    cookie_legal = cookies.join('|');
    console.log(cookie_legal);
  }

  if(cookielocalstorage == null || cookielocalstorage == 'rechazadas') {
    pageview('ckgPageView' , cookie_legal);
  }
  //Variable de sesion, se borra al cerrar pestaña
  localStorage.setItem('cookie_legal', cookie_legal);
 }

 //Auxiliar functions
 function setCookie(cname, cvalue, exdays) {
   var d = new Date();
   d.setTime(d.getTime() + (exdays*24*60*60*1000));
   var expires = "expires="+ d.toUTCString();
   document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
 }


 function getCookie(cname) {
   var name = cname + "=";
   var decodedCookie = decodeURIComponent(document.cookie);
   var ca = decodedCookie.split(';');
   for(var i = 0; i <ca.length; i++) {
     var c = ca[i];
     while (c.charAt(0) == ' ') {
       c = c.substring(1);
     }
     if (c.indexOf(name) == 0) {
       return c.substring(name.length, c.length);
     }
   }
   return "";
 }

 function pageview(gtm_tipo, gtm_ev_act){
   dataLayer.push({
     event: gtm_tipo,
     cookies_legal: gtm_ev_act,
   });

 }


 function ck_desplegar_desplegable($obj, navigation = true) {
   if ($obj.hasClass('desplegable-lat')) {//Si es botón lateral
       $obj.addClass("desplegable-lat-visible");
   } else if ($obj.hasClass('popup')) {//Si es botón lateral
       $obj.fadeIn('fast');
   } else {//Si es desplegable simple
       $obj.slideDown('fast', function () {
           jQuery($obj).css('overflow', 'visible')
       });
   }

 }
 function ck_plegar_desplegable($obj) {

   if ($obj.hasClass('desplegable-lat')) {//Si es botón lateral
       $obj.removeClass("desplegable-lat-visible");
   } else if ($obj.hasClass('popup')) {//Si es botón lateral
       $obj.fadeOut('fast');
   } else {//Si es desplegable simple
       $obj.fadeOut('fast');
   }


   //Si hay fondo oscuro
   if ($obj.find(".desplegable-fondo").length) {
       $obj.find(".desplegable-fondo").fadeOut('800');
       jQuery("body").css({
           "height": "auto",
           "overflow": "visible"
       });
   }

 }


 /***************************** Página vista ********/



 jQuery(document).on('ajaxCookiesLoaded', function() {

   jQuery( ".configurar-cookies-button" ).on( "click", function() {
     jQuery(".desplegable-config").css('display','block');
     jQuery(".desplegable-lat-bloque.cookies-desplegable").css('display','none');
    //  jQuery('.switch-input').removeAttr('checked');

 });

   jQuery( ".cookies-configure a" ).on( "click", function() {
     jQuery(".guardar-cookies").css('display','block');
   });

   jQuery( ".borde-privacidad a" ).on( "click", function() {
     jQuery(".guardar-cookies").css('display','block');
   });

   jQuery( ".guardar-cookies" ).on( "click", function() {
     jQuery("#config-cookies").css('display','none');
   });


 });