jQuery(document).ready(function() {

	var mll = gclid = user_id = undefined;

	inicio_formulario_enviado = [];



	if(!sessionStorage.getItem("dl_mll")){

		if(getParameterByName('MLL')){

			sessionStorage.setItem("dl_mll", getParameterByName('MLL'));

			rellena_campo_formulario(jQuery(".gf-modollegada"), sessionStorage.getItem("dl_mll"));

			rellena_campo_formulario(jQuery(".gf-mll"), sessionStorage.getItem("dl_mll"));

		}else if(getParameterByName('modo_llegada')){

			sessionStorage.setItem("dl_mll", getParameterByName('modo_llegada'));

			rellena_campo_formulario(jQuery(".gf-modollegada"), sessionStorage.getItem("dl_mll"));

			rellena_campo_formulario(jQuery(".gf-mll"), sessionStorage.getItem("dl_mll"));

		}

	}

	else{


		//gtm_var.mll = sessionStorage.getItem("dl_mll");



		rellena_campo_formulario(jQuery(".gf-modollegada"), sessionStorage.getItem("dl_mll"));

		rellena_campo_formulario(jQuery(".gf-mll"), sessionStorage.getItem("dl_mll"));

	}

	if(!sessionStorage.getItem("dl_gclid")){

		if(getParameterByName('gclid')){

			sessionStorage.setItem("dl_gclid", getParameterByName('gclid'));

		}

	}

	else{

		gtm_var.gclid = sessionStorage.getItem("dl_gclid");

		rellena_campo_formulario(jQuery(".gf-gclid"), sessionStorage.getItem("dl_gclid"));

	}



	//ID Usuario Google Analytics

	if(document.cookie.split("idgacampus=")[1]!=undefined){

		gtm_var.user_id = document.cookie.split("idgacampus=")[1].split(";")[0];

	}

	else check_ga(0);


	//#####################

	//## CAPTURA EVENTOS ##

	//#####################

	//Al pulsar un input para rellenar

	jQuery(".gform_body input").on("focus", function(){

		let id_formulario = jQuery(this).attr("id").split("_")[1];

		if(!inicio_formulario_enviado[id_formulario]) pasos_formularios(id_formulario, "start", 1);

	});



	//Al cambiar un desplegable

	jQuery(".gform_body select").on("change", function(){

		let id_formulario = jQuery(this).attr("id").split("_")[1];

		if(!inicio_formulario_enviado[id_formulario]) pasos_formularios(id_formulario, "start", 1);

		// if(!inicio_formulario_enviado) pasos_formularios(jQuery(this).attr("id").split("_")[1], "start", 1);

		var contenedor_formulario = "#gform_"+jQuery(this).attr("id").split("_")[1];

		var nombre_curso_seleccionado = jQuery(this).val().split("|")[2];

		jQuery(contenedor_formulario+" .gf-nombre_curso input").val(nombre_curso_seleccionado);



		if(nombre_curso_seleccionado){

			var product_name_lower = nombre_curso_seleccionado.toLowerCase();



			if(product_name_lower.indexOf("oposi") > -1 || product_name_lower.indexOf("funcionario") > -1){

				jQuery(contenedor_formulario+" .gf-tipocurso input").val("oposiciones");

			}else if(product_name_lower.indexOf("curso") > -1 || product_name_lower.indexOf("carnet") > -1 || product_name_lower.indexOf("veterinaria") > -1 || product_name_lower.indexOf("administracion") > -1 || product_name_lower.indexOf("turismo") > -1 || product_name_lower.indexOf("belleza") > -1 || product_name_lower.indexOf("rrpp") > -1 || product_name_lower.indexOf("oficios") > -1){

				jQuery(contenedor_formulario+" .gf-tipocurso input").val("cursos");

			}else if(product_name_lower.indexOf("pruebas") > -1 || product_name_lower.indexOf("formacion") > -1 || product_name_lower.indexOf("fp") > -1){

				jQuery(contenedor_formulario+" .gf-tipocurso input").val("fp");

			}



			if(jQuery(this).val().split("|")[1].indexOf(".pdf")>-1){

				jQuery(contenedor_formulario+" .gf-producto_catalogo input").val(jQuery(this).val().split("|")[1]);

			}

		}

	});



	//Al intentar enviar un formulario. Enviamos dentro del render para capturar re-envíos después de un error

	jQuery(document).on('gform_post_render', function(e,f,p){

		jQuery('#gform_ajax_frame_'+f).attr('src', 'about:blank'); //Prevenimos que se envién duplicados en el formulario


	});


	jQuery(document).on('gform_confirmation_loaded', function(event, f){
		var contenedor_formulario = "#gform_wrapper_"+f;
	   
		gtm_evento("ckgAnalyticsEvent", "masterclass", "solicitud_formacion_trysend", "disno_d_vidogos");


	});





	jQuery(".gf-selector_curso select").on("change", function(){

		let id_formulario = jQuery(this).attr("id").split("_")[1];

		console.log(id_formulario);

		jQuery("#gform_"+id_formulario+" .gf-idcurso input").val(jQuery(this).val().split("|")[0]).attr('value', jQuery(this).val().split("|")[0]);

		jQuery("#gform_"+id_formulario+" .producto_tipo input").val(jQuery(this).val().split("|")[1]).attr('value', jQuery(this).val().split("|")[1]);

		jQuery("#gform_"+id_formulario+" .gf-producto_categoria input").val(jQuery(this).val().split("|")[2]).attr('value', jQuery(this).val().split("|")[2]);

		jQuery("#gform_"+id_formulario+" .gf-nombrecurso input").val(jQuery(this).find("option:selected").text()).attr('value', jQuery(this).find("option:selected").text());

	});



});



//#######################

//## FUNCIONES EVENTOS ##

//#######################

function pasos_formularios(id_formulario, event_action, checkout_step){


	var tipo_de_formulario = formacion_o_guia(id_formulario);

	var accion_evento = "solicitud_"+tipo_de_formulario+"_"+event_action;

	var contenedor_formulario = "#gform_"+id_formulario;



	setTimeout(function(){

		if(jQuery(contenedor_formulario+" .gf-idcurso input").val()!=""){

			//Prevenimos que se vuelva a enviar de nuevo el inicio del formulario

			inicio_formulario_enviado[id_formulario] = true;



			//Guardamos ciertos valores en algunos campos para poder ser recuperados después

			//jQuery("#gform_"+id_formulario+" .gf-producto_tipo input").val(gtm_var.product_type);

			if(jQuery(contenedor_formulario+" .gf-producto_categoria input").val()=="") jQuery(contenedor_formulario+" .gf-producto_categoria input").val(gtm_var.product_category);

			if(jQuery(contenedor_formulario+" .gf-tipocurso input").val()=="") jQuery(contenedor_formulario+" .gf-tipocurso input").val(gtm_var.product_type);

			//jQuery(contenedor_formulario+" .gf-prelead_name input").val(gtm_var.page_title);

			if(jQuery(contenedor_formulario+" .gf-prelead_blogcat input").val()=="") jQuery(contenedor_formulario+" .gf-prelead_blogcat input").val(gtm_var.blog_cat);

			if(jQuery(contenedor_formulario+" .gf-prelead_author input").val()=="") jQuery(contenedor_formulario+" .gf-prelead_author input").val(gtm_var.blog_author);

			if(jQuery(contenedor_formulario+" .gf-pagina_seccion input").val()=="") jQuery(contenedor_formulario+" .gf-pagina_seccion input").val(gtm_var.page_section);

			if(jQuery(contenedor_formulario+" .gf-pagina_tipo input").val()=="") jQuery(contenedor_formulario+" .gf-pagina_tipo input").val(gtm_var.page_type);

			if(jQuery(contenedor_formulario+" .gf-user-id input").val()=="") jQuery(contenedor_formulario+" .user-id input").val(gtm_var.user_id);



			var product_id = jQuery(contenedor_formulario+" .gf-idcurso input").val();

			var product_name = product_name = jQuery(contenedor_formulario+" .gf-nombrecurso input").val();

			if(product_name==undefined) product_name = jQuery(contenedor_formulario+" #input_7_15").val();

			var product_category = gtm_var.product_category;



			console.log('##########################');

			console.log('## EMPEZANDO NUEVO PUSH ##');

			console.log('##########################');

			if(jQuery(contenedor_formulario+" .gfield_error").length<1){

				//var nombre_curso_limpio = limpiar_guiones(jQuery(contenedor_formulario+" .gf-nombre_curso input").val());

				var nombre_curso_limpio = limpiar_guiones(product_name);

				if(jQuery("body").hasClass("postid-4040")){
					gtm_evento("ckgAnalyticsEvent", "masterclass", accion_evento , nombre_curso_limpio);
			    }

			    else{
					gtm_checkout(product_id,product_name,product_category,checkout_step);
					gtm_evento("ckgAnalyticsEvent", gtm_var.page_type, accion_evento , nombre_curso_limpio);
			    }

			}

			console.log('###########################');

			console.log('## TERMINANDO NUEVO PUSH ##');

			console.log('###########################');

		}

		else console.log("primero necesito saber el curso");

	}, 500);

}





function gtm_evento(gtm_tipo,gtm_ev_cat,gtm_ev_act,gtm_ev_lab){

	dataLayer.push({

		'ClientID': gtm_var.user_id,

		'mll': gtm_var.mll,

		'gclid': gtm_var.gclid,

		'event': gtm_tipo,

		'eventCategory': gtm_ev_cat,

		'eventAction': gtm_ev_act,

		'eventLabel': gtm_ev_lab

	});

}



function gtm_checkout(product_id,product_name,product_category,checkout_step){

	dataLayer.push({

		'event': 'checkout',

		'ecommerce': {

		currencyCode: 'EUR',

			'checkout': {

				'actionField': {'step': checkout_step},

				'products': [{

					'name': product_name,// Nombre formación,

					'id': product_id,// Id de formación

					'price': '1',// Precio de la formación

					'category': product_category,// Categoría de formación

					'quantity': '1',// Solo se puede solicitar 1 por formación.

				}]

			}

		}

	});

}




//######################

//## FUNCIONES VARIAS ##

//######################



function getParameterByName(name) { //Recuperar el modo de llegada

	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");

	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),

		results = regex.exec(location.search);

	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));

}



function formacion_o_guia(id_formulario){

	id_formulario = parseInt(id_formulario);

	var tipo_de_formulario = "formacion";

	if ([5].indexOf(id_formulario) > -1) tipo_de_formulario = "guia";

	return tipo_de_formulario;

}



function check_ga(reintentos) {

	nombreCookieGA = 'idgacampus';

	if(document.cookie.indexOf(nombreCookieGA)==-1){

		console.log('Pruebo a cazar clientID');

		if (typeof ga === 'function') {

			//console.log('La función existe');

			//comprobamos que existe la cookie

			if(document.cookie.indexOf("_ga=")>-1){

				//obtenemos el valor de la cookie

				gaCookie = document.cookie.split("_ga=")[1].split(";")[0].split(".");

				clientID = "GA."+gaCookie[2] + "." + gaCookie[3];

				//Basura de debug

				//console.log('clientID de Google Analytics');

				//console.log(clientID);

				//Creamos la cookie propia para facilitar el acceso - 30 días duración (diasCookieGA)

				var caducidadCookieGA = '', valorCookieGA = clientID, diasCookieGA = 30, date = new Date();

				if (diasCookieGA) {

					date.setTime(date.getTime() + (diasCookieGA * 24 * 60 * 60 * 1000));

					caducidadCookieGA = '; expires=' + date.toGMTString();

				}

				document.cookie = nombreCookieGA + '=' + valorCookieGA + caducidadCookieGA + '; path=/';

				gtm_var.user_id = valorCookieGA;

				console.log('ID Anlytics capturado');

			}

			else{

				//console.log('La función existe pero no hay cookie establecida');

			}

		} else {

			//console.log("GA no está implementado o está bloqueado por el cliente")

			reintentos++;

			if(reintentos<5) setTimeout(check_ga,2500,reintentos);

		}

	}

	if(document.cookie.indexOf(nombreCookieGA)>-1){

		console.log('ya existe idgacampus');

	}

}



function rellena_campo_formulario(jQueryobjetoContenedor, valor){

	if(jQueryobjetoContenedor.length){

		jQueryobjetoContenedor.each(function (index){

			jQuery(this).find('input').val(valor);

		});

	}

}



function limpiar_guiones(cadena){

   // Definimos los caracteres que queremos eliminar

   var specialChars = "¡!@#jQuery^&%*()+[]\{}|:<>¿.";

   // Los eliminamos todos

   for (var i = 0; i < specialChars.length; i++) {

       cadena= cadena.replace(new RegExp("\\" + specialChars[i], 'gi'), '');

   }

   // Lo queremos devolver limpio en minusculas

   cadena = cadena.toLowerCase();

   // Quitamos espacios y los sustituimos por _ porque nos gusta mas asi

   cadena = cadena.replace(/ /g,"_");

   // Quitamos acentos y "ñ". Fijate en que va sin comillas el primer parametro

   cadena = cadena.replace(/-/gi,"_");

   cadena = cadena.replace(/,/gi,"|");

   cadena = cadena.replace(/á/gi,"a");

   cadena = cadena.replace(/é/gi,"e");

   cadena = cadena.replace(/í/gi,"i");

   cadena = cadena.replace(/ó/gi,"o");

   cadena = cadena.replace(/ú/gi,"u");

   cadena = cadena.replace(/ñ/gi,"n");

   cadena = cadena.replace(/ň/gi,"n");



   return cadena;

}