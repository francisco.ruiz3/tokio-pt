wow = new WOW({
    boxClass: 'wow', // default
    animateClass: 'animated', // default
    offset: 0, // default
    mobile: true, // default
    live: true // default
})
wow.init();

jQuery(document).ready(function() {


    jQuery(".button-masterclass .elementor-button-link").removeAttr("href");

    /**
     * Nav bar sticky
     */

    ///////////////// fixed menu on scroll for desktop
    if (jQuery(window).width() > 1 && jQuery('nav.navbar').length) {
        // jQuery('nav.navbar').addClass('fixed-top');
        jQuery(window).scroll(function() {
            if (jQuery(window).scrollTop() >= (jQuery('.topbar-second').position().top + jQuery('.topbar-second').outerHeight())) {
                if (jQuery('nav.navbar').hasClass('fixed-top')) return;

                jQuery('nav.navbar').addClass('fixed-top');
                jQuery('main > div').first().css('margin-top', jQuery('nav.navbar').outerHeight() + 'px');
            } else {
                jQuery('nav.navbar').removeClass('fixed-top');
                jQuery('main > div').first().css('margin-top', '0');
            }

        });
    } // end if



    // jQuery(window).scroll(function () {
    //   var headerHeight = jQuery('#masthead').outerHeight();
    //   if (jQuery(this).scrollTop() > headerHeight ) {
    //     console.log('1scroll');

    //   } else {
    //     console.log('2scroll');

    //   }
    // });

    var lastScrollTop = 0;
    jQuery(window).scroll(function(event) {
        var st = jQuery(this).scrollTop();
        if (st < lastScrollTop) {
            // upscroll code
            jQuery('.sticky-footer-mobile').addClass('show');
            jQuery('body').addClass('sticky-footer-actived');
        } else {
            // downscroll code
            jQuery('.sticky-footer-mobile').removeClass('show');
            jQuery('body').removeClass('sticky-footer-actived');
        }
        lastScrollTop = st;
    });


    /**
     * Home carousel custom controls
     */

    if (jQuery('.carousel-home-areas-formacion').length) {
        jQuery('.custom-carousel-controls .custom-controls-button, .custom-carousel-controls-mobile .link-button').click(function(e) {
            e.preventDefault();
            jQuery('.custom-carousel-controls .custom-controls-button.active, .custom-carousel-controls .link-button.active').removeClass('active');
            jQuery(this).addClass('active');
            jQuery('.carousel-home-areas-formacion .flickity-page-dots .dot').eq(jQuery(this).index()).click();

            //Calculamos cuanto tenemos que scrollear la botonera
            let scrollSize = jQuery('.custom-carousel-controls-mobile .link-button.active').offset().left - jQuery('.custom-carousel-controls-mobile').offset().left + jQuery('.custom-carousel-controls-mobile').scrollLeft();
            jQuery('.custom-carousel-controls-mobile').animate({ scrollLeft: scrollSize }, 400);
            // jQuery('.custom-carousel-controls-mobile').animate({scrollLeft: scrollSize + '%'}, 200);
        });

        jQuery('.carousel-home-areas-formacion').on('change.flickity', function(event, index) {
            //Mobile
            jQuery('.custom-carousel-controls-mobile .link-button.active').removeClass('active hover');
            jQuery('.custom-carousel-controls-mobile .link-button').eq(index).addClass('active').click();

            //PC
            jQuery('.custom-carousel-controls .custom-controls-button.active').removeClass('active hover');
            jQuery('.custom-carousel-controls .custom-controls-button').eq(index).addClass('active').click();

            let move = 100 / jQuery('.custom-carousel-controls .custom-controls-button').length;
            jQuery('.active-element-bar').css('top', move * index + '%');
        });

        jQuery('.custom-carousel-controls .custom-controls-button').hover(function() {
            jQuery(this).addClass('hover');
        }, function() {
            jQuery(this).removeClass('hover');
        });

    }

    /**
     * Home carousel custom controls
     */


    /**
     * Accordion
     */

    if (jQuery('.custom-accordion').length) {
        jQuery(".custom-accordion .card-header").click(function() {
            //If this accordion item is collapsing ignore do nothing
            if (jQuery('#' + jQuery(this).attr('href').replace('#', '')).hasClass('collapsing')) return;

            if (jQuery(this).hasClass('collapsed')) {
                jQuery(".custom-accordion .accordion-item.active").removeClass('active');
                jQuery(this).closest('.accordion-item').addClass('active');
            } else {
                jQuery(".custom-accordion .accordion-item.active").removeClass('active');
            }
        });
    }


    // jQuery('#accordion').on('shown.bs.collapse', function () {
    //     console.log('jej');

    //     console.log(jQuery(this).find('.collapsing'));
    //     var panel = jQuery(this).find('.collapsing');

    //     jQuery('html, body').animate({
    //         scrollTop: panel.offset().top - 130
    //     }, 500);
    // });

    // jQuery("#accordion").on("shown.bs.collapse", function () {
    //     var selected = jQuery(this);
    //     var collapseh = jQuery(".collapsing").height();
    //     jQuery('body').scrollTo(selected, 500, {
    //         offset: -(collapseh)
    //     });
    // });
    // jQuery('#accordion').on('shown.bs.collapse', function(event) {
    //   jQuery('html, body').css('overflow-y', 'initial');
    // });
    // jQuery('#accordion').on('show.bs.collapse', function(event) {
    //   jQuery('html, body').css('overflow-y', 'hidden');
    // });
    // jQuery('#accordion').on('hidden.bs.collapse', function(event) {
    //   jQuery('html, body').css('overflow-y', 'initial');
    // });
    // jQuery('#accordion').on('hide.bs.collapse', function(event) {
    //   jQuery('html, body').css('overflow-y', 'hidden');
    // });


    /**
     * Component popup video
     */
    if (jQuery('.component-popup-video').length) {
        let $videoSrc;
        jQuery('.video-btn').click(function() {
            $videoSrc = jQuery(this).data("src");
        });

        // when the modal is opened autoplay it
        jQuery('#videoModal').on('shown.bs.modal', function(e) {
            //Avoid body scroll when modal is opened
            jQuery('html, body').css('overflow-y', 'hidden');
            // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
            jQuery("#video").attr('src', $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
        });

        // stop playing the youtube video when I close the modal
        jQuery('#videoModal').on('hide.bs.modal', function(e) {
            //Restore body scroll when moda hidden
            jQuery('html, body').css('overflow-y', 'initial');
            // a poor man's stop video
            jQuery("#video").attr('src', $videoSrc);
        })
    }

    /**
     * Formación modulos
     */

    if (jQuery('#modulos-formacion').length) {
        jQuery('.active-element-bar').css('width', (100 / jQuery('#modulos-formacion .modulos-nav .modulos-nav-item').length) + '%');
        //Le damos el ancho dinámico a la barrita rosa dependiendo de cuantos elementos haya
        jQuery('#modulos-formacion .modulos-nav .active-element-bar')
        jQuery('.modulos-nav .modulos-nav-item').click(function() {
            if (jQuery(this).hasClass('active')) return;
            let index = jQuery(this).index();
            jQuery('.modulos-nav .modulos-nav-item.active').removeClass('active');
            jQuery(this).addClass('active');

            // jQuery('.modulos-content .modulos-content-item.active').removeClass('active');
            // jQuery('.modulos-content .modulos-content-item').eq(index).addClass('active');
            jQuery('.modulos-content .modulos-content-item.active').fadeOut(200, function() {
                jQuery('.modulos-content .modulos-content-item').eq(index).fadeIn(200, function() {
                    jQuery(this).addClass('active');
                })
                jQuery(this).removeClass('active');
            });

            //Get new index

            let move = 100 / jQuery('#modulos-formacion .modulos-nav .modulos-nav-item').length;
            jQuery('.active-element-bar').css('left', move * index + '%');
            jQuery('.horizontal-scrollable .modulos-nav').animate({ scrollLeft: (jQuery(this).outerWidth() * (index - 1)) + 10 }, 500);
        });
    }


    /**
     * Formación carrousel profesores
     */
    if (jQuery('#nuestros-profesores .carousel-profes').length) {

        //Custom nav buttons
        var customPrev = jQuery('.carousel-profes-custom-nav .prev');
        var customNext = jQuery('.carousel-profes-custom-nav .next');

        customPrev.click(function() {
            console.log('prev');
            jQuery('.flickity-button.previous').click();
        });

        customNext.click(function() {
            console.log('next');
            jQuery('.flickity-button.next').click();
        });

        jQuery('.carousel-profes').on('change.flickity', function(event, index) {
            console.log('Boton prev:' + jQuery('.flickity-button.previous').attr('disabled'));
            console.log('Boton next:' + jQuery('.flickity-button.next').attr('disabled'));
            if (jQuery('.flickity-button.previous').attr('disabled')) {
                customPrev.removeClass('active');
            } else {
                customPrev.addClass('active');
            }
            if (jQuery('.flickity-button.next').attr('disabled')) {
                customNext.removeClass('active');
            } else {
                customNext.addClass('active');
            }
        });
    }


    /**
     * Contact Gmaps
     */
    if (jQuery('#map-container-google').length) {
        jQuery('.ubication-gmaps').click(function() {
            let lat = jQuery(this).data('lat');
            let lng = jQuery(this).data('lng');
            let dir = jQuery(this).data('dir');
            if (dir != "") jQuery('#map-container-google iframe').attr('src', 'https://maps.google.com/maps?q=' + dir + '&t=&z=13&ie=UTF8&iwloc=&output=embed');
            else jQuery('#map-container-google iframe').attr('src', 'https://maps.google.com/maps?q=' + lat + ',' + lng + '&t=&z=13&ie=UTF8&iwloc=&output=embed');
            jQuery('.ubication-gmaps.active').removeClass('active');
            jQuery(this).addClass('active');
        });
    }




    /**
     * Para empresas carousel
     */
    if (jQuery('#hiring-partners').length) {
        jQuery('.carousel-logos-empresas-custom-nav .prev').click(function() {
            jQuery('.carousel-logos-empresas .flickity-button.previous').click();
        });
        jQuery('.carousel-logos-empresas-custom-nav .next').click(function() {
            jQuery('.carousel-logos-empresas .flickity-button.next').click();
        });
    }



    /**
     * Página formaciones
     */
    if (jQuery('.categorias-cursos-nav').length) {
        jQuery('.categorias-cursos-nav').on('click', '.nav-item', function() {
            if (jQuery(this).hasClass('active')) return;
            if (jQuery('.categorias-cursos-nav').hasClass('loading')) return;
            jQuery('.categorias-cursos-nav').addClass('loading');

            //Nav styling
            jQuery('.categorias-cursos-nav .nav-item').removeClass('active');
            jQuery(this).addClass('active');

            //Hide/Show grids
            let grid = jQuery(this).data('grid');
            let gridElementCurrent = jQuery('.formaciones-grid.active');
            let gridElementNext = jQuery('.formaciones-grid.' + grid + '-grid');
            // console.log(grid);
            gridElementCurrent.fadeOut(300, function() {
                gridElementNext.fadeIn(300, function() {
                    gridElementCurrent.removeClass('active');
                    gridElementNext.addClass('active');
                    jQuery('.categorias-cursos-nav').removeClass('loading');
                });
            });
        });
    }

    /*
     * Cambio de color en los select
     */
    var $select = jQuery('select');
    $select.each(function() {
        jQuery(this).addClass(jQuery(this).children(':selected').val());
    }).on('change', function(ev) {
        jQuery(this).addClass('selected').val();
    });

    //Animated counters functionality
    start_animated_counter();


    //Resize para que funcione el slider dentro del modal de proyectos
    jQuery('#projectModal').on('shown.bs.modal', function(e) {
        jQuery(".carousel").flickity('resize');
        console.log('shown')
    });

    //List tab para Especialidades Python
    // jQuery('#list-tab a').on('click', function (e) {
    //   e.preventDefault();
    //   jQuery(this).tab('show');
    //   console.log('hola');
    //   jQuery('#list-tab a[href="#list-inteligencia-artificial"]').tab('show');
    //   jQuery('#list-tab a[href="#list-machine-learning"]').tab('show');
    // });
    if (jQuery('.seccion-especialidades').length) {
        jQuery('.seccion-especialidades .content-controls a').click(function() {
            //Si estoy pulsando un elemento que ya es active no hago nada
            if (jQuery(this).hasClass('active')) return;

            //Le quito el active al actual active
            jQuery('.seccion-especialidades .content-controls a.active').removeClass('active');
            //Y se lo pongo al que acabo de clicar
            jQuery(this).addClass('active');

            //Pillo el data que me indica qué contenedor vamos a mostrar
            let content = jQuery(this).data('content');

            //Oculto el visible actual
            jQuery('.especialidades-content div.active').fadeOut(200, function() {
                //Y al acabar muestro el nuevo
                jQuery('.especialidades-content #content-' + content).addClass('active').fadeIn(200);
            }).removeClass('active');

        });
    }

    if (jQuery('#searchwp').length) {
        jQuery('#searchwp').focus(function() {
            jQuery('.searchwp-container').addClass('onfocus');
        });
        jQuery('#searchwp').blur(function() {
            if (jQuery(this).val() == '') {
                jQuery('.searchwp-container').removeClass('onfocus');
            }
        });

        jQuery('.form-search').submit(function(e) {
            e.preventDefault();
        });
    }

    jQuery('#navbarSupportedContent').on('show.bs.collapse', function() {
        // do something…
        jQuery('html').addClass('overlayed');
    });
    jQuery('#navbarSupportedContent').on('hide.bs.collapse', function() {
        // do something…
        jQuery('html').removeClass('overlayed');
    });
    jQuery('.menu-overlay').click(function() {
        jQuery('.navbar.mobile .navbar-toggler').click();
    });


    //FacetWP pagina noticias
    var facetLoadedFirstTime = true;
    var facetLoadMore = false;


    jQuery(document).on('facetwp-loaded', function() {
        if (facetLoadMore) {
            facetLoadMore = false;
            jQuery('.facetwp-load-more').click(function() {
                console.log('paso a true');
                facetLoadMore = true;
            });
            return;
        }

        if (!facetLoadedFirstTime && jQuery('html').hasClass('mobile')) {
            jQuery([document.documentElement, document.body]).animate({
                scrollTop: jQuery(".facetwp-template").offset().top - 50
            }, 500);
        } else {
            facetLoadedFirstTime = false;
        }
        jQuery('.facetwp-load-more').click(function() {
            console.log('paso a true');
            facetLoadMore = true;
        });

    });


    /**
     * Flip 404 page effect
     */
    jQuery('.404lolflip .back').css("visibility", "visible");
    jQuery(".404lolflip").flip({
        trigger: 'hover',
        axis: 'x'
    });

});


jQuery(window).on('load', function() {
    console.log('wload');
    //Si existe algún flickity lo inicializamos.
    jQuery('.carousel').flickity('resize');

    //Fix opiniones slider (se inicializa mal, hacemos un recálculo al cargar la página completamente)
    var $carousel_proyectos = jQuery('.carousel-proyectos');
    $carousel_proyectos.flickity('resize');

    jQuery('.modal-proyectos').on('shown.bs.modal', function() {
        jQuery(this).find('.carousel').flickity('resize').animate({ opacity: 1.0 }, 200);
    });

    var scene = document.getElementById('parallaxhover');
    if (scene) {
        var parallaxInstance = new Parallax(scene, {
            relativeInput: true,
            hoverOnly: false
        });
    }

    var scenex = document.getElementById('parallaxhoverx');
    if (scenex) {
        var parallaxInstancex = new Parallax(scenex, {
            relativeInput: true,
            hoverOnly: false,
            limitY: 1
        });
    }
    // parallaxInstance.enable();
    // parallaxInstance.friction(0.2, 0.2);

});



function start_animated_counter() {
    jQuery('.animated-counter').each(function() {
        let prefix = (jQuery(this).data('counter-prefix')) ? jQuery(this).data('counter-prefix') : '';
        let sufix = (jQuery(this).data('counter-sufix')) ? jQuery(this).data('counter-sufix') : '';
        jQuery(this).animate({
            counter: jQuery(this).data('counter')
        }, {
            duration: 3000,
            easing: 'swing',
            step: function(now) {
                jQuery(this).text(prefix + Math.ceil(now) + sufix);
            },
            complete: start_animated_counter
        });
    })
};



jQuery(document).on('ready', function($) {
    /* Close elementor acordions at page load */
    var delay = 100;
    setTimeout(function() {
        jQuery('.elementor-tab-title').removeClass('elementor-active');
        jQuery('.elementor-tab-content').css('display', 'none');
    }, delay);
    /* Close elementor acordions at page load */


    /* Menu a behaviour */
    //var tempClickA = '';
    //jQuery('.menu-item').click(function(e) {
    // if(jQuery(this).attr() == tempClickA) {
    //   tempClickA = '';
    // } else {
    //e.preventDefault();
    //tempClickA = jQuery(this).attr();
    // }
    //});

    /* Menu a behaviour */

    /* Brandbook mobile menu funcitonality */
    if (jQuery('.mobile-menu-content').length) {
        jQuery('.mobile-menu-open-button .elementor-icon').click(function() {
            jQuery('.mobile-menu-content').animate({ left: '0%' });
        });
        jQuery('.mobile-menu-close-button .elementor-icon').click(function() {
            jQuery('.mobile-menu-content').animate({ left: '110%' });
        });
        jQuery('.mobile-menu-content .sub-menu a').click(function() {
            jQuery('.mobile-menu-content').animate({ left: '110%' });
        });
    }
    /* Brandbook mobile menu funcitonality */

    /** Remove elementor smooth scrolling */
    var change_elementor_options = function() {
        if (typeof elementorFrontend === 'undefined') {
            return;
        }
        elementorFrontend.on('components:init', function() {
            elementorFrontend.utils.anchors.setSettings('selectors.targets', '.dummy-selector');
        });
    };
    jQuery(window).on('elementor/frontend/init', change_elementor_options);
    /** Remove elementor smooth scrolling */


    //Modals page scroll
    jQuery(".modal, #navbarOffcanvasMobile").on("show.bs.modal", function() {
        //Al abrir modal desactivo scroll
        jQuery('html').css("overflow", "hidden");
    }).on("hide.bs.modal", function() {
        //Al abrir modal activo scroll
        jQuery('html').css("overflow", "scroll");
    });


});



jQuery(document).on('gform_post_render', function(event, form_id, current_page) {
    //Cuando submiteamos mostramos el spinner guay
    jQuery('.gform_wrapper form').submit(function() {
        jQuery(this).find('.submit-bs-loader').removeClass('d-none');
    });

    //Prevenimos que se envién duplicados en el formulario
    jQuery('#gform_ajax_frame_' + form_id).attr('src', 'about:blank');
});






//Cookies

if (!localStorage.getItem("cookies")) {
    jQuery('.cookies').css("display", "block");
    jQuery(".cookies-boton").click(function(e) {
        localStorage.setItem("cookies", "activa");
        jQuery('.cookies').slideUp("fast");
        return false;
    });
}


//Move elements on window resize
jQuery(window).resize(function() {
    if (jQuery('.single-post').length) {
        var $contenedor_pc_form_noticia = jQuery('.form-contacto-pc-container');
        var $contenedor_mobile_form_noticia = jQuery('.form-contacto-mobile-container');
        let width = jQuery('body').width();
        //Movemos bloque dependiendo del tamaño de pantalla
        if (width <= 991) { //Si son moviles
            if ($contenedor_mobile_form_noticia.is(':empty')) {
                let temp = $contenedor_pc_form_noticia.html();
                $contenedor_mobile_form_noticia.html(temp);
                $contenedor_pc_form_noticia.html('');
            }
        } else { //Si es PC
            if ($contenedor_pc_form_noticia.is(':empty')) {
                let temp = $contenedor_mobile_form_noticia.html();
                $contenedor_pc_form_noticia.html(temp);
                $contenedor_mobile_form_noticia.html('');
            }
        }
    }
});