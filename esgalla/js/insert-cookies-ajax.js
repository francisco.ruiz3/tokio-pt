//Paint template parts through ajax
jQuery( document ).ready(function() {

  //Si existe el bloque newsletter
  if(jQuery('.cookies-ajax-container').length) {
    // console.log('metemos cookies ajax');
    //Hacemos la llamada AJAX.
    jQuery.ajax({
      type: 'GET',
      async: true,
      url: parametros_insertcookiesajax.ajaxURL,
      data: {
        action : 'template_part_ajax',                //Este es el nombre de la funcion PHP a la que vamos a llamar (está en el functions.php).
        template_part : 'cookies-form',            //Este es un parámetro que le mandamos para recoger dentro de la funcion PHP y hacer movidas (indicar que template part queremos, por ejemplo.)
        // form: '',                                  //Todas estas variables en este caso no nos hacen falta. Para los formularios de lead sí, ya que llevan analítica y necesitan ciertos valores para funcionar correctamente.
        // form_tipo: gtm_var_ajax.form_tipo,
        // form_seccion: gtm_var_ajax.form_seccion,
        // producto_id: gtm_var_ajax.producto_id,
        // producto_banner: '',
        // producto_id_wp: gtm_var_ajax.producto_id_wp,
        // producto_name: gtm_var_ajax.producto_name,
        // producto_cat: gtm_var_ajax.producto_cat,
        // pagina_name: gtm_var_ajax.pagina_name,
        // full_url: gtm_var_ajax.full_url
      },
      success: function(data){
        //Cuando se ejecuta correctamente y recogemos el return de php, hacemos una serie de acciones, entre ellas pintar el codigo html que me devuelve.
        // console.log('ajaxCookiesLoaded');
        jQuery('.cookies-ajax-container').hide().html(data.cookiesform).fadeIn(0, function() {
          jQuery(document).trigger('ajaxCookiesLoaded',[true]);                 //Lanzamos un evento para confirmar que ya hemos acabado nuestra carga por AJAX
        });
      },
      error: function(MLHttpRequest, textStatus, errorThrown){
        console.log(errorThrown);
      }
    });
  }

});
