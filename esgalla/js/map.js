
	//------------------------------------------------------------------------- Mapas
	var ventanas = []; //El array de las marcas para los filtros
	var marcas = [];
	var map;
	var prev_infowindow =false; //Para cerrar las ventanas previas

	// if (jQuery(".mapa").length){
	// 	var script = document.createElement('script');
	// 	script.type = 'text/javascript';
	// 	script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAo5wQLFXDyTcr-ZXSE2fvElmZ4K1kURsg&'+'callback=googleMap';
	// 	document.body.appendChild(script);
	// }

	function googleMap() {
		var latitud = jQuery(".mapa").data("lat");
		var longitud = jQuery(".mapa").data("lng");
		var icono = jQuery(".mapa").data("icono");
		var zoom = 5;
		if(jQuery(window).width() < 700){zoom = 4;}

		var mapOptions = {
			center: new google.maps.LatLng(latitud, longitud),
			scrollwheel: true,
			zoom: zoom
		};

		var styles = [
			{
				"stylers": [
					{ "saturation": -40 }
				]
			}
		];

		map = new google.maps.Map(document.getElementById("contacto-mapa"),mapOptions);
		map.setOptions({styles: styles});

		/*marcas*/

		jQuery(".contacto-delegacion").each(function (index){

			var ciudad = jQuery(this).data("ciudad");
			var dir = jQuery(this).data("direccion");
			var cp= jQuery(this).data("cp");
			var hor= jQuery(this).data("horario");
			var mail= jQuery(this).data("mail");
			var lat= jQuery(this).data("latitud");
			var lng= jQuery(this).data("longitud");



			//Marca
			var marcaLat = new google.maps.LatLng(lat, lng);
			var marca = new google.maps.Marker({
				position: marcaLat,
				icon: icono
			});
			marca.setMap(map);
			marcas.push(marca);

			// var ventana = '<div class="contacto-mapa-ventana p-3"><h3 class="h3">'+ciudad+'</h3><p>'+dir+'</p><p>'+cp+', '+ciudad+'</p><p>Horario: '+hor+'</p><p><a href="mailto:'+mail+'" class="fw-b">'+mail+'</a></p></div>';
			var ventana = '<div class="contacto-mapa-ventana p-3"><h3 class="h3">'+ciudad+'</h3><p>'+dir+'<br>'+cp+', '+ciudad+'</p><p>Horario: '+hor+'</p></div>';
			var infowindow = new google.maps.InfoWindow({content: ventana});
			ventanas.push(infowindow);

			marca.addListener('click', function(e) {

				if(prev_infowindow) {
					prev_infowindow.close();
				}

				infowindow.open(map,marca);
				prev_infowindow = infowindow;
				var center = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());
				map.setCenter(center);
				map.setZoom(12);
			});

		});

	}


	jQuery(document).on('click', '.contacto-delegacion', function(e) {
		var jQueryparent = jQuery(this).parent();
		var index = jQueryparent.index();
		var lat= jQuery(this).data("latitud");
		var lng= jQuery(this).data("longitud");

		if(prev_infowindow) {
			prev_infowindow.close();
		}
		ventanas[index].open(map,marcas[index]);
		prev_infowindow = ventanas[index];
		var center = new google.maps.LatLng(lat,lng);
		map.setCenter(center);
		map.setZoom(12);

		//Desplazar a móvil
		if(jQuery(window).width() < 800){

			var distancia = jQuery(".mapa").offset().top-90;
			jQuery('body,html').animate({scrollTop: distancia-50},500);


		}else{

		}

	});
