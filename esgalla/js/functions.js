function fullHeightBlock() {
  var parent = jQuery('.full-height');
  var childrenHeight = jQuery('.full-height-container').outerHeight();
  var windowBlockHeight = jQuery(window).outerHeight() ;
  var navbarHeight = jQuery('.navbar').outerHeight();
  var topbarHeight = jQuery('.topbar').outerHeight();
  if (childrenHeight <= (windowBlockHeight - (navbarHeight + topbarHeight)))  {
    parent.css({'height': (windowBlockHeight - (navbarHeight + topbarHeight)) + 'px'});
  } else {
    parent.css({'height': childrenHeight + 'px'});
  }
}

function alignVertical() {
  jQuery('.align-vertical').each(function(){
    var that = jQuery(this);
    var height = that.height();
    var parentHeight = that.parent().height();
    var padAmount = ( (parentHeight  -  height) / 2 );
    that.css( 'padding-top', padAmount );
    that.css( 'padding-bottom', padAmount );
  });
}



jQuery(document).ready(function($) {

  jQuery(".button-masterclass .elementor-button-link").removeAttr("href");

  fullHeightBlock();
  alignVertical();

  // bgImgHolderSrcset();

  $(window).resize( function() {
    fullHeightBlock();
    alignVertical();

    // bgImgHolderSrcset();
  });

  $(window).scroll(function () {
    fullHeightBlock();
  });


  // Opendropdowns on :hover
  var $dropdown = $(".dropdown");
  var $dropdownToggle = $(".dropdown-toggle");
  var $dropdownMenu = $(".dropdown-menu");
  var showClass = "show";

  $(window).on("load resize", function() {
    if (this.matchMedia("(min-width: 992px)").matches) {
      $dropdown.hover(
        function() {
          var $this = $(this);
          $this.addClass(showClass);
          $this.find($dropdownToggle).attr("aria-expanded", "true");
          $this.find($dropdownMenu).addClass(showClass);
        },
        function() {
          var $this = $(this);
          $this.removeClass(showClass);
          $this.find($dropdownToggle).attr("aria-expanded", "false");
          $this.find($dropdownMenu).removeClass(showClass);
        }
        );
    } else {
      $dropdown.off("mouseenter mouseleave");
    }
  });



  // Nav classes on scroll
  $(window).scroll(function () {
    var topbarHeight = $('.topbar').outerHeight();
    if ($(this).scrollTop() >= topbarHeight) {
      $('body').addClass('scrolled');
    } else {
      $('body').removeClass('scrolled');
    }
  });

  // Removes 'current_' class in menu for some pages
  if ($('body').hasClass("error404")) {
    $("li").removeClass("current_page_parent").removeClass("current-menu-ancestor").removeClass('current_page_ancestor');
  }

   // Scroll-link
  $('.scroll-link').click(function(e){
    e.preventDefault();
    $('html, body').animate({'scrollTop': $($(this).attr('href')).position().top}, 1000, 'easeInOutCubic');
    if ($('.navbar-collapse').hasClass('in')){
      $('.navbar-collapse').removeClass('in').addClass('collapse');
    }
  });

  // Div has-link
  $('.has-link').click(function(e){
    e.preventDefault();
    var url = $(this).find('a').attr('href');
    window.location = url;
  });

  // Append .bg-img-holder <img>'s as CSS backgrounds
  $('.bg-img-holder').each(function(){
    console.log('bg-img-holder');
    var imgSrc = $(this).children('img').attr('src');
    var imgSrcSet = $(this).children('img').attr('srcset');
    $(this).css('background-image', 'url("' + imgSrc + '")');
    $(this).attr('srcset', imgSrcSet);
    $(this).children('img').hide();
  });



  // Handles the lack of SVG support
  if( $('html').hasClass('no-svg') ) {

    $('img[src*="svg"]').attr('src', function() {
      return $(this).attr('src').replace('.svg', '.png');
    });

  } else {

    // Replaces svg with svg code
    $('img.raw-svg, bg-img-holder.raw-svg').each(function(){
      var image = $(this);
      var imageID = image.attr('id');
      var imageClass = image.attr('class');
      var imageURL = image.attr('src');

      $.get(imageURL, function(data) {
      // Gets the SVG tag, ignore the rest
      var svg = $(data).find('svg');

      // Adds replaced image's ID to the new SVG
      if(typeof imageID !== 'undefined') {
        svg = svg.attr('id', imageID);
      }
      // Adds replaced image's classes to the new SVG
      if(typeof imageClass !== 'undefined') {
        svg = svg.attr('class', imageClass+' replaced-svg');
      }

      // Removes any invalid XML tags as per http://validator.w3.org
      svg = svg.removeAttr('xmlns:a');

      // Replaces image with new SVG
      image.replaceWith(svg);

      }, 'xml');
    });
  }

});

jQuery(window).load(function(){

  jQuery('html').addClass('all-loaded');
  jQuery(window).resize().scroll();
  // jQuery('#page').css('opacity', 1);
  // Remove Loader
  jQuery('.preloader').css('opacity', 0);


});

