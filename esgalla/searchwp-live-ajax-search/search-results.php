<?php
/**
 * Search results are contained within a div.searchwp-live-search-results
 * which you can style accordingly as you would any other element on your site
 *
 * Some base styles are output in wp_footer that do nothing but position the
 * results container and apply a default transition, you can disable that by
 * adding the following to your theme's functions.php:
 *
 * add_filter( 'searchwp_live_search_base_styles', '__return_false' );
 *
 * There is a separate stylesheet that is also enqueued that applies the default
 * results theme (the visual styles) but you can disable that too by adding
 * the following to your theme's functions.php:
 *
 * wp_dequeue_style( 'searchwp-live-search' );
 *
 * You can use ~/searchwp-live-search/assets/styles/style.css as a guide to customize
 */
?>

<div class="px-3 py-4">


<?php
	$swp_query_formaciones = new SWP_Query(array(
		'engine' => 'formaciones',
		's'			 => $_POST['s']
	));
?>


<?php if ( have_posts() ) : ?>
	<?php do_action( 'searchwp_metrics_click_tracking_start' ); ?>
	<span class="searchwp-results-section-title mb-3">Programas formativos</span>
	<div class="container">
		<?php $loop_limit = 4; ?>
		<?php foreach ( $swp_query_formaciones->posts as $formacion ): ?>
		<?php if($loop_limit == 0 ) break; ?>

			<?php $post_type = get_post_type(); ?>
				<div class="row mb-3" role="option" id="" aria-selected="false">
					<div class="col-9 px-0">
						<div class="form-search-result-tit">
							<a href="<?php echo esc_url( get_permalink($formacion->ID) ); ?>" class="form-search-result-link">
								<?php echo get_the_title($formacion->ID); ?>
							</a>
						</div>
					</div>
					<div class="col-3 d-flex justify-content-end align-items-center px-0">
						<?php $tipo_formacion = get_field('tipo', $formacion->ID); ?>
						<?php if($tipo_formacion == 'Carrera profesional') $tipo_formacion = 'Carrera'; ?>
						<span class="badge badge-light my-auto"><img src="<?php echo get_site_url(); ?>/wp-content/themes/esgalla/img/medallita-azul.svg" class="img-fluid raw-svg" /> <?php echo $tipo_formacion; ?></span>
					</div>
				</div>
				<? $loop_limit--; ?>
		<?php endforeach; ?>
		<div class="row">
			<div class="col-12 px-0">
				<small class=""><a href="<?php echo get_site_url(); ?>/formacoes/">Ver todos los programas formativos</a></small>
			</div>
		</div>
	</div>

	<hr class="mt-4 mb-4">
	<?php rewind_posts(); ?>
	<span class="searchwp-results-section-title mb-3">Notícias relacionadas</span>
	<div class="container">
		<?php $loop_limit = 4; ?>
		<?php while ( $loop_limit > 0 && have_posts() ) : the_post(); ?>
			<?php $post_type = get_post_type(); ?>
			<?php if($post_type == 'post'): ?>
				<?php $loop_limit--; ?>
				<div class="row" role="option" id="" aria-selected="false">
					<div class="col-9 px-0 mb-3">
						<div class="form-search-result-tit">
							<a href="<?php echo esc_url( get_permalink() ); ?>" class="form-search-result-link">
								<?php echo get_the_title(); ?>
							</a>
						</div>
					</div>
					<div class="col-3 text-right px-0">
						<img src="<?php the_post_thumbnail_url('thumbnail'); ?>" class="post-image rounded" />
					</div>
				</div>
			<?php endif; ?>
		<?php endwhile; ?>
		<div class="row">
			<div class="col-12 px-0">
				<small class=""><a href="<?php echo get_site_url(); ?>/noticias/">Ver todas las noticias</a></small>
			</div>
		</div>
	</div>
	<?php do_action( 'searchwp_metrics_click_tracking_stop' ); ?>
<?php else : ?>
	<p class="searchwp-live-search-no-results" role="option">
		<em><?php esc_html_e( 'Lo sentimos, no hemos encontrado resultadoss', 'esgalla' ); ?></em>
	</p>
<?php endif; ?>


</div>
