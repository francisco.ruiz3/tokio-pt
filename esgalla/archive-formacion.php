<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package esgalla
 */

get_header();

get_template_part("template-parts/tema", "header");

wp_reset_query(); 
?>

	<main id="archive-formacion" class="site-main">
		<div class="container-fluid bg-tokio-navyblue pt-5 pt-md-5">
			<nav aria-label="breadcrumb">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a class=" text-white" href="<?php echo get_home_url( ) ?>">Inicio</a></li>
						<li class="breadcrumb-item text-tokio-green">Formações</li>
					</ol>
				</div>
			</nav>
			<div class="container full-height-container h-100">
				<div class="row align-items-center h-100">
					<div class="col-lg-6 align-self-center my-5 my-md-0">
						<h1 class="masthead-title text-tokio-green mb-4 wow animate__fadeInUp" data-wow-duration="2s">Formações TIC</h1>
						<p class="masthead-lead titilumregular text-white mb-4">
						Procuras uma <strong>formação com futuro</strong>, que te leve de n00b a samurai? Procuras mais do que uma <strong>formação especializada</strong>, uma formação que se adapte a ti? Procuras... e encontraste! Pronto para dominar a tua área?</p>
					</div>
					<div class="col-lg-6 order-lg-last align-self-center text-lg-right" >
						<!-- <div id="parallaxhoverx">
							<div data-depth="0.2"> -->
								<img src="<?php echo get_template_directory_uri() ?>/img/programas-formativos-hero.jpg" class="img-fluid"/>
							<!-- </div>
						</div> -->
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid categorias-cursos-nav pt-3">
			<div class="container ">
				<div class="row">
					<div class="col-12">
						<? $loop = 0; ?>
						<? foreach (get_terms('categorias_formacion',['exclude' => [6, 96]]) as $categoria): ?>
							<? if($loop == 0): ?>
								<span class="nav-item titilumsemibold d-inline-block active pt-3 pb-4 mr-5" data-grid="<? echo $categoria->slug; ?>"><? echo $categoria->name; ?></span>
							<? else: ?>
								<span class="nav-item titilumsemibold d-inline-block pt-3 pb-4 mr-5" data-grid="<? echo $categoria->slug; ?>"><? echo $categoria->name; ?></span>
							<? endif; ?>
							<? $loop++; ?>
						<? endforeach; ?>
					</div>
				</div>
			</div>
		</div>




<?php
	//foreach (get_terms('categorias_formacion',['exclude' => [6, 96]]) as $categoria) {
	foreach (get_terms('categorias_formacion') as $categoria) {
		if($categoria->count > 0){
			$cat_activo = ""; if($categoria->slug == "design-e-programacao-de-videojogos") $cat_activo = " active";
			echo '<div class="container formaciones-grid '.$categoria->slug.'-grid py-5 pb-md-6 '.$cat_activo.'">';
				echo '<div class="row">';
							$posts_categoria = get_posts(
								array(
									'post_type' => 'formacion',
									'posts_per_page' => -1,
									'tax_query' => array(
										'relation' => 'AND',
										array(
											'taxonomy' => 'categorias_formacion',
											'field' => 'slug',
											'terms' => $categoria->slug
										),
										//Excluimos las carreras profesionales de este listado
										array(
											'taxonomy' => 'tag_formacion',
											'terms'    => ['carreras-profesionales', 'cursos-complementarios'],
											'field'    => 'slug',
											'operator' => 'NOT IN',
										)
									)
								)
							);
							foreach ($posts_categoria as $post) {
								echo '<div class="col-md-6 col-lg-4 mb-4">';
								echo '<div class="card formacion-card rounded h-100" style="">
									<div class="card-img-top-container bg-img-holder rounded mx-3 mt-3">
										<img src="' . get_field('imagen_tarjetas', $post->ID)['sizes']['medium'] . '" class="img-fluid" alt="' . get_field('cabecera', $post->ID)['titulo_cabecera'] . '"/>
									</div>
									<div class="card-body">
										<div class="formacion-badges d-flex my-4">
											<span class="badge text-white badge-tokio-green mr-3 "><img src="'.get_template_directory_uri().'/img/badge-award.svg" class="img-fluid"/> '.get_field('tipo', $post->ID).'</span>
											<span class="badge text-white badge-tokio-green mr-3 "><img src="'.get_template_directory_uri().'/img/badge-timer.svg" class="img-fluid"/> '.get_field('duracion', $post->ID).' h</span>
											<span class="badge text-white badge-tokio-green ml-auto">'.get_field('modalidad', $post->ID).'</span>
										</div>
										<h2 class="h5 card-title text-primary">'.get_field('cabecera', $post->ID)['titulo_cabecera'].'</h2>
										<p class="card-text titilumregular">'.get_field('descripcion_tarjetas', $post->ID).'</p>
										<a href="'.get_the_permalink($post->ID).'" class="stretched-link"></a>
									</div>
								</div>';
						echo '</div>';
							}
				echo '</div>';
			echo '</div>';
		}
	}
?>

	</main><!-- #main -->


	<? if( get_field('id') ): ?>
	
	<? endif; ?>


<?php
// get_sidebar();
get_footer();
