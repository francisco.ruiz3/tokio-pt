<?php
/**
* Template Name: Gracias
*
* @package esgalla
*/
get_header();
get_template_part("template-parts/tema", "header");

?>
<header id="masthead" class="site-header fullheight position-relative">
	<div class="container-fluid bg-tokio-navyblue bg-404">
		<div class="container full-height-container h-100 py-5 py-md-6">
			<div class="row align-items-center h-100">
				<div class="col-lg-12 align-self-center text-center">
					<h1 class="masthead-title text-white mb-4">Obrigado</h1>
					<!-- <div class="display-1 font-weight-bolder text-white big404">404</div> -->
					<p class="text-white mb-5">O formulário foi enviado com sucesso. A nossa equipa irá entrar em contacto contigo o mais rápido possível.</p>
					<div class="masthead-links text-center">
						<a class="btn rounded-pill text-white btn-tokio-green py-3 px-4" href="<?php echo home_url(); ?>">Voltar ao inicio</a>
					</div>
				</div>


			</div>
		</div>
	</div>


</header><!-- #masthead -->

<?php
get_footer();

