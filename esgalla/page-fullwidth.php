<?php
/**
* Template Name: Full width
*
* This is the template that displays full width pages
*
*
* @package esgalla
*/

get_header();
?>

	<nav aria-label="breadcrumb">
		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item"><a href="#">Library</a></li>
				<li class="breadcrumb-item active" aria-current="page">Data</li>
			</ol>
		</div>
	</nav>

	<main id="primary" class="site-main py-5">

		<div class="container">
			<div class="row">
				<div class="col-sm-12">

					<?php
					while ( have_posts() ) :
						the_post();

						get_template_part( 'template-parts/content', 'page' );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>

				</div>
			</div>
		</div>

	</main><!-- #main -->

<?php
get_footer();
