<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package esgalla
 */

get_header();
get_template_part("template-parts/tema", "header");

?>

<header id="masthead" class="site-header fullheight position-relative">
  <main>
	<div class="container-fluid bg-tokio-navyblue bg-404">
		<div class="container full-height-container h-100 py-5 py-md-6">
			<div class="row align-items-center h-100">
				<div class="col-lg-6 align-self-center text-center mx-auto">
					<h1 class="masthead-title text-white mb-4">Erro</h1>
          <div class="404lolflip mb-4">
            <div class="display-1 font-weight-bolder text-white big404 front">404</div>
            <div class="display-1 font-weight-bolder text-white big404 back" style="visibility:hidden;">LOL</div>
          </div>
					<h2 class="masthead-title h3 md-h2 text-white mb-5">Permite um erro como este uma vez na vida! </h2>
					<div class="masthead-links text-center">
						<a class="btn rounded-pill text-white btn-tokio-green py-3 px-4" href="<?php echo home_url(); ?>">Voltar ao início</a>
					</div>
				</div>


			</div>
		</div>
	</div>

  </main>
</header><!-- #masthead -->


<?php
get_footer();
