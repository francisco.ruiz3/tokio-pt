<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package esgalla
 */

?>

<?php if( is_single( 4040 ) ) {}
    else{ ?>
	<footer id="colophon" class="site-footer">

		<aside id="tertiary" class="site-footer-menus widget-area pt-4 pb-4  pb-md-3 pt-lg-6">
			<div class="container">
				<div class="row">

					<?php
						if ( ! is_active_sidebar( 'first-footer-widget-area' ) ) {
						return;
					}?>

					<div class="col-sm-12 col-md-3 text-left py-3">
						<div id="footer-menu-1" class="widget">
							<?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
						</div><!-- #secondary -->
					</div>

					<?php
						if ( ! is_active_sidebar( 'second-footer-widget-area' ) ) {
						return;
					}?>

					<div class="col-sm-6 col-md-3 text-left py-3">
						<div id="footer-menu-2" class="widget">
							<?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
						</div><!-- #secondary -->
					</div>

					<?php
						if ( ! is_active_sidebar( 'third-footer-widget-area' ) ) {
						return;
					}?>

					<div class="col-sm-6 col-md-3 text-left py-3">
						<div id="footer-menu-3" class="widget">
							<?php dynamic_sidebar( 'third-footer-widget-area' ); ?>
						</div><!-- #secondary -->
					</div>

					<?php
						if ( ! is_active_sidebar( 'fourth-footer-widget-area' ) ) {
						return;
					}?>

					<div class="col-sm-6 col-md-3 d-flex flex-column py-3 mb-3 widget">
						<div class="h3 text-tokio-blue h6 mb-3 fs-20">Segue-nos</div>
						<div class="rrss-icons">
							<a class="icon-instagram" href="https://www.instagram.com/tokioschoolportugal" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/Instagram.svg" class="img-fluid raw-svg mr-3"/></a>
							<a class="icon-facebook" href="https://facebook.com/tokioschoolportugal" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/facebook-fill.svg" class="img-fluid raw-svg mr-3"/></a>
							<a class="icon-linkedin" href="https://www.linkedin.com/school/tokioschoolportugal" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/linkedin-fill.svg" class="img-fluid raw-svg mr-3"/></a>
							<a class="icon-youtube" href="https://www.youtube.com/channel/UC2y3CnU6sV6bRkBSDZOWfvw" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/youtube.svg" class="img-fluid raw-svg mr-3"/></a>
							<a class="icon-twitch" style="width: 32px;" href="https://www.twitch.tv/tokioschool" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/logo-twitch.svg" class="img-fluid raw-svg "/></a>
						</div>
						<div class="py-3">
							<ul>
								<li class=""><a href="<? echo get_home_url() .'/mapa-del-sitio/'; ?>">Mapa do sitio</a></li>
								<li class=""><a href="<? echo get_home_url() .'/politica-de-cookies/'; ?>">Política de Cookies</a></li>
								<li class=""><a href="<? echo get_home_url() .'/politica-privacidad/'; ?>">Política de Privacidade</a></li>
							</ul>
							
						</div>

					</div>

				</div>
			</div>
		</aside><!-- .site-footer-menus -->


	</footer><!-- #colophon -->

</div><!-- #page -->

<?php } ?>
<?php wp_footer(); ?>

<?php if( is_single( 4040 ) ) {}
    else{ ?>

<div class="menu-overlay"></div>
	<!-- <div class="chatbot-icon">
		<img src="<?php echo get_template_directory_uri() ?>/img/Chatbot.svg" class="img-fluid rounded" />
	</div> -->

	<!--
	**	Popup Botón solicitar información
	**
	-->

	<div class="modal fade modal-contacto form-contacto px-0" id="modal-contacto" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content d-flex align-items-center bg-tokio-navyblue" style="">
	      <div class="modal-header align-self-end">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="w-100"></div>
	      <div class="modal-body">
				<div class="h3 text-tokio-green mb-0">Queres saber mais sobre os nossos cursos?</div>
				<p class="text-white my-4">Deixa-nos os teus dados e o que procuras, entraremos em contacto contigo para te dar toda a informação que precisas.</p>
	        <?php //echo do_shortcode( '[gravityform id="1" title="false" description="false" ajax="true" tabindex="49"]' ); ?>
			  <?php
			  gravity_form( 9, false, false, false, array(
				  "nombrecurso" => $GLOBALS['product_name'], 
					"idcurso" => $GLOBALS['product_id'], 
					"idpais" => "1", "modollegada" => $MLL,
				  "producto_tipo" => $GLOBALS['product_type'], 
					"producto_categoria" => $GLOBALS['product_category'], 
					"prelead_name" => get_the_title(), 
					"prelead_tipo" => $GLOBALS['page_type'],
				  "pdf" => get_field("catalogo")
			  ), true, 900);
			  ?>
	      </div>
	    </div>
	  </div>
	</div>


	<div class="modal fade modal-contacto form-contacto" id="modal-preinscribe" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content d-flex align-items-center bg-tokio-navyblue">
	      <div class="modal-header align-self-end">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="w-100"></div>
			<div class="modal-body">
				<div class="h3 mb-0 text-tokio-green"><?php echo $GLOBALS['product_name']; ?></div>
				<p class="my-4 text-white">Rellena tus datos y nos pondremos en contacto contigo para sacarte de dudas. ¡El camino para convertirte en un tokier empieza aquí!</p>
				<?php //echo do_shortcode( '[gravityform id="1" title="false" description="false" ajax="true" tabindex="49"]' ); ?>
				<?php
				gravity_form( 8, false, false, false, array(
					"nombrecurso" => $GLOBALS['product_name'], 
					"idcurso" => $GLOBALS['product_id'], 
					"idpais" => "1", 
					"modollegada" => $MLL,
					"producto_tipo" => $GLOBALS['product_type'], 
					"producto_categoria" => $GLOBALS['product_category'], 
					"prelead_name" => get_the_title(), 
					"prelead_tipo" => $GLOBALS['page_type'],
					"pdf" => get_field("catalogo")
				), true, 800);
				?>
			</div>
	    </div>
	  </div>
	</div>

	<!--
	**  Popup mensaje confirmación para formulario integrado en cuerpo
	**
	-->
	<div class="modal fade modal-gracias" id="modal-gracias" tabindex="-1" role="dialog" aria-labelledby="formularioenviado" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content d-flex align-items-center">
		    <div class="modal-header align-self-end">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		           <span aria-hidden="true">&times;</span>
		        </button>
		    </div>
		    <div class="w-100"></div>
		    <div class="modal-body">
					<div class="h3" style="text-align: center; font-size: 32px;">¡Muchas gracias!</div>
					<p style="text-align: center;">El formulario ha sido enviado correctamente. Nuestro equipo se pondrá en contacto contigo lo antes posible.</p>
				</div>
	    </div>
	  </div>
	</div>

	<div class="d-md-none sticky-footer-mobile text-center p-2 shadow">
		
		<?php if(!is_singular("landing-elementor")): ?>
			<a class="btn btn-primary rounded-pill btn-navbar px-4 ml-lg-4" role="button" data-toggle="modal" data-target="#<? echo ((get_field('id') && (!is_archive())) ? 'modal-preinscribe' : 'modal-contacto') ?>" href="javascript:void(0)">Quero saber mais</a><? //echo get_field('id'); ?>
		<?php else: ?>
			<a class="btn btn-primary rounded-pill btn-navbar px-4 ml-lg-4" role="button" href="#form-fixed">Quero saber mais </a>
		<?php endif; ?>
	</div>


	<div class="modal fade modal-buscador-mobile" id="modal-buscador-mobile" tabindex="-1" role="dialog" aria-labelledby="buscadormobile" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content d-flex align-items-center">
		    <div class="modal-header align-self-end">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
					</button>
		    </div>
		    <div class="w-100"></div>
		    <div class="modal-body py-5">
					<div class="container">
						<div class="row">
							<div class="col-12 text-center">
								<p class="h2 mb-5">¿En qué estás interesado?</p>
								<div>
									<style>
										#modal-buscador-mobile form {
											width: 100%;
										}
										#modal-buscador-mobile input {
											font-size: 18px!important;
											border: 1px solid #1C233D!important;
											color: #1C233D!important;
										}
										#modal-buscador-mobile input::placeholder {
											color: #1C233D!important;
										}

										#modal-buscador-mobile .searchwp-container {
												width: 100%;
										}

										html.mobile .searchwp-live-search-results {
											width: 100vw !important;
											left: 0!important;
											overflow-y: scroll!important;
											max-height: 60vh!important;
										}
									
									</style>
									<? echo do_shortcode('[shortcode_buscador]'); ?>
								</div>
							</div>
						</div>
					</div>
					
				</div>
	    </div>
	  </div>
	</div>

<?php } ?>

<div class="cookies-ajax-container"></div>

</body>
</html>
