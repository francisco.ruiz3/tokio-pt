<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package esgalla
 */

get_header();
get_template_part("template-parts/tema", "header");
?>

	<main id="primary" class="site-main pt-0 pb-5">

		<div class="container-fluid px-0">

					<?php
					while ( have_posts() ) :
						the_post();

						// 6 - Portada
						//18 - Descubre Tokio
						//38 - FAQs
						//53 - Noticias
						//58 - Contacto
						//60 - Para empresas
						switch (get_the_ID()) {
							case 6:
								get_template_part( 'template-parts/content', 'home' );
							break;
							case 18:
								get_template_part( 'template-parts/content', 'descubre-tokio' );
							break;
							case 38:
								get_template_part( 'template-parts/content', 'faqs' );
							break;
							case 53:
								get_template_part( 'template-parts/content', 'noticias' );
							break;
							case 58:
								get_template_part( 'template-parts/content', 'contacto' );
							break;
							case 60:
								get_template_part( 'template-parts/content', 'para-empresas' );
							break;

							default:
								get_template_part( 'template-parts/content', 'page' );
							break;
						}
						//get_template_part( 'template-parts/content', 'page' );

					endwhile; // End of the loop.
					?>


		</div>

	</main><!-- #main -->

<?php
get_footer();
