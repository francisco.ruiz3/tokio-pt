<?php
/**
 * esgalla functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package esgalla
 */

include("func/registro-elementos.php");
include("func/analitica-dl.php");

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'esgalla_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function esgalla_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on esgalla, use a find and replace
		 * to change 'esgalla' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'esgalla', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu().
		register_nav_menus( array(
			'primary'      => esc_html__( 'Menú Principal', 'esgalla' ),
			'mobile'       => esc_html__( 'Menú Movíl', 'esgalla' ),
			'footer'       => esc_html__( 'Menú Footer', 'esgalla' )
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'esgalla_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);

	}
endif;
add_action( 'after_setup_theme', 'esgalla_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function esgalla_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'esgalla_content_width', 640 );
}
add_action( 'after_setup_theme', 'esgalla_content_width', 0 );


if ( ! function_exists( 'esgalla_header_menu' ) ) :
	/**
	 * Header menu (should you choose to use one)
	 */
	function esgalla_header_menu() {
		// display the WordPress Custom Menu if available
		// wp_reset_query();
		// wp_reset_postdata();
		wp_nav_menu(array(
			'menu'              => esc_html__( 'Menú Principal', 'esgalla' ),
			'menu_class'        => 'navbar-nav mr-auto ml-3',
			'theme_location'    => 'primary',
			'container'       	=> 'div',
			'container_id'    	=> 'navbarSupportedContent',
			'container_class' 	=> 'collapse navbar-collapse',
			'depth'             => 2,
			'fallback_cb'    	=> 'WP_Bootstrap_Navwalker::fallback',
			'walker'         	=> new WP_Bootstrap_Navwalker(),
			// 'items_wrap' 		=> '<ul id="%1$s" class="%2$s">%3$s</ul><a class="btn btn-navbar text-primary px-4 ml-lg-2" href="#"><img src="https://www.tokioschool.com/wp-content/themes/esgalla/img/search.svg" class="img-fluid icon-search" />Buscar</a><a class="btn btn-primary btn-navbar px-4 ml-lg-2" role="button" data-toggle="modal" data-target="#modal-contacto" href="javascript:void(0)">Solicitar información</a>'
			'items_wrap' 		=> '<ul id="%1$s" class="%2$s">%3$s</ul><img src="https://www.tokioschool.com/wp-content/themes/esgalla/img/person_blue.svg" class="img-fluid icon-person" /><a  class="fs-14 font-weight-bold text-primary" href="https://learning.tokioschool.com/login/login-image/login.php">Plataforma virtual</a><a class="btn btn-primary rounded-pill btn-navbar px-4 ml-lg-4" role="button" data-toggle="modal" data-target="#' . ((get_field('id') && (!is_archive())) ? 'modal-preinscribe' : 'modal-contacto') . '" href="javascript:void(0)">Quero saber mais</a>'
		));
	} /* end esgalla header menu */
endif;

if ( ! function_exists( 'esgalla_mobile_menu' ) ) :
/**
 * Mobile menu
 */
function esgalla_mobile_menu() {
	// display the WordPress Custom Menu if available
	wp_nav_menu(array(
		'menu'              => esc_html__( 'Menú Movíl', 'esgalla' ),
		'menu_class'        => 'navbar-nav',
		'theme_location'    => 'primary',
		'container'       	=> 'div',
		'container_id'    	=> 'navbarSupportedContent',
		'container_class' 	=> 'collapse navbar-collapse navBarMobile',
		'depth'             => 2,
		'fallback_cb'    	=> 'WP_Bootstrap_Navwalker::fallback',
		'walker'         	=> new WP_Bootstrap_Navwalker(),
		'items_wrap' 		=> '<div class="d-flex flex-column justify-content-between h-100"><ul id="%1$s" class="%2$s"><a class="navbar-toggler order-xl-2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation">
		<img src="'.get_template_directory_uri().'/img/close.svg" class="raw-svg img-fluid"/>
</a> %3$s</ul>
										<div class="secondary-mobile-menu d-flex flex-column">
											<div class="mobile-menu-divider"></div>
											<a href="' . get_permalink(53) . '">Back Up Blog</a>
											<a href="' . get_permalink(58) . '">Contacto</a>
											<a href="https://learning.tokioschool.com/login/login-image/login.php">Plataforma virtual</a>
											<a href="tel:+34918281978">Información: <span class="text-secondary">918 281 978</span></a>
										</div>
									</div>
								'


	));
} /* end esgalla mobile menu */
endif;

if ( ! function_exists( 'esgalla_footer_menu' ) ) :
/**
 * Footer menu
 */
function esgalla_footer_menu() {
	// display the WordPress Custom Menu if available
	wp_nav_menu(array(
		'menu'              => esc_html__( 'Menú Pié de Página', 'esgalla' ),
		'menu_class'        => 'navbar-nav ml-auto',
		'theme_location'    => 'footer',
		'container'       	=> 'div',
		'container_id'    	=> 'navbarSupportedContent',
		'container_class' 	=> 'collapse navbar-collapse',
		'depth'             => 1,
		'fallback_cb'    	=> 'WP_Bootstrap_Navwalker::fallback',
		'walker'         	=> new WP_Bootstrap_Navwalker()

	));
} /* end esgalla footer menu */
endif;

/**
 * Register footer widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function esgalla_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'esgalla' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'esgalla' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="h2 text-primary widget-title">',
		'after_title'   => '</div>',
	) );

	// First footer widget area, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => esc_html__( 'First Footer Widget Area', 'esgalla' ),
		'id' => 'first-footer-widget-area',
		'description' => esc_html__( 'The first footer widget area', 'esgalla' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="h3 text-tokio-blue widget-title">',
		'after_title' => '</div>',
	) );

	// Second Footer Widget Area, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => esc_html__( 'Second Footer Widget Area', 'esgalla' ),
		'id' => 'second-footer-widget-area',
		'description' => esc_html__( 'The second footer widget area', 'esgalla' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="h3 text-tokio-blue widget-title">',
		'after_title' => '</div>',
	) );

	// Third Footer Widget Area, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => esc_html__( 'Third Footer Widget Area', 'esgalla' ),
		'id' => 'third-footer-widget-area',
		'description' => esc_html__( 'The third footer widget area', 'esgalla' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="h3 text-tokio-blue widget-title">',
		'after_title' => '</div>',
	) );

	// Fourth Footer Widget Area, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => esc_html__( 'Fourth Footer Widget Area', 'esgalla' ),
		'id' => 'fourth-footer-widget-area',
		'description' => esc_html__( 'The fourth footer widget area', 'esgalla' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="h3 text-tokio-blue widget-title">',
		'after_title' => '</div>',
	) );

	// Fifth Footer Widget Area, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => esc_html__( 'Fifth Footer Widget Area', 'esgalla' ),
		'id' => 'fifth-footer-widget-area',
		'description' => esc_html__( 'The fifth footer widget area', 'esgalla' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="h3 text-tokio-black widget-title">',
		'after_title' => '</div>',
	) );
}
add_action( 'widgets_init', 'esgalla_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function esgalla_scripts() {
	// Add Bootstrap default CSS

	if ( !is_admin() ) {

		wp_enqueue_style( 'custom-css', get_template_directory_uri() . '/css/custom.css', array(), date('Ymdijs') );
		wp_enqueue_style( 'custom-css', get_template_directory_uri() . '/style.css' );
		wp_enqueue_style( 'fa-css', get_template_directory_uri() . '/css/fontawesome.min.css' );
		wp_enqueue_style( 'cookiescss', get_template_directory_uri() . '/css/cookies.css' );
		wp_enqueue_style( 'esgalla-style', get_stylesheet_uri(), array() );
		// wp_style_add_data( 'esgalla-style', 'rtl', 'replace' );

		// Analítica web
		wp_enqueue_script( 'insertcookiesajax', get_template_directory_uri() . '/js/insert-cookies-ajax.js', array('jquery'), false, true );
	    wp_localize_script( 'insertcookiesajax', 'parametros_insertcookiesajax', array(
			'ajaxURL' => site_url() . '/wp-admin/admin-ajax.php'
		) );

		wp_enqueue_script( 'analitica-dl', get_template_directory_uri() . '/js/analitica-dl.js', array('jquery') );

		wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery') );

		wp_enqueue_script('core-js', get_template_directory_uri() . '/js/core.min.js', array('jquery') );

		// wp_enqueue_script('easing-js', get_template_directory_uri() .'/js/jquery.easing.min.js', array('jquery'), false, true );

		// Main theme related functions
		wp_enqueue_script( 'functions-js', get_template_directory_uri() . '/js/functions.min.js', array('jquery') );

		// Main js file
		wp_enqueue_script( 'scripts-js', get_template_directory_uri() . '/js/scripts.js', array('jquery') );
		wp_enqueue_script( 'cookiesjs', get_template_directory_uri() . '/js/cookies.js', array('jquery'), false, true );

		// Formulario pasos
		wp_enqueue_script( 'stepform', get_template_directory_uri() . '/js/stepform.js', array('jquery') );
		if(is_page(60)) wp_enqueue_script( 'para-empresas', get_template_directory_uri() . '/js/para-empresas.js', array('jquery') );

		if(is_page(58)) wp_enqueue_script( 'map', get_template_directory_uri() . '/js/map.js', array('jquery') );

		// wp_enqueue_script( 'esgalla-navigation', get_template_directory_uri() . '/js/navigation.js', array(), true );

		
		if(get_post_type() == 'landing-elementor') {
			wp_enqueue_style( 'landing-elementor-css', get_template_directory_uri() . '/css/landings/landings.css' );
		}

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

	}
}
add_action( 'wp_enqueue_scripts', 'esgalla_scripts' );

/**
 * Remove Gutenberg Block Library CSS from loading on the frontend
 */
function smartwp_remove_wp_block_library_css(){

	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style( 'wp-block-library-theme' );
	wp_dequeue_style( 'wc-block-style' ); // Remove WooCommerce block CSS

}
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100 );


/**
 * Implement the Custom Header feature.
 */
// require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
// require get_template_directory() . '/inc/customizer.php';

/**
 * Load custom nav walker
 */
require get_template_directory() . '/inc/wp-bootstrap-navwalker.php';

/**
 * Load Jetpack compatibility file.
 */
// if ( defined( 'JETPACK__VERSION' ) ) {
// 	require get_template_directory() . '/inc/jetpack.php';
// }


function ec_get_template_part($parte1, $parte2 = null, $variables = null) {
	if ($variables && count($variables)) {
		foreach ($variables as $nombre => $valor) {
			set_query_var( $nombre, $valor );
		}
	}
	get_template_part($parte1, $parte2);
}









//Auxiliar functions
function numberToRomanRepresentation($number) {
  $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
  $returnValue = '';
  while ($number > 0) {
      foreach ($map as $roman => $int) {
          if($number >= $int) {
              $number -= $int;
              $returnValue .= $roman;
              break;
          }
      }
  }
  return $returnValue;
}



add_shortcode('shortcode_buscador', 'buscador');
function buscador() {
	ob_start();//Pare devolver tempalte parts

	$t = (isset($atts["t"])) ? $atts["t"] : "principal"; //t --> tipo de elemento
	$p = (isset($atts["p"])) ? $atts["p"] : __("Procurar...", "esgalla"); //t --> tipo de elemento

	// echo '<form role="search" action="' . get_home_url() . '" method="get" class="form-search form-search-' . $t . '">';
	// echo '<input type="text" name="s" value="" data-swplive="true" placeholder="' . $p . '"/>';
	// echo '<button type="submit"><span class="fas fa-search"></span></button>';
	// echo '</form>';
	echo '<form role="search" action="' . get_home_url() . '" method="get" class="float-left form-search form-search-principal d-flex" style=""><span class="d-inline searchwp-container px-0 pr-2"><input type="text" class="form-control fs-14" id="searchwp" placeholder="Procurar..." name="s"  data-swplive="true"></span></form>';

	return ob_get_clean(); //Devolvemos los tempalte parts
}


add_filter( 'facetwp_facet_html', function( $output, $params ) {
	if ( 'dropdown' == $params['facet']['type'] ) {
		$output = str_replace( 'facetwp-dropdown', 'facetwp-dropdown form-control', $output );
	}
	return $output;
}, 10, 2 );




//------------------------------------------------------------------ ACF
add_filter('acf/settings/google_api_key', function () { //--- Añadimos la Api para Google Map
   return 'AIzaSyAo5wQLFXDyTcr-ZXSE2fvElmZ4K1kURsg';
});





//------------------------------------------------------------------ Gravity Forms
function rellenar_selector_cursos( $form ) {
	// print_r($form);
	foreach ( $form['fields'] as &$field ) {
		if ( $field->type == 'select' && strpos( $field->cssClass, 'gf-selector_curso' ) !== false ) {
			//Metemos los hijos de haberlos
			$args = array(
				'numberposts' => -1,
				'order' => 'ASC',
				'orderby' => 'title',
				// 'post_status' => null,
				// 'post_parent' => get_the_ID(),
				'post_type' => 'formacion',
				'tax_query' => array(
					array(
						'taxonomy' => 'categorias_formacion',
						'field' => 'id',
						'terms' => array(7, 8, 9, 11, 17)
					)
				)
			);

			$hijos = get_posts( $args );
			// print_r($hijos);
			$choices = array();
			$choices_carreras = [];
			foreach ( $hijos as $x ) {
				if(get_field('tipo', $x->ID) != 'Carrera profesional') {
					$choices[] = array( 'text' => get_the_title($x->ID), 'value' => get_field("id",$x->ID).'|'.sanitize_title(get_field('tipo', $x->ID)) );
				} else {
					$choices_carreras[] = array( 'text' => get_the_title($x->ID), 'value' => get_field("id",$x->ID).'|'.sanitize_title(get_field('tipo', $x->ID)) );
				}
			}
			//Concatenamos las carreras al final del array
			$choices = array_merge($choices, $choices_carreras);
			$field->placeholder = 'Seleciona o curso em que estás interessado *';
			$field->choices = $choices;
		}

	}

	return $form;
}
add_filter( 'gform_pre_render_9', 'rellenar_selector_cursos' );
add_filter( 'gform_pre_validation_9', 'rellenar_selector_cursos' );
add_filter( 'gform_pre_submission_filter_9', 'rellenar_selector_cursos' );
add_filter( 'gform_admin_pre_render_9', 'rellenar_selector_cursos' );
add_filter( 'gform_pre_render_10', 'rellenar_selector_cursos' );
add_filter( 'gform_pre_validation_10', 'rellenar_selector_cursos' );
add_filter( 'gform_pre_submission_filter_10', 'rellenar_selector_cursos' );
add_filter( 'gform_admin_pre_render_10', 'rellenar_selector_cursos' );




function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
 }
 add_filter('upload_mimes', 'cc_mime_types');



// Changes Gravity Forms Ajax Spinner (next, back, submit) to a transparent image
// this allows you to target the css and create a pure css spinner like the one used below in the style.css file of this gist.
add_filter("gform_submit_button", "form_submit_button", 10, 2);
function form_submit_button($button, $form){
    return "<input type='submit' value='{$form['button']['text']}' class='gform_button button' id='gform_submit_button_{$form["id"]}'><span class='submit-bs-loader spinner-border spinner-border-sm mr-1 d-none' role='status' aria-hidden='true'></span></input>";
}
add_filter( 'gform_ajax_spinner_url', 'spinner_url', 10, 2 );
function spinner_url( $image_src, $form ) {
	return  ' '; // relative to you theme images folder
}


//Eliminamos scroll al intentar submitear formularios
add_filter( 'gform_confirmation_anchor', '__return_false' );




//------------------------------------------Permalink Manager Pro compatibility with Metrics
class SWP_Set_Metrics_Tracking{
	private $click_params;
	private $loop_starts = 0;
	private $loop_ends = 0;

	function __construct(){
			add_filter( 'post_link',        array( $this, 'get_click_params' ), 20 );
			add_filter( 'page_link',        array( $this, 'get_click_params' ), 20 );
			add_filter( 'post_type_link',   array( $this, 'get_click_params' ), 20 );
			add_filter( 'attachment_link',  array( $this, 'get_click_params' ), 20 );
			add_filter( 'the_permalink',    array( $this, 'get_click_params' ), 20 );
			add_filter( 'post_link',        array( $this, 'set_click_params' ), 999 );
			add_filter( 'page_link',        array( $this, 'set_click_params' ), 999 );
			add_filter( 'post_type_link',   array( $this, 'set_click_params' ), 999 );
			add_filter( 'attachment_link',  array( $this, 'set_click_params' ), 999 );
			add_filter( 'the_permalink',    array( $this, 'set_click_params' ), 999 );

			add_action( 'loop_start', function() {
					$this->loop_starts++;
			});

			add_action( 'loop_end', function() {
					$this->loop_ends++;
			});
	}

	function get_click_params( $url ){
			if ( ! $this->is_search_results_loop() && ! $this->doing_link_tracking() ) {
					return $url;
			}
			$url_query = parse_url( $url, PHP_URL_QUERY );
			parse_str( $url_query, $params );
			$this->click_params = $params;
	}

	function set_click_params( $url ){
			if ( ! $this->is_search_results_loop() && ! $this->doing_link_tracking() ) {
					return $url;
			}

			if( ! empty( $this->click_params ) ){
					$url = add_query_arg( $this->click_params, $url );
			}
			return $url;
	}

	function is_search_results_loop() {
			// This check accounts for circumstances where multiple Loops are on the page
			return is_main_query() && ( is_search() || did_action( 'searchwp_after_query_index' ) ) && $this->loop_starts > $this->loop_ends;
	}

	function doing_link_tracking() {
			return did_action( 'searchwp_metrics_click_tracking_start' ) && ! did_action( 'searchwp_metrics_click_tracking_stop' );
	}
}
new SWP_Set_Metrics_Tracking();



function wpbeginner_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	echo '<div id="masnumpages" style="display:none;">';
	echo '</div>';
	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
			return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/** Add current page to the array */
	if ( $paged >= 1 )
			$links[] = $paged;

	/** Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
			$links[] = $paged - 1;
			$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
			$links[] = $paged + 2;
			$links[] = $paged + 1;
	}

	echo '<div class="navigation"><ul class="pagination justify-content-center">' . "\n";

	/** Previous Post Link */
	if ( get_previous_posts_link() )
			printf( '<li class="page-item">%s</li>' . "\n", get_previous_posts_link('<span class="page-link"><</span>') );

	/** Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
			$class = 1 == $paged ? ' class="page-item active"' : '';

			printf( '<li%s class="page-item"><a href="%s" class="page-link">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

			if ( ! in_array( 2, $links ) )
					echo '<li class="page-item">…</li>';
	}

	/** Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
			$class = $paged == $link ? ' class="page-item active"' : '';
			printf( '<li%s class="page-item"><a href="%s" class="page-link">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/** Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
			if ( ! in_array( $max - 1, $links ) )
					echo '<li class="page-item">…</li>' . "\n";

			$class = $paged == $max ? ' class="page-item active"' : '';
			printf( '<li%s class="page-item"><a href="%s" class="page-link">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/** Next Post Link */
	if ( get_next_posts_link() )
			printf( '<li class="page-item">%s</li>' . "\n", get_next_posts_link('<span class="page-link">></span>') );

	echo '</ul></div>' . "\n";

}

//Remove items from Sitemap
add_filter( 'wpseo_exclude_from_sitemap_by_post_ids', function () {
  return array( 
		3900, 	//Gracias Landing
		3536, 	//Gracias Lead
		3567, 	//Mapa del Sitio
	);
} );

/**
 * Pintar template part por ajax
 */
function template_part_ajax() {

    // $GLOBALS["form"] = $_REQUEST['form'];
    // $GLOBALS["producto_banner"] = $_REQUEST['producto_banner'];
    // $GLOBALS["producto_id"] = $_REQUEST['producto_id'];
    // $GLOBALS["form_tipo"] = $_REQUEST['form_tipo'];
    // $GLOBALS["producto_id_wp"] = $_REQUEST['producto_id_wp'];
    // $GLOBALS["producto_name"] = $_REQUEST['producto_name'];
    // $GLOBALS["producto_cat"] = $_REQUEST['producto_cat'];
    // $GLOBALS["pagina_name"] = $_REQUEST['pagina_name'];

    // wp_send_json('holas');

    $gtm_var_ajax = [
        'form' => $_REQUEST['form'],
        'producto_banner' => $_REQUEST['producto_banner'],
        'producto_id' => $_REQUEST['producto_id'],
        'form_tipo' => $_REQUEST['form_tipo'],
        'form_seccion' => $_REQUEST['form_seccion'],
        'producto_id_wp' => $_REQUEST['producto_id_wp'],
        'producto_name' => $_REQUEST['producto_name'],
        'producto_cat' => $_REQUEST['producto_cat'],
        'pagina_name' => $_REQUEST['pagina_name'],
        'full_url' => $_REQUEST['full_url'],
        'pagina_seccion' => $_REQUEST['pagina_seccion'],
    ];

    $return = [];
    switch ($_REQUEST['template_part']) {
        
        case 'cookies-form':
            $return['cookiesform'] = load_template_part("template-parts/cookies", "", []);
        break;
        default:
            # code...
        break;
    }

    wp_send_json($return);
}


// creating Ajax call for WordPress
add_action('wp_ajax_nopriv_template_part_ajax', 'template_part_ajax');
add_action('wp_ajax_template_part_ajax', 'template_part_ajax');


function load_template_part($template_name, $part_name=null, $params) {
    ob_start();
    get_template_part($template_name, $part_name, $params);
    $var = ob_get_contents();
    ob_end_clean();
    return $var;
}

//Controlar teléfono para Portugal
add_filter('gform_field_validation', 'custom_phone', 10, 4);

function custom_phone($result, $value, $form, $field){    
    
	//if ($field->cssClass == "gf-tlf") {//Teléfono sacado de CEMP, lo dejo aquí por si hace falta
		if ($field->type == 'phone') {//Teléfono
        $input = str_replace(array(" ", "-"), "", $value);
        $pattern = "/^(\+351|0351|351)?[9|8|6|7][0-9]{8}$/";
        if (!preg_match($pattern, $input)) {
            $result['is_valid'] = false;
            $result['message'] = "Número de telefone válido obrigatório de Portugal";
        }
    } else if ($field->cssClass == "gf-cp") {
		//Códigos postales
        if (!preg_match('/^\d{4}-\d{3}?$/', $value) ) {
            $result['is_valid'] = false;
            $result['message'] = "Adicione um código postal válido para Portugal";
        }

    }

    return $result;

}