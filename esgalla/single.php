<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package esgalla
 */

get_header();
get_template_part("template-parts/tema", "header");
?>

	<main id="primary" class="site-main">

		<div class="container-fluid px-0">

			<header id="masthead" class="site-header fullheight position-relative">

				<div class="container-fluid full-height-container h-100 big-post">
					<div class="row align-items-center h-100">
						<div class="col d-flex align-self-center bg-img-holder big-post-col" >
							<?php /*<img src="<?php echo get_template_directory_uri() ?>/img/img-android.jpg" class="img-fluid rounded" /> ?> */ ?>
							<?php echo get_the_post_thumbnail(null, null, ['alt' => get_the_title()]); ?>
						</div>
					</div>
				</div>

			</header><!-- #masthead -->


			<section class="single-post-title">
				<div class="container">
					<div class="row">
						<div class="col-12 px-1">
							<div class="rounded shadow bg-white big-post-title-card">
								<h1 class="h2 text-primary mb-3 wow animate__fadeInUp" data-wow-duration="2s"><?php echo get_the_title( ) ?></h1>
								<div class="h4 text-tokio-black font-weight-light mb-3"><?php echo (get_field('subtitulo')!="") ? get_field('subtitulo') : ''; ?></div>
								<?php
								$categorias = get_the_category();
								if ($categorias):
									foreach($categorias as $category) {
										echo '<a href="' . get_tag_link($category->term_id) . '"><span class="badge badge-tokio-green text-white mr-3 mt-3" style="white-space:normal;">'.$category->name.'</span></a>';
									}
								endif; ?>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

		<div class="container px-1 px-sm-3">
			<section class="single-post-content">
				<div class="container pt-3">
					<div class="row">
						<div class="mx-auto">
							<nav aria-label="breadcrumb">
								<div class="container">
									<ol class="breadcrumb">
										<li class="breadcrumb-item text-tokio-navyblue"><a class="" href="<?php echo get_home_url( ) ?>">Inicio</a></li>
										<li class="breadcrumb-item text-tokio-navyblue"><a class="" href="<?php echo get_the_permalink(53) ?>">Back Up Blog</a></li>
										<li class="breadcrumb-item text-tokio-green active" aria-current="page"><?php echo get_the_title( ) ?></li>
									</ol>
								</div>
							</nav>
						</div>
					</div>
				</div>

				<div class="container pt-5">
					<div class="row">
						<div class="col-noticias mx-auto">
							<div class="autor d-flex align-items-center">
								<!-- <img src="<?php echo get_template_directory_uri() ?>/img/janedoe.png" alt=""> -->
								<p class="mt-3 ml-3"><?php echo get_the_author_meta('display_name', $post->post_author) ?> | <?php echo get_the_date('d/m/Y') ?></p>
							</div>
							<?php the_content(); ?>
							<hr class="mt-5 mb-5 mb-lg-0 mt-lg-6 post-end" />
						</div>
					</div>
				</div>
				<div class="container m-0">
					<div class="row">
						<div class="col-12 px-3 py-4 py-md-5 p-lg-0 bg-tokio-navyblue rounded-lg form-contacto form-contacto-mobile-container"></div>
					</div>
				</div>
			</section>
		</div>


		<div class="container-fluid pt-0 mt-0 px-0" style="margin-top: -75px!important;">

			<?
				if(get_field('id')) {
					$id_prod = get_field('id');
					$posts = get_posts(array(
						'numberposts'	=> 1,
						'post_type'		=> 'formacion',
						'meta_key'		=> 'id',
						'meta_value'	=> $id_prod
					));

					$id_prod_wordpress = $posts[0]->ID;
					$categoria_prod = get_the_terms($id_prod_wordpress, 'categorias_formacion')[0];

					$prod_relacionados_conf['titulo'] = get_field('cabecera_fichas', 'categorias_formacion_' . $categoria_prod->term_id);
					$prod_relacionados_conf['categoria'] = $categoria_prod;
					$prod_relacionados_conf['excluidos'] = array();
					get_template_part('template-parts/blocks/block', 'producto-relacionados', $prod_relacionados_conf);
				}
			?>

			<?php
				$post_relacionados_conf['titulo'] = 'Pode também estar interessado em...';
				$post_relacionados_conf['categoria'] = wp_get_post_categories($post->ID);
				$post_relacionados_conf['limite'] = 10;
				$post_relacionados_conf['excluidos'] = array($post->ID);
				get_template_part('template-parts/blocks/block', 'post-relacionados', $post_relacionados_conf);
			?>

			<section id="nube-tags">
				<div class="container pt-0 pt-lg-3 pb-lg-6">
					<h2 class="text-tokio-navyblue mb-4 wow animate__fadeInUp" data-wow-duration="2s">Em Tóquio falamos sobre...</h2>
					<div class="row">
						<div class="col">
							<div class="single-post-badges py-1">
								<?php
									foreach (get_categories() as $categoria) {
										echo '<span class="badge badge-tokio-green mr-3 mt-3"><a class="text-white" href="' . get_category_link( $categoria ) . '">'.$categoria->cat_name.'</a></span>';
									}
								?>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="profesional-futuro" class=" price-card-form">
				<div class="container-fluid py-5 py-md-4 px-lg-4">
					<div class="mw-1500 mx-auto">
						<div class="row justify-content-between bg-tokio-navyblue px-3 px-xl-7 py-4 py-lg-6 form-card-container">
							<div class="col-12 col-lg py-lg-5 col-futuro form-contacto form-contacto-pc-container order-lg-last">
								<h2 class="text-white mb-3 mb-md-4 wow animate__fadeInUp" data-wow-duration="2s">Mais informação</h2>
								<p class="text-tokio-green mb-3 mb-md-4">Se chegaste até aqui é porque temos algo que te interessa, certo? Claro que sim! Tu também nos interessas. Estamos ansiosos para poder chamar-te pelo teu nome, falar contigo, saber em que podemos ajudar-te. Resumidamente: gostaríamos (muito!) de te conhecer. Envia-nos uma mensagem. Entraremos em contacto contigo num piscar de olhos.</p>
								<?php //echo do_shortcode( '[gravityform id="4" title="false" description="false" ajax="true" tabindex="459"]' ); ?>
								<?

									$id = (get_field("id")) ? get_field("id") : "2042";
									$posts = get_posts(array(
										'numberposts'	=> 1,
										'post_type'		=> 'formacion',
										'meta_key'		=> 'id',
										'meta_value'	=> $id
									));
									$id_post_wordpress = $posts[0]->ID;

								?>
								<?php

								$curso_categoria = wp_get_post_terms( $id_post_wordpress, 'categorias_formacion', array())[0]->slug;
								// $curso_tipo = wp_get_post_terms( $id_post_wordpress, 'tag_formacion', array())[0]->slug;
								$curso_tipo = get_field('tipo', $id_post_wordpress);

								gravity_form( 6, false, false, false, array(
									"nombrecurso" => get_the_title($id_post_wordpress),
									"idcurso" => $id,
									"idpais" => "1",
									"modollegada" => $MLL,
									"producto_tipo" => $curso_tipo,
									"producto_categoria" => $curso_categoria,
									"prelead_name" => get_the_title(),
									"prelead_tipo" => $GLOBALS['page_type'],
									"prelead_seccion" => $GLOBALS['page_section'],
									"prelead_author" => $GLOBALS['blog_author'],
									"prelead_blogcat" => $GLOBALS['blog_cat']
								), true, 600);
								?>
							</div>
							<div class="col-12 col-lg col-programa bg-white my-3 my-md-4">
								<p class="formacion-nombre mini-text mb-3">Todas as formações em Tokio School incluem:</p>
								<span class="h3 text-primary d-block">Desde</span>
								<span class="h2 text-primary d-block">1.800 €</span>
								<p class="iva-incluido mini-text mb-4">I.V.A. incluído</p>
								<ul class="list-group listado-formacion px-4">
									<li class="list-group-item mb-3">Formação 100% online</li>
									<li class="list-group-item mb-3">Más de 150 clases telepresenciales/mes </li>
									<li class="list-group-item mb-3">Masterclass complementares</li>
									<li class="list-group-item mb-3">Assessoria pedagógica</li>
									<li class="list-group-item mb-3">Formação em inglês</li>
									<li class="list-group-item mb-3">Estágio em empresas de referência</li>
									<li class="list-group-item mb-3">Tokio Net (alertas de emprego durante 5 anos)</li>
									<li class="list-group-item mb-3">E, além disso, 2 anos para concluires a tua formação</li>
								</ul>
								<hr class="mt-3 mb-3">
								<!-- <span class="text-primary d-block mb-3">Fechas del curso</span>
								<p class="">Las fechas del curso se determinarán una vez que se establezcan los miembros que conformarán cada grupo</p> -->

									<? if(get_field('pdf_adjunto', $id_post_wordpress)): ?>

										<!-- <a class="btn text-tokio-green py-3 px-0" target="_blank" href="<? echo get_field('pdf_adjunto', $id_post_wordpress); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/download-green.svg" class="img-fluid icon-download"/>Descargar programa</a> -->
										<a class="btn text-tokio-green py-3 px-4" data-toggle="modal" data-target="#modal-programa-formativo" href="javascript:void(0)" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/download-green.svg" class="img-fluid icon-download"/>Descargar programa</a>
										<div class="modal fade modal-programa-formativo form-contacto" id="modal-programa-formativo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											<div class="modal-dialog modal-dialog-centered" role="document">
												<div class="modal-content d-flex align-items-center bg-tokio-navyblue">
													<div class="modal-header align-self-end">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
													</div>
													<div class="w-100"></div>
												<div class="modal-body">
													<div class="h3 mb-0 text-tokio-green">Programa formativo: <?php echo $GLOBALS['product_name']; ?></div>
													<p class="my-4 text-white">Rellena tus datos y te enviaremos el programa formativo a tu email. ¡El camino para convertirte en un tokier empieza aquí!</p>
													<?php //echo do_shortcode( '[gravityform id="1" title="false" description="false" ajax="true" tabindex="49"]' ); ?>
													<?php
													gravity_form( 13, false, false, false, array(
														"nombrecurso" => $GLOBALS['product_name'], 
														"idcurso" => $GLOBALS['product_id'], 
														"idpais" => "1", 
														"modollegada" => $MLL,
														"producto_tipo" => $GLOBALS['product_type'], 
														"producto_categoria" => $GLOBALS['product_category'], 
														"prelead_name" => $GLOBALS['product_name'], 
														"prelead_tipo" => $GLOBALS['page_type'],
														"pdf" => get_field("catalogo"),
														"urlprogramaformativo" => get_field('pdf_adjunto')

													), true, 800);
													?>
												</div>
												</div>
											</div>
										</div>
									<? endif; ?>
							</div>
						</div>
					</div>
				</div>
			</section>

		</div>

	</main><!-- #main -->

<?php
get_footer();
