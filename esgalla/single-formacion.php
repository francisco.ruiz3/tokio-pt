<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package esgalla
 */

get_header();
get_template_part("template-parts/tema", "header");

$iconos_componentes = array('cube.svg', 'code-download.svg', 'bulb.svg', 'games.svg');
$terms = get_the_terms( get_the_ID(), 'categorias_formacion' );
?>

	<main id="single-formacion" class="site-main">
	

    <?php
    while ( have_posts() ) :
      the_post(); ?>
		<div>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<section id="formacion-header">
				<div class="container-fluid py-5 pt-5 py-md-5 pt-md-5">
					<nav aria-label="breadcrumb">
						<div class="container">
							<ol class="breadcrumb light">
								<li class="breadcrumb-item"><a class="text-tokio-dark" href="<?php echo get_home_url() ?>">Início</a></li>
								<li class="breadcrumb-item"><a class="text-tokio-dark" href="<?php echo get_post_type_archive_link('formacion') ?>">Formações</a></li>
								<? if(get_field('tipo') != 'Curso complementario'): ?>
									<li class="breadcrumb-item"><a class="text-tokio-dark" href="<?php echo get_term_link( $terms[0]->term_id ); ?>"><?php echo $terms[0]->name; ?></a></li>
								<? endif; ?>

								<li class="breadcrumb-item text-tokio-pink active" aria-current="page"><?php echo get_the_title( ) ?></li>
							</ol>
						</div>
					</nav>
					<div class="container  py-5">
						<div class="row">
							<div class="col-md-6">
								<div class="mb-3 mb-lg-5">
									<? if(get_field('tipo') != 'Curso complementario'): ?>
										<span class="formacion-dato mr-3"><img src="<?php echo get_template_directory_uri() ?>/img/award.svg" class="img-fluid"/> <?php echo (get_field('tipo')!='') ? get_field('tipo') : 'Carreira' ?></span>
										<span class="formacion-dato mr-3"><img src="<?php echo get_template_directory_uri() ?>/img/timer.svg" class="img-fluid"/> <?php echo (get_field('duracion')!='') ? get_field('duracion') : '300' ?>h</span>
									<? else: ?>
										<span class="formacion-dato mr-3"><img src="<?php echo get_template_directory_uri() ?>/img/award.svg" class="img-fluid"/> Curso gratuito</span>
									<? endif; ?>
								</div>
								<h1 class="h2 text-tokio-pink mb-4 mt-5 mt-lg-6 wow animate__fadeInUp" data-wow-duration="2s"><?php echo get_field('cabecera')['titulo_cabecera']; ?></h1>
								<p class="text-tokio-black mb-5 mt-4 mt-md-0"><?php echo get_field('cabecera')['descripcion_cabecera']; ?></p>
								<? if(get_field('tipo') != 'Curso complementario'): ?>
									<div class="d-flex flex-column flex-lg-row mb-5">
										<a class="btn btn-tokio-navyblue rounded-pill mb-3 mb-lg-0 py-3 px-4 mr-lg-4" role="button" data-toggle="modal" data-target="#modal-preinscribe" href="javascript:void(0)"><?php echo (get_field('cabecera')['texto_boton']!='') ? get_field('cabecera')['texto_boton'] : 'Mais Informações' ?></a>
										<?php if(get_field('pdf_adjunto')): ?>
											<a class="btn text-tokio-pink py-3 px-4" data-toggle="modal" data-target="#modal-programa-formativo" href="javascript:void(0)" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/download.svg" class="img-fluid icon-download"/>Descarregar programa</a>
										<?php endif; ?>
									</div>
								<? endif; ?>
							</div>
							<div class="col-md-6 text-lg-right">
								<img src="<?php echo get_field('cabecera')['imagen']['url'] ?>" class="img-fluid border border-4 border-tokio-pink rounded-circle p-3"/>
							</div>
						</div>
					</div>
				</div>
			</section>


			<section id="modulos-formacion" class="<?if(isset($_GET['scrum']) && get_field('incluye_sukiru')) {echo 'bg-tokio-pink';} ?>">
				<div class="container-fluid bg-tokio-navyblue rounded-bottom-right">
					<div class="container bg-navyblue-pico-top-left py-5 py-md-6">
					<? if(get_field('tipo') != 'Curso complementario'): ?>
						<div class="row">
							<div class="col-12">
								<h2 class="text-white mb-4 mb-md-5 wow animate__fadeInUp" data-wow-duration="2s">O que vais aprender?</h2>
								<p class="text-tokio-pink titilumregular mb-6 two-columns-text">
									<?php echo (get_field('que_aprenderas')['descripcion']!='') ? get_field('que_aprenderas')['descripcion'] : 'Officia non est elit pariatur amet occaecat. Exercitation fugiat mollit culpa aliqua incididunt consectetur velit. Enim exercitation mollit amet dolore deserunt quis sint anim culpa do. Culpa commodo pariatur deserunt minim laborum non aliqua voluptate nulla ad veniam nisi. Culpa laboris ipsum id aute deserunt ea irure dolore. Veniam aliquip ex nulla id aliquip officia elit et incididunt. Sunt dolor dolore aliqua commodo voluptate aliqua.Ex minim aliquip duis sit anim tempor dolor commodo. Nulla consectetur exercitation occaecat velit ad aute cillum duis exercitation ipsum magna. Dolore anim magna dolore Lorem voluptate dolore voluptate. Cillum eu proident incididunt eu sint pariatur officia pariatur. Esse non pariatur dolor ex aliqua. Quis aliquip est duis anim cupidatat do adipisicing.'; ?>
								</p>
								<?php if(get_field('que_aprenderas')['modulos']): ?>
								<div class="container horizontal-scrollable px-0">
									<div class="active-element-bar"></div>
									<div class="row d-block d-md-flex modulos-nav mb-5 mb-lg-6 px-3">
										<?php if(get_field('que_aprenderas')['modulos']):
											foreach (get_field('que_aprenderas')['modulos'] as $key => $modulo):
												$modulo_sel_titulo = explode(':', $modulo['titulo'])[0]; ?>
											<div class="col-4 col-sm-3 col-md col-lg d-flex flex-column text-center modulos-nav-item <?php echo ($key == 0)? 'active' : ''; ?>">
												<span class="modulo-name mb-2"><?php echo $modulo_sel_titulo; ?></span><span class="modulo-horas"><?php echo ($modulo['horas']!="") ? $modulo['horas'].'h' : '' ?></span>
											</div>
										<?php endforeach;
										 endif; ?>
									</div>
								</div>
								<?php endif; ?>
								<div class="row modulos-content mb-5 mb-md-6 pt-3">
									<?php if(get_field('que_aprenderas')['modulos']):
										foreach (get_field('que_aprenderas')['modulos'] as $key => $modulo):
											//$modulo_titulo = (strpos($modulo['titulo'], "Proyecto final")===0) ? $modulo['titulo'] : 'Módulo '.($key+1).': '.$modulo['titulo']; ?>
										<div class="col-12 modulos-content-item <?php echo ($key == 0)? 'active' : ''; ?> px-sm-5">
											<h3 class="modulo-title text-tokio-pink mb-5 mb-lg-6"><?php echo $modulo['titulo'] ?></h3>
											<div class="row">
											<?php foreach($modulo['componentes'] as $comp_i => $componente):
												$componente_icono = ($componente['icono']['url']) ? $componente['icono']['url'] : get_template_directory_uri().'/img/'.$iconos_componentes[$comp_i]; ?>
												<div class="col-md-6 py-4 pr-md-5 pl-md-0">
													<div class="media">
														<div class="px-2 mr-2">
															<img src="<?php echo $componente_icono ?>" class="img-fluid modulo-icon" style="max-width:48px"/>
														</div>
														<div class="media-body">
															<h4 class="text-tokio-pink mt-0"><?php echo $componente['titulo'] ?></h4>
															<div class="media-body-content"><?php echo nl2br($componente['descripcion']) ?></div>
														</div>
													</div>
												</div>
											<?php endforeach; ?>
											</div>
										</div>
									<?php endforeach;
									 endif; ?>
									<div class="col-12 text-center mt-3 mt-md-5">
										<?php if(get_field('pdf_adjunto')): ?>
											<a class="btn text-tokio-pink py-3 px-4" data-toggle="modal" data-target="#modal-programa-formativo" href="javascript:void(0)" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/download.svg" class="img-fluid icon-download"/>Descarregar programa</a>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					<? else: ?>
						<div class="row">
							<div class="col-lg-7 text-center d-flex flex-column justify-content-center mx-auto">
								<h3 class="text-tokio-pink mb-5">Módulo de metodologías ágiles</h3>
								<p class="text-white">En nuestro <strong>curso Scrum Manager</strong> te empaparás de una metodología de trabajo que está en boca de todas las empresas: la <strong>metodología Scrum</strong> (que está enmarcada dentro de las <strong>metodologías Agile</strong>). En Tokio School somos <strong>centro autorizado Scrum Manager</strong>. En este curso te preparamos para que obtengas la <strong>Certificación Scrum Manager</strong>.</p>
								<img src="/wp-content/themes/esgalla/img/centro-autorizado-scrum.png" class="img-fluid mx-auto mb-4" style="max-width:300px;">
								<a class="btn text-tokio-green py-3 px-4" href="/wp-content/uploads/2021/03/SCRM_-Esquema-de-contenidos.pdf" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/download-green.svg" class="img-fluid icon-download"/>Descobre qué aprenderás</a>
							</div>
						</div>
							
					<? endif; ?>
					</div>
				</div>
			</section>

			<? if(get_field('tipo') == 'Curso complementario'): ?>
			<section>
					<div class="container py-5 p-lg-6">
						<div class="row">
							<div class="col-12">
								<div class="col-12">
									<div id="scrum-faqs-1" class="accordion accordion-faqs mx-auto">
										<div class="card border-0 rounded-0 mb-0">
											<div class="card-header bg-white collapsed px-1 pb-4 pt-5" data-toggle="collapse" data-parent="#scrum-faqs-1" href="#collapse1">
												<a href="javascript: void(0)" class="card-title text-tokio-black h5 font-weight-bold"> ¿Qué es la metodología Scrum? </a>
											</div>
											<div id="collapse1" class="collapse p-0 bg-white" data-parent="#scrum-faqs-1">
												<div class="card-body px-1 py-3">Scrum es una metodología de trabajo en la cual se aplica de manera constante, un conjunto de buenas prácticas para trabajar colaborativamente en equipo y de esta forma lograr el mejor resultado posible de un proyecto. Estas prácticas se apoyan unas a otras y su selección tiene origen en un estudio de la manera de trabajar de equipos altamente productivos. También se utiliza para resolver situaciones en las cuales el cliente no está recibiendo lo que necesita, cuando las entregas de un proyecto se alargan demasiado, cuando los costes se disparan o la calidad no es aceptable, cuando se necesita capacidad de reacción ante la competencia, cuando la moral de los equipos es baja y la rotación alta, cuando es necesario identificar y solucionar ineficiencias sistemáticamente o cuando se quiere trabajar utilizando un proceso especializado en el desarrollo de producto, es decir, es una metodología que se puede aplicar en diferentes escenarios. Se enmarca dentro de las metodologías Agile.</div>
											</div>
											<div class="acordion-item-separator w-100"></div>
										</div>
									</div>

									<div id="scrum-faqs-2" class="accordion accordion-faqs mx-auto">
										<div class="card border-0 rounded-0 mb-0">
											<div class="card-header bg-white collapsed px-1 pb-4 pt-5" data-toggle="collapse" data-parent="#scrum-faqs-2" href="#collapse2">
												<a href="javascript: void(0)" class="card-title text-tokio-black h5 font-weight-bold"> ¿En qué se basan las metodologías Agile? </a>
											</div>
											<div id="collapse2" class="collapse p-0 bg-white" data-parent="#scrum-faqs-2">
												<div class="card-body px-1 py-3">En aspectos como la flexibilidad ante los cambios y nuevos requisitos durante un proyecto complejo, el factor humano, la colaboración e interacción con el cliente y el desarrollo iterativo como forma de asegurar buenos resultados.</div>
											</div>
											<div class="acordion-item-separator w-100"></div>
										</div>
									</div>

									<div id="scrum-faqs-3" class="accordion accordion-faqs mx-auto">
										<div class="card border-0 rounded-0 mb-0">
											<div class="card-header bg-white collapsed px-1 pb-4 pt-5" data-toggle="collapse" data-parent="#scrum-faqs-3" href="#collapse3">
												<a href="javascript: void(0)" class="card-title text-tokio-black h5 font-weight-bold"> ¿Por qué ser formarse en metodologías ágiles? </a>
											</div>
											<div id="collapse3" class="collapse p-0 bg-white" data-parent="#scrum-faqs-3">
												<div class="card-body px-1 py-3">Al igual que evolucionan la tecnología y los lenguajes de programación, las metodologías de trabajo también cambian con el tiempo. Las organizaciones que aspiran a innovar y a crecer buscan nuevos mecanismos para ser más eficientes y productivas. Formas de agilizar procesos, reducir costes y acelerar la producción para obtener mayores beneficios y es aquí donde la figura del Scrum master es clave para gestionar de forma efectiva esos proyectos y alcanzar los resultados esperados. Nuestro curso en metodologías ágiles con Certificación Scrum te permitirá dominar el marco Scrum y convertirte en Scrum Master, una de las titulaciones y profesiones más demandadas actualmente. Adicionalmente, te permitirá entender y aplicar otras metodologías Agile como Kanban, cada vez más en vigor en el dinámico mercado que nos envuelve.</div>
											</div>
											<div class="acordion-item-separator w-100"></div>
										</div>
									</div>

									<div id="scrum-faqs-4" class="accordion accordion-faqs mx-auto">
										<div class="card border-0 rounded-0 mb-0">
											<div class="card-header bg-white collapsed px-1 pb-4 pt-5" data-toggle="collapse" data-parent="#scrum-faqs-4" href="#collapse4">
												<a href="javascript: void(0)" class="card-title text-tokio-black h5 font-weight-bold"> ¿Salidas laborales de Scrum? </a>
											</div>
											<div id="collapse4" class="collapse p-0 bg-white" data-parent="#scrum-faqs-4">
												<div class="card-body px-1 py-3">
													En cuanto a las salidas laborales, el puesto de Scrum master es cada vez más demandado. Pero no solo la figura de Scrum sino tener experiencia o conocer la metodología es un plus en las entrevistas de trabajo dentro del mundo de la creación de software de cualquier tipo. Actualmente más del 60% de las empresas tecnológicas trabajan sus proyectos y necesidades bajo esta metodología. Certificándote como Scrum Master básicamente podrás desarrollar posiciones como:
													<ul>
														<li>Scrum master</li>
														<li>Consultor de proyectos ágiles</li>
														<li>Consultor transformación Agile</li>
													</ul>
												</div>
											</div>
											<div class="acordion-item-separator w-100"></div>
										</div>
									</div>

									<div id="scrum-faqs-5" class="accordion accordion-faqs mx-auto">
										<div class="card border-0 rounded-0 mb-0">
											<div class="card-header bg-white collapsed px-1 pb-4 pt-5" data-toggle="collapse" data-parent="#scrum-faqs-5" href="#collapse5">
												<a href="javascript: void(0)" class="card-title text-tokio-black h5 font-weight-bold"> ¿Cuánto cobra un especialista en Scrum? </a>
											</div>
											<div id="collapse5" class="collapse p-0 bg-white" data-parent="#scrum-faqs-5">
												<div class="card-body px-1 py-3">El sueldo promedio de un Scrum master en España puede oscilar entre los 32.000 y los 45.000 euros al año, en función de la empresa, las habilidades y la experiencia del profesional, aunque como comentamos, siempre hay excepciones por encima y por debajo de estos límites.</div>
											</div>
											<div class="acordion-item-separator w-100"></div>
										</div>
									</div>


								</div>
							</div>
						</div>
					</div>
			</section>
			<? endif; ?>

			<? if(isset($_GET['scrum']) && get_field('incluye_sukiru')): ?>
				<? get_template_part('template-parts/blocks/block', 'curso-complementario'); ?>
			<? endif; ?>
		</div>

<?php
$especialidades = array();
if(have_rows('especialidades')){
	foreach(get_field('especialidades') as $especialidad){
		array_push($especialidades, $especialidad);
	}
}
if($especialidades && (get_the_ID() == 108 || (get_the_ID() == 70 && isset($_GET['spring'])))):
	$logos_especialidades = array('artificial', 'machine', 'deep');
?>
<section id="especialidades" class="seccion-especialidades">
	<div class="container py-5 pb-md-5 pt-md-6">
		<div class="row">
			<div class="col-12">
				<h2 class="text-navyblue text-center wow animate__fadeInUp" data-wow-duration="2s">Especialidades</h2>
			</div>
		</div>
		<div class="row d-flex justify-content-center">
			<div class="col col-md-9 col-md-offset-3 bg-white mt-5">
				<div class="row list-group content-controls" id="list-tab" role="tablist">
					<?php foreach($especialidades as $key => $especialidad): ?>
					<a href="#list-<?php echo sanitize_title($especialidad["titulo"]) ?>" data-content="<?php echo sanitize_title($especialidad["titulo"]) ?>" class="list-group-item list-group-item-action col d-flex justify-content-center flex-column<?php echo ($key==0) ? ' active' : ''; ?>" id="show-<?php echo sanitize_title($especialidad["titulo"]) ?>">
						<!-- <img class="" src="<?php echo ($especialidad["icono"]["url"]=='') ? get_template_directory_uri().'/img/'.$logos_especialidades[$key].'.svg' : $especialidad["icono"]["url"] ; ?>" alt="Deep learning" style="height: 30px"> -->
						<h3 class="h4 text-center mt-2 mt-md-3 subtitle"><?php echo $especialidad["titulo"] ?></h3>
					</a>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="w-100 tab-content especialidades-content" >
				<?php foreach($especialidades as $key => $especialidad): ?>
				<div class="especialidades-content-item<?php echo ($key==0) ? ' active' : '' ; ?>" id="content-<?php echo sanitize_title($especialidad["titulo"]) ?>" <?php echo ($key!=0) ? ' style="display:none"' : '' ; ?>>
					<div class="d-flex justify-content-center">
						<div class="col col-md-9 col-md-offset-3 py-5 py-md-6 pl-0">
							<div class="h3 text-tokio-pink"><?php echo $especialidad["subtitulo"] ?></div>
							<div class="d-flex justify-content-md-left py-3">
								<p class="pr-3 text-tokio-black two-columns-text"><?php echo $especialidad["descripcion"] ?></p>
							</div>
							<div class="h3 text-tokio-navyblue py-4 py-md-5">Temario</div>
							<div class="row">
								<!-- <div class="col col-md-12 d-flex align-content-center justify-content-between px-md-3"> -->
									<?php foreach($especialidad['temario'] as $key_mod => $modulo): ?>
									<div class="col-6 col-md d-flex flex-column align-items-center mb-5 mb-md-0">
										<div class="num-temario"><?php echo $key_mod+1 ?></div>
										<span class="text-center text-tokio-navyblue font-weight-bold pt-3 pt-md-4"><?php echo $modulo["titulo"] ?></span>
										<span class="text-center text-secondary  pt-2"><?php echo $modulo["descripcion"] ?></span>
									</div>
									<?php endforeach; ?>
								<!-- </div> -->
							</div>
							<? if(count($especialidades) > 1): ?>
								<div class="italic-especialidad">*Podes estudar várias destas especialidades simultaneamente.
								</div>
							<? endif; ?>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>


			<?php
				$python = get_field('curso_python');

				if ($python && !$especialidades): ?>

					<section id="especialidades" class="seccion-especialidades">
						<div class="container py-5 py-md-6">
							<div class="row">
								<div class="col-12">
									<h2 class="text-white wow animate__fadeInUp" data-wow-duration="2s">Especialidades</h2>
								</div>
							</div>
							<div class="row d-flex justify-content-center">
								<div class="col col-md-9 col-md-offset-3 bg-white mt-5 shadow rounded">
									<div class="row list-group content-controls" id="list-tab" role="tablist">
										<a href="#list-inteligencia-artificial" data-content="inteligencia-artificial" class="list-group-item list-group-item-action active col d-flex justify-content-center flex-column" id="show-inteligencia-artificial">
											<img class="" src="<?php echo get_template_directory_uri() ?>/img/artificial.svg" alt="Inteligencia artificial">
											<h4 class="text-center subtitle mt-2 mt-md-3">Inteligencia Artificial</h4>
										</a>
										<a href="#list-machine-learning" data-content="machine-learning" class="list-group-item list-group-item-action col d-flex justify-content-center flex-column" id="show-machine-learning">
											<img class="" src="<?php echo get_template_directory_uri() ?>/img/machine.svg" alt="Machine learning">
											<h4 class="text-center subtitle mt-2 mt-md-3">Machine Learning</h4>
										</a>
										<a href="#list-deep-learning" data-content="deep-learning" class="list-group-item list-group-item-action col d-flex justify-content-center flex-column" id="show-deep-learning">
											<img class="" src="<?php echo get_template_directory_uri() ?>/img/deep.svg" alt="Deep learning">
											<h4 class="text-center subtitle mt-2 mt-md-3">Deep Learning</h4>
										</a>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="w-100 tab-content especialidades-content" >
									<div class="especialidades-content-item active" id="content-inteligencia-artificial" style="">
										<div class=" d-flex justify-content-center">
											<div class="col col-md-9 col-md-offset-3 py-5 py-md-6 pl-0">
												<h3>¿Qué aprenderás con DL? Inteligencia artificial</h3>
												<div class="d-flex justify-content-md-left py-3">
													<p class="two-columns-text">Apple es una de las empresas líderes en el mercado tecnológico y de innovación. Cada día lanzan nuevos proyectos con un éxito asegurado. Uno de ellos fue la creación de su lenguaje de programación Swift enfocado para el desarrollo de aplicaciones iOS y MacOS. Todavía queda mucho que aportar y aprender de este lenguaje. ¿Quieres ser un profesional que marcará el futuro? Con nuestro Carreira en Programación de Aplicaciones con Swift, lo serás.</p>
												</div>
												<h3 class="py-4 py-md-5">Temario</h3>
												<div class="d-flex justify-content-center">
													<!-- <div class="col col-md-12 d-flex align-content-center justify-content-between px-md-3"> -->
														<div class="container">
															<div class="row">
																<div class="col-6 col-md d-flex flex-column align-items-center mb-5 mb-md-0">
																	<div class="num-temario ">1</div>
																	<span class="text-center text-tokio-black font-weight-bold pt-3 pt-md-4">Módulo 1</span>
																	<span class="text-center text-tokio-black  pt-2">Introducción al DL</span>
																</div>
																<div class="col-6 col-md d-flex flex-column align-items-center mb-5 mb-md-0">
																	<div class="num-temario">2</div>
																	<span class="text-center text-tokio-black font-weight-bold pt-3 pt-md-4">Módulo 2</span>
																	<span class="text-center text-tokio-black  pt-2">Aprendizaje supervisado</span>
																</div>
																<div class="col-6 col-md d-flex flex-column align-items-center mb-5 mb-md-0">
																	<div class="num-temario">3</div>
																	<span class="text-center text-tokio-black font-weight-bold pt-3 pt-md-4">Módulo 3</span>
																	<span class="text-center text-tokio-black  pt-2">Conceptos avanzados</span>
																</div>
																<div class="col-6 col-md d-flex flex-column align-items-center mb-5 mb-md-0">
																	<div class="num-temario">4</div>
																	<span class="text-center text-tokio-black font-weight-bold pt-3 pt-md-4">Módulo 4</span>
																	<span class="text-center text-tokio-black  pt-2">Proyecto Final</span>
																</div>
															</div>
														</div>
													<!-- </div> -->
												</div>
												<div class="italic-especialidad">*Se pueden cursar varias especialidades. Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</div>
											</div>
										</div>
									</div>

									<div class="especialidades-content-item" id="content-machine-learning" style="display: none;">
										<div class="d-flex justify-content-center">
											<div class="col col-md-9 col-md-offset-3 py-5 py-md-6 pl-0">
												<h3>¿Qué aprenderás con DL? Machine learning</h3>
												<div class="d-flex justify-content-md-left py-3">
													<p class="two-columns-text">Apple es una de las empresas líderes en el mercado tecnológico y de innovación. Cada día lanzan nuevos proyectos con un éxito asegurado. Uno de ellos fue la creación de su lenguaje de programación Swift enfocado para el desarrollo de aplicaciones iOS y MacOS. Todavía queda mucho que aportar y aprender de este lenguaje. ¿Quieres ser un profesional que marcará el futuro? Con nuestro Carreira en Programación de Aplicaciones con Swift, lo serás.</p>
												</div>
												<h3 class="py-4 py-md-5">Temario</h3>
												<div class="d-flex justify-content-center">
													<!-- <div class="col col-md-12 d-flex align-content-center justify-content-between px-md-3"> -->
														<div class="container">
															<div class="row">
																<div class="col-6 col-md d-flex flex-column align-items-center mb-5 mb-md-0">
																	<div class="num-temario ">1</div>
																	<span class="text-center text-tokio-black font-weight-bold pt-3 pt-md-4">Módulo 1</span>
																	<span class="text-center text-tokio-black  pt-2">Introducción al DL</span>
																</div>
																<div class="col-6 col-md d-flex flex-column align-items-center mb-5 mb-md-0">
																	<div class="num-temario">2</div>
																	<span class="text-center text-tokio-black font-weight-bold pt-3 pt-md-4">Módulo 2</span>
																	<span class="text-center text-tokio-black  pt-2">Aprendizaje supervisado</span>
																</div>
																<div class="col-6 col-md d-flex flex-column align-items-center mb-5 mb-md-0">
																	<div class="num-temario">3</div>
																	<span class="text-center text-tokio-black font-weight-bold pt-3 pt-md-4">Módulo 3</span>
																	<span class="text-center text-tokio-black  pt-2">Conceptos avanzados</span>
																</div>
																<div class="col-6 col-md d-flex flex-column align-items-center mb-5 mb-md-0">
																	<div class="num-temario">4</div>
																	<span class="text-center text-tokio-black font-weight-bold pt-3 pt-md-4">Módulo 4</span>
																	<span class="text-center text-tokio-black  pt-2">Proyecto Final</span>
																</div>
															</div>
														</div>
													<!-- </div> -->
												</div>
												<div class="italic-especialidad">*Se pueden cursar varias especialidades. Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</div>
											</div>
										</div>
									</div>

									<div class="especialidades-content-item" id="content-deep-learning" style="display: none;">
										<div class="d-flex justify-content-center">
											<div class="col col-md-9 col-md-offset-3 py-5 py-md-6 pl-0">
												<h3>¿Qué aprenderás con DL? Deep learning</h3>
												<div class="d-flex justify-content-md-left py-3">
													<p class="two-columns-text">Apple es una de las empresas líderes en el mercado tecnológico y de innovación. Cada día lanzan nuevos proyectos con un éxito asegurado. Uno de ellos fue la creación de su lenguaje de programación Swift enfocado para el desarrollo de aplicaciones iOS y MacOS. Todavía queda mucho que aportar y aprender de este lenguaje. ¿Quieres ser un profesional que marcará el futuro? Con nuestro Carreira en Programación de Aplicaciones con Swift, lo serás.</p>
												</div>
												<h3 class="py-4 py-md-5">Temario</h3>
												<div class="d-flex justify-content-center">
													<!-- <div class="col col-md-12 d-flex align-content-center justify-content-between px-md-3"> -->
														<div class="container">
															<div class="row">
																<div class="col-6 col-md d-flex flex-column align-items-center mb-5 mb-md-0">
																	<div class="num-temario ">1</div>
																	<span class="text-center text-tokio-black font-weight-bold pt-3 pt-md-4">Módulo 1</span>
																	<span class="text-center text-tokio-black  pt-2">Introducción al DL</span>
																</div>
																<div class="col-6 col-md d-flex flex-column align-items-center mb-5 mb-md-0">
																	<div class="num-temario">2</div>
																	<span class="text-center text-tokio-black font-weight-bold pt-3 pt-md-4">Módulo 2</span>
																	<span class="text-center text-tokio-black  pt-2">Aprendizaje supervisado</span>
																</div>
																<div class="col-6 col-md d-flex flex-column align-items-center mb-5 mb-md-0">
																	<div class="num-temario">3</div>
																	<span class="text-center text-tokio-black font-weight-bold pt-3 pt-md-4">Módulo 3</span>
																	<span class="text-center text-tokio-black  pt-2">Conceptos avanzados</span>
																</div>
																<div class="col-6 col-md d-flex flex-column align-items-center mb-5 mb-md-0">
																	<div class="num-temario">4</div>
																	<span class="text-center text-tokio-black font-weight-bold pt-3 pt-md-4">Módulo 4</span>
																	<span class="text-center text-tokio-black  pt-2">Proyecto Final</span>
																</div>
															</div>
														</div>
													<!-- </div> -->
												</div>
												<div class="italic-especialidad">*Se pueden cursar varias especialidades. Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</div>
											</div>
										</div>
									</div>

									</div>
							</div>
						</div>
					</section>

				<?php endif; ?>


			<? if(get_field('tipo') != 'Curso complementario'): ?>
				<div class="bg-navyblue-arrow-2">
					<section id="nuestros-profesores">
						<div class="container">
							<div class="row">
								<div class="col-md-9 mx-auto">
									<h2 class="text-tokio-navyblue text-center mt-5 mt-xl-6 mb-4 wow animate__fadeInUp" data-wow-duration="2s">Os nossos senseis</h2>
									<p class="text-tokio-black text-center mb-5"><?php echo get_field('texto_introductorio_profesores'); ?></p>
								</div>
							</div>
						</div>
						<div class="container-fluid pl-lg-0">
							<div class="row row-carousel">
								<div class="col-12">
									<div class="carousel-profes" data-flickity='{ "groupCells": false, "contain": true, "wrapAround": false }'>
									<?php if(get_field('profesores')): foreach (get_field('profesores') as $profesor) : ?>
										<div class="carousel-cell">
											<div class="card  profe-card">
												<div class="card-horizontal">
													<div class="img-square-wrapper bg-img-holder text-center mb-3 mb-sm-0 profe-foto">
														<img src="<?php echo get_field('foto',$profesor)["url"] ?>" class="profe-foto img-fluid rounded-left rounded-lg" alt="Profesor 1 foto">
													</div>
													<div class="card-body">
														<h3 class="h5 profe-nombre mb-3"><?php echo get_the_title($profesor) ?></h3>
														<div class="h6 profe-materia mb-4"><?php echo get_field('puesto',$profesor) ?></div>
														<p class="profe-descripcion h5"><?php echo substr(get_field('descripcion',$profesor), 0, 150) ?></p>
														<div class="mt-4">
															<?php
															while ( have_rows('redes_sociales',$profesor) ){ the_row();
																//echo '<a href="'.get_sub_field('url').'"><i class="fab '.get_sub_field('icono').'"></i></a>';
															}
															?>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php endforeach; endif; ?>
									</div>
								</div>
							</div>
						</div>
					</section>

					<section id="metodologia-tokio" class="">
						<div class="container pt-5 pt-md-7">

							<div class="row">
								<div class="col-md-6 bg-image-circe-right">
									<!-- <img src="<?php echo get_template_directory_uri() ?>/img/ilustracion-metodologia-formacion.svg" class="d-none d-md-block img-fluid"/> -->
								</div>
								<div class="col-lg-6">
									<h2 class="text-tokio-navyblue mb-4 mb-md-5 wow animate__fadeInUp" data-wow-duration="2s">A metodologia de Tokio</h2>
									<!-- <img src="<?php echo get_template_directory_uri() ?>/img/ilustracion-metodologia-formacion.svg" class="d-md-none img-fluid mb-5"/> -->
									<div id="accordion-metodologia-tokio" class="accordion custom-accordion d-none d-md-block">
										<div class="card border-0 bg-white mb-0">
											<div class="accordion-item active">
												<div class="card-header border-0 bg-white pl-4 pt-4" data-toggle="collapse" href="#collapseOne2" aria-expanded="true">
													<a class="card-title h5">Aulas telepresenciais</a>
												</div>
												<div id="collapseOne2" class="collapse p-0 pl-1 collapse show" data-parent="#accordion-metodologia-tokio">
													<div class="card-body text-tokio-black pt-2">Todos os nossos senseis transmitem as suas aulas em direto. E, se não puderes assistir, não há problema! Voltaremos a retransmitir a aula noutro dia, dessa semana e, além disso, ficará disponível na plataforma virtual.</div>
												</div>
											</div>
											<div class="accordion-item">
												<div class="card-header border-0 bg-white collapsed pl-4 pt-4" data-toggle="collapse" data-parent="#accordion-metodologia-tokio" href="#collapseTwo2">
													<a class="card-title h5">Tutorias personalizadas</a>
												</div>
												<div id="collapseTwo2" class="collapse p-0 pl-1 collapse" data-parent="#accordion-metodologia-tokio">
													<div class="card-body text-tokio-black pt-2">Estaremos ao teu lado ao longo de toda a formação. Os nossos assessores pedagógicos vão ajudar-te a alcançar os teus objetivos (organização e motivação para toki-sans!).</div>
												</div>
											</div>
											<div class="accordion-item">
												<div class="card-header border-0 bg-white collapsed pl-4 pt-4" data-toggle="collapse" data-parent="#accordion-metodologia-tokio" href="#collapseThree2">
													<a class="card-title h5">Docentes especialistas </a>
												</div>
												<div id="collapseThree2" class="collapse p-0 pl-1" data-parent="#accordion-metodologia-tokio">
													<div class="card-body text-tokio-black pt-2">Especialistas reais no ativo que trabalharam em diversos tatamis (autênticos senseis!).</div>
												</div>
											</div>
											<div class="accordion-item">
												<div class="card-header border-0 bg-white collapsed pl-4 pt-4" data-toggle="collapse" data-parent="#accordion-metodologia-tokio" href="#collapseFour2">
													<a class="card-title h5">Flexibilidade</a>
												</div>
												<div id="collapseFour2" class="collapse p-0 pl-1" data-parent="#accordion-metodologia-tokio">
													<div class="card-body text-tokio-black pt-2">Adaptamo-nos a cada aluno fixando objetivos realistas que se ajustem às suas circunstâncias pessoais e às suas capacidades. O nosso modelo baseia-se numa aprendizagem “feita à medida”.</div>
												</div>
											</div>
										</div>
									</div>
									<div id="slider-como-lo-hacemos" class="d-sm-none">
										<div class="" data-flickity='{ "contain": true, "pageDots": false }'>
											<div class="carousel-cell">
												<h3 class="h5">Clases telepresenciales</h3>
												<p class="">Todos nuestros senséis transmiten sus clases en directo. Y si no has podido asistir, ¡no hay problema! Volvemos a retransmitir la clase otro día esa semana y, además, la subimos a la plataforma virtual.</p>
											</div>
											<div class="carousel-cell">
												<h3 class="h5">Tutorías personalizadas</h3>
												<p class="">Estaremos a tu lado a lo largo de toda la formación. Nuestros asesores pedagógicos te ayudarán a conseguir tus objetivos (¡organización y motivación para tokiers!).</p>
											</div>
											<div class="carousel-cell">
												<h3 class="h5">Profesores especialistas</h3>
												<p class="">Expertos reales en activo que han trabajado en diversos tatamis (¡auténticos senséis!).</p>
											</div>
											<div class="carousel-cell">
												<h3 class="h5">Flexibilidad</h3>
												<p class="">Nos adaptamos a cada alumno fijando objetivos realistas que se ajustan a sus circunstancias personales y a sus capacidades. El nuestro es un aprendizaje “hecho a medida”.</p>
											</div>
										</div>
									</div>
								</div>


							</div>

						</div>
					</section>
				</div>

				<div class="bg-green-arrow mb-lg-6">


					<section id="empresas-lideres" class="">
						<div class="container py-5 py-md-6">

						<?php get_template_part('template-parts/blocks/block', 'medallas'); ?>

							<div class="row d-flex justify-content-between align-items-center">
								<div class="col-md-6 pr-md-6">

									<h2 class="text-tokio-navyblue mb-4 wow animate__fadeInUp" data-wow-duration="2s">É no tatame que mostras o que vales</h2>
									<p class="text-tokio-black titilumsemibold mb-5">O teu tatame são as empresas, líderes no setor tecnológico e digital, nas quais podes aplicar os teus conhecimentos de samurai. Na Tokio School tens <strong>até 300 horas de estágios de qualidade</strong> para dar um push ao teu networking e um boost diferenciador ao teu CV. </p>
									<a class="btn btn-tokio-navyblue rounded-pill d-none d-md-inline-block py-3 px-4" href="/para-empresas/">Procure empresas</a>

								</div>
								<div class="col-md-6 d-flex justify-content-center p-3 p-md-4 innovacion">
									<div class="row text-center">
									<div class="col-4 d-flex justify-content-center"><img class="img-fluid" src="<?php echo wp_get_attachment_url( 4323 ) ?>" /></div>
										<div class="col-4 d-flex justify-content-center"><img class="img-fluid" src="<?php echo wp_get_attachment_url( 4324 ) ?>" /></div>
										<div class="col-4 d-flex justify-content-center"><img class="img-fluid" src="<?php echo wp_get_attachment_url( 4325 ) ?>" /></div>
										<div class="col-4 d-flex justify-content-center"><img class="img-fluid" src="<?php echo wp_get_attachment_url( 4326 ) ?>" /></div>
										<div class="col-4 d-flex justify-content-center"><img class="img-fluid" src="<?php echo wp_get_attachment_url( 4327 ) ?>" /></div>
										<div class="col-4 d-flex justify-content-center"><img class="img-fluid" src="<?php echo wp_get_attachment_url( 4328 ) ?>" /></div>
										<div class="col-4 d-flex justify-content-center"><img class="img-fluid" src="<?php echo wp_get_attachment_url( 4329 ) ?>" /></div>
										<div class="col-4 d-flex justify-content-center"><img class="img-fluid" src="<?php echo wp_get_attachment_url( 4330 ) ?>" /></div>
										<div class="col-4 d-flex justify-content-center"><img class="img-fluid" src="<?php echo wp_get_attachment_url( 4331 ) ?>" /></div>
										<div class="col-4 d-flex justify-content-center"><img class="img-fluid" src="<?php echo wp_get_attachment_url( 4332 ) ?>" /></div>
										<div class="col-4 d-flex justify-content-center"><img class="img-fluid" src="<?php echo wp_get_attachment_url( 4333 ) ?>" /></div>
										<div class="col-4 d-flex justify-content-center"><img class="img-fluid" src="<?php echo wp_get_attachment_url( 4334 ) ?>" /></div>

									</div>
								</div>
							</div>

						</div>
					</section>

	
				</div>



				<?php get_template_part('template-parts/blocks/block', 'speak-english'); ?>

			<? endif; ?>
			<?php if(have_rows('titulaciones')): //mostrar la sección solo cuando tiene alguna titulación ?>
				<?php while( have_rows('titulaciones') ) : the_row(); ?>
						<section id="titulacion-oficial">
							<div class="container py-5 py-md-5 pt-md-6">
								<div class="row">
									<div class="col-lg-9 text-center mx-auto">
											<h2 class="text-tokio-navyblue mb-3 mb-md-4"><?php echo get_sub_field('titulo'); ?></h2>
											<div class="text-tokio-black mb-4 w-md-50 mx-auto"><?php echo get_sub_field('descripcion'); ?></div>
									</div>
								</div>
								<div class="row text-center">
									<?php if(get_sub_field('logo')):
											echo '<div class="col-12 col-md my-3"><img src="'.get_sub_field('logo')["url"].'" class="img-fluid"/></div>';
									endif;?>
								</div>
							</div>
						</section>
				<?php endwhile; ?>
			<?php endif; ?>

			<? if(get_field('tipo') != 'Curso complementario'): ?>

				<section id="profesional-futuro" class=" price-card-form">
					<div class="container-fluid pt-0 pb-5 py-md-6 px-lg-6">
						<div class="row justify-content-between bg-tokio-navyblue px-3 px-xl-7 py-4 py-md-6 form-card-container">
							<div class="col-12 col-lg col-programa bg-white mb-5">
								<p class="formacion-nombre mini-text mb-3">Todas as formações em Tokio School incluem:</p>
								<span class="h3 text-tokio-navyblue d-block">Desde</span>
								<span class="h2 text-tokio-navyblue d-block">1.800 €</span>
								<p class="iva-incluido mini-text mb-4">I.V.A. incluido</p>
								<ul class="list-group listado-formacion px-4">
									<!-- <li class="list-group-item mb-3">2 años para acabar tu formación</li> -->
									<li class="list-group-item mb-3">Formação 100% online</li>
									<li class="list-group-item mb-3">Masterclass complementares</li>
									<li class="list-group-item mb-3">Assessoria pedagógica</li>
									<li class="list-group-item mb-3">Formação em inglês</li>
									<li class="list-group-item mb-3">Estágio em empresas</li>
									<li class="list-group-item mb-3">Tokio Net (alertas de emprego durante 5 anos)</li>
									<li class="list-group-item mb-3">E, além disso, 2 anos para concluir a tua formação</li>
								</ul>
								<hr class="mt-3 mb-3">
								<?php /*
								<p class="text-tokio-black fechas-del-curso mb-2">Fechas del curso</p>
								<p class="">Nuestros cursos empiezan en cuanto el grupo de alumnos está cerrado. ¡Pregúntanos cuándo es el siguiente!</p>
								*/ ?>
								<!-- <a class="btn text-secondary py-3 px-0" href="<?php echo (get_field('pdf_adjunto')!='') ? get_field('pdf_adjunto') : get_home_url( ).'/base-tokio.pdf' ?>"><img src="<?php echo get_template_directory_uri() ?>/img/download_pink.svg" class="img-fluid icon-download"/>Descargar programa</a> -->
							</div>
							<div class="col-12 col-lg col-futuro form-contacto px-0 px-lg-5">
								<div class="h2 text-white mb-3 mb-md-4 wow animate__fadeInUp" data-wow-duration="2s">Mais informação</div>
								<p class="text-tokio-green mb-3 mb-md-4 pb-2">Se chegaste até aqui abaixo é porque temos algo que te interessa, certo? Claro que sim! Tu também nos interessas. Estamos ansiosos para poder chamar-te pelo teu nome, falar contigo, saber em que te podemos ajudar. Em resumo: gostaríamos (muito!) de te conhecer. Envia-nos uma mensagem. Entraremos em contacto contigo num piscar de olhos.</p>
								<?php //echo do_shortcode( '[gravityform id="4" title="false" description="false" ajax="true" tabindex="459"]' ); ?>
								<?php
								gravity_form( 5, false, false, false, array(
									"nombrecurso" => $GLOBALS['product_name'], "idcurso" => $GLOBALS['product_id'], "idpais" => "1", "modollegada" => $MLL,
									"producto_tipo" => $GLOBALS['product_type'], "producto_categoria" => $GLOBALS['product_category'], "prelead_name" => $GLOBALS['product_name'], "prelead_tipo" => $GLOBALS['page_type'],
									"pdf" => get_field("catalogo")
								), true, 100);
								?>
							</div>
						</div>
					</div>
				</section>

				<section id="preguntas-frecuentes">
					<div class="container py-5 py-md-5 d-flex justify-content-center">
						<div class="row">
							<div class="col">
								<span class="h2 text-tokio-navyblue d-block mb-5 wow animate__fadeInUp" data-wow-duration="2s">FAQ</span>
								<div id="accordion-faqs-titulos-y-certificados" class="accordion accordion-faqs">
									<div class="card border-0 rounded-0 mb-0">
										<div class="card-header bg-white collapsed px-1 py-4" data-toggle="collapse" href="#collapseTitulos1">
											<a class="card-title h5 font-weight-bold">Que tipos de aulas vou ter?</a>
										</div>
										<div id="collapseTitulos1" class="collapse p-0 bg-white collapse" data-parent="#accordion-faqs-titulos-y-certificados">
											<div class="card-body px-1 py-3">
												<p>Terás 4 tipos de aulas na tua plataforma virtual:</p>
												<ul>
													<li class="mb-4">
														<strong>Aulas em direto</strong>
														<p>Conecta-te em direto às aulas online com o teu professor especialista, que te vai explicar o programa e abordará questões relacionadas à realidade laboral atual. No teu calendário podes consultar os dias e horas destas aulas, incluídas na tua formação. A principal vantagem de assistires a estas sessões em direto é que, sempre que tiveres uma dúvida podes aproveitar para a colocar diretamente ao professor, no final da sessão.</p>
													</li>
													<li class="mb-4">
														<strong>Aulas em streaming</strong>
														<p>Não conseguiste assistir às aulas em direto? Sem problema! Todas as aulas são gravadas para que possas aceder-lhes sempre que quiseres, através da nossa plataforma virtual, o que te permite também estudar ao teu ritmo. Além disso, as aulas gravadas voltam a ser emitidas semanalmente. Podes consultar o teu calendário e descobrir quando é a próxima transmissão da aula do tema que te interessa.</p>
													</li>
													<li class="mb-4">
														<strong>Aulas de resolução de dúvidas</strong>
														<p>Uma vez por semana, os professores dedicam uma aula à resolução de dúvidas, exercícios/casos práticos e à revisão de conteúdos. Estas sessões são uma excelente oportunidade de interagir com o professor, colocar questões e aprender com as dúvidas colocadas pelos teus colegas.</p>
													</li>
													<li class="mb-4">
														<strong>Masterclasses complementares</strong>
														<p>A metodologia de estudo da Tokio School supera o programa formativo. Já que, durante todo o curso, os alunos podem assistir a masterclasses, que complementam o seu conhecimento e CV. Como preparar uma entrevista de trabalho, como construir um bom CV, a indústria e os diferentes perfis profissionais são alguns dos temas que podem ser abordados.</p>
														<p>Quanto mais competências e aptidões adquirires, melhor preparado estás para entrar no mercado laboral, porque o nosso objetivo é que os alunos sigam #alwaysforward!</p>
													</li>
												</ul>
											</div>
										</div>
										<div class="acordion-item-separator w-100"></div>

										<div class="card-header bg-white collapsed px-1 py-4" data-toggle="collapse" data-parent="#accordion-faqs-titulos-y-certificados" href="#collapseTitulos2">
											<a class="card-title h5 font-weight-bold">O que necessito para fazer um curso na Tokio School?</a>
										</div>
										<div id="collapseTitulos2" class="collapse p-0 bg-white collapse" data-parent="#accordion-faqs-titulos-y-certificados">
											<div class="card-body px-1 py-3">
												<p>Genericamente falando, não há requisitos de admissão na Tokio School. O imprescindível é teres um computador e saberes trabalhar com ele.</p>
												<p>Contudo, em alguns cursos são necessários requisitos específicos, como ter um computador com determinadas caraterísticas (para o caso do curso de Unreal, por exemplo), ou ter conhecimentos mínimos de matemática (para o curso de Python, por exemplo).</p>
											</div>
										</div>
										<div class="acordion-item-separator w-100"></div>

										<div class="card-header bg-white collapsed px-1 py-4" data-toggle="collapse" data-parent="#accordion-faqs-titulos-y-certificados" href="#collapseTitulos3">
											<a class="card-title h5 font-weight-bold">Quanto custam os cursos da Tokio School?</a>
										</div>
										<div id="collapseTitulos3" class="collapse p-0 bg-white" data-parent="#accordion-faqs-titulos-y-certificados">
											<div class="card-body px-1 py-3">
												<p>As formações da Tokio School têm um custo que varia entre os €1800 e os €4000, dependendo da sua duração, programa ou certificação académica.</p>
												<p>Também é necessário ter em conta que o preço do curso difere consoante a forma de pagamento escolhida. Podes optar por pagar a pronto, financiamento ou pagar em prestações. Por exemplo: pagar um curso de €2200 euros em 18 meses custará €120 por mês, um valor mais acessível.</p>
												<p>E este valor é fixo durante todo o curso. Desta forma, poderás saber quanto custa a tua formação, sem valores ocultos ou mensalidades indefinidas que se arrastam no tempo.</p>
												<p>Pontualmente, os alunos têm a possibilidade de beneficiar de promoções, pelo que o preço das formações será inferior.</p>
											</div>
										</div>
										<div class="acordion-item-separator w-100"></div>

										<div class="card-header bg-white collapsed px-1 py-4" data-toggle="collapse" data-parent="#accordion-faqs-titulos-y-certificados" href="#collapseTitulos4">
											<a class="card-title h5 font-weight-bold">Como serão os meus estágios na Tokio School?</a>
										</div>
										<div id="collapseTitulos4" class="collapse p-0 bg-white" data-parent="#accordion-faqs-titulos-y-certificados">
											<div class="card-body px-1 py-3">
												<p>Geralmente, os estágios são presenciais, mas, dependendo do curso, existe também a possibilidade de se optar pela versão em teletrabalho, com as mesmas garantias. Os estágios devem iniciar-se durante o período da matrícula e têm uma duração entre as 60 e as 300 horas.</p>
												<p>Muitas vezes, as empresas decidem contratar os alunos estagiários e esta é uma grande oportunidade para entrares no mercado laboral.</p>
											</div>
										</div>
										<div class="acordion-item-separator w-100"></div>

										<div class="card-header bg-white collapsed px-1 py-4" data-toggle="collapse" data-parent="#accordion-faqs-titulos-y-certificados" href="#collapseTitulos5">
											<a class="card-title h5 font-weight-bold">Quanto tempo preciso para estar preparado, na Tokio School?</a>
										</div>
										<div id="collapseTitulos5" class="collapse p-0 bg-white" data-parent="#accordion-faqs-titulos-y-certificados">
											<div class="card-body px-1 py-3">
												<p>Isso depende do conteúdo da formação – da sua extensão – e do tempo que poderás dedicar-lhe. Quanto mais te dedicares, mais depressa irás concluir o teu curso. A Tokio School adapta-se ao teu ritmo e às tuas necessidades. Em linhas gerais, o tempo médio de preparação dos nossos alunos é de 7 a 12 meses, mas terás até dois anos para realizar cada programa formativo.</p>
											</div>
										</div>
										<div class="acordion-item-separator w-100"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<?php if(get_field('carrera_profesional_asociada')): ?>
				<section id="carrera-asociada">
					<?php get_template_part('template-parts/components/component', 'carrera-asociada'); ?>
				</section>
				<?php endif; ?>

			<? endif; ?>

		</article><!-- #post-<?php the_ID(); ?> -->


  <?php endwhile; // End of the loop. ?>

	</main><!-- #main -->

<? if(get_field('pdf_adjunto')): ?>
	<div class="modal fade modal-programa-formativo form-contacto" id="modal-programa-formativo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content d-flex align-items-center bg-tokio-navyblue">
	      <div class="modal-header align-self-end">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="w-100"></div>
			<div class="modal-body">
				<div class="h3 mb-0 text-tokio-green">Programa formativo: <?php echo $GLOBALS['product_name']; ?></div>
				<p class="my-4 text-white">Preenche os teus dados e entraremos em contacto contigo para tirar-te todas as dúvidas. O caminho para te tornares num toki-san começa aqui!</p>
				<?php //echo do_shortcode( '[gravityform id="1" title="false" description="false" ajax="true" tabindex="49"]' ); ?>
				<?php
				gravity_form( 13, false, false, false, array(
					"nombrecurso" => $GLOBALS['product_name'], 
					"idcurso" => $GLOBALS['product_id'], 
					"idpais" => "1", 
					"modollegada" => $MLL,
					"producto_tipo" => $GLOBALS['product_type'], 
					"producto_categoria" => $GLOBALS['product_category'], 
					"prelead_name" => $GLOBALS['product_name'], 
					"prelead_tipo" => $GLOBALS['page_type'],
					"pdf" => get_field("catalogo"),
					"urlprogramaformativo" => get_field('pdf_adjunto')

				), true, 800);
				?>
			</div>
	    </div>
	  </div>
	</div>
<? endif; ?>

<?php
get_footer();
