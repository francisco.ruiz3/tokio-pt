<?php
/**
* The template for displaying archive pages
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package esgalla
*/

get_header();

get_template_part("template-parts/tema", "header");


$productos_en_categoria = get_posts(

	array(

		'posts_per_page' => -1,

		'post_type' => 'formacion',

		'tax_query' => array(

      'relation' => 'AND',

			array(

				'taxonomy' => 'categorias_formacion',

				'field' => 'term_id',

				'terms' => get_queried_object()->term_id,

      ),

      //Excluimos las carreras profesionales de este listado

      array(

        'taxonomy' => 'tag_formacion',

        'terms'    => 'carreras-profesionales',

        'field'    => 'slug',

        'operator' => 'NOT IN',

      )

      )

	)

);



//wp_reset_query();



?>



<main id="taxonomy-categorias_formacion" class="site-main">

<?



if(isset($_GET['test'])) {

	echo get_the_title();

}



?>

	<div class="container-fluid bg-tokio-navyblue py-5 pt-5 py-md-5 pt-md-5">

		<nav aria-label="breadcrumb">

			<div class="container">

				<ol class="breadcrumb">

					<li class="breadcrumb-item"><a class="text-white" href="<?php echo get_home_url( ) ?>">Inicio</a></li>

					<li class="breadcrumb-item"><a class="text-white" href="<?php echo get_post_type_archive_link('formacion') ?>">Formações</a></li>

					<li class="breadcrumb-item text-tokio-green active" aria-current="page"><?php echo get_queried_object()->name ?></li>

				</ol>

			</div>

		</nav>

		<div class="container full-height-container h-100 py-5 py-md-6">

			<div class="row align-items-center h-100">

				<div class="col-lg-6 align-self-center">

					<div class="masthead-title h1 text-tokio-green mb-4 wow animate__fadeInUp" data-wow-duration="2s"><?php echo get_field('titulo_cabecera', get_queried_object()) ?></div>

					<p class="masthead-lead titilumregular text-white mb-4"><?php echo get_queried_object()->description ?></p>

				</div>

				<div class="col-lg-6 order-lg-last align-self-center text-lg-right" >

					<div id="parallaxhover">

						<div data-depth="0.2">

							<img src="<?php echo get_field('ilustracion', get_queried_object()); ?>" class="img-fluid"/>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>



	<div class="container formaciones-grid programacion-grid py-5 pb-md-5">

		<div class="row">

			<div class="col mb-4 mb-md-5">

				<h1 class="text-tokio-navyblue h2 wow animate__fadeInUp" data-wow-duration="2s"><?php echo get_field('cabecera_fichas', get_queried_object()) ?></h1>

			</div>

		</div>



		<div class="row">

			<?php foreach ($productos_en_categoria as $producto) : $producto_id = $producto->ID; ?>

				<div class="col-md-6 col-lg-4 mb-4">

					<div class="card formacion-card rounded h-100" style="">

						<!-- <div class="card-img-top-container bg-img-holder rounded mx-3 mt-3"><img src="<?php echo get_template_directory_uri() ?>/img/img-formacion-<?php echo ($producto_id%3)+1 ?>.jpg" class="img-fluid"/></div> -->

						<div class="card-img-top-container bg-img-holder rounded mx-3 mt-3"><img src="<?php echo get_field('imagen_tarjetas', $producto_id)['sizes']['medium']; ?>" class="img-fluid" alt="<?php echo get_field('cabecera', $producto_id)["titulo_cabecera"] ?>"/></div>

						

						<div class="card-body">

							<div class="formacion-badges d-flex my-4">

								<span class="badge badge-tokio-green text-white mr-3 "><img src="<?php echo get_template_directory_uri() ?>/img/badge-award.svg" class="img-fluid"/> <?php echo (get_field('tipo', $producto_id)) ? get_field('tipo', $producto_id) : 'Curso' ?></span>

								<span class="badge badge-tokio-green text-white mr-3 "><img src="<?php echo get_template_directory_uri() ?>/img/badge-timer.svg" class="img-fluid"/> <?php echo (get_field('duracion', $producto_id)) ? get_field('duracion', $producto_id) : '200' ?> h</span>

								<span class="badge badge-tokio-green text-white ml-auto"><?php echo (get_field('modalidad', $producto_id)) ? get_field('modalidad', $producto_id) : 'Online' ?></span>

							</div>

							<h2 class="h5 card-title text-primary"><?php echo get_field('cabecera', $producto_id)["titulo_cabecera"] ?></h2>

							<p class="card-text"><?php echo get_field('descripcion_tarjetas', $producto_id); ?></p>

							<a href="<?php echo get_the_permalink( $producto_id ) ?>" class="stretched-link"></a>

						</div>

					</div>

				</div>

			<? endforeach; ?>

		</div>

	</div>



<?php
	$post_relacionados_conf['titulo'] = 'Artigos relacionados';
	$post_relacionados_conf['categoria'] = get_queried_object()->slug;
	$post_relacionados_conf['limite'] = 10;
	$post_relacionados_conf['excluidos'] = array();
	get_template_part('template-parts/blocks/block', 'post-relacionados', $post_relacionados_conf);
?>


</main><!-- #main -->


<? if( get_field('id') ): ?>


	<? endif; ?>

<?php



// get_sidebar();
get_footer();

