<?php
/**
* Template part for displaying page content in page-noticias.php
*
* @package esgalla
*/

get_header();
get_template_part("template-parts/tema", "header");
?>

<main id="primary" class="site-main page-id-53">

	<div class="container-fluid px-0">

	<header id="masthead" class="site-header fullheight position-relative bg-tokio-navyblue">



		<div class="container full-height-container h-100 py-4 pb-5 pt-md-6 pb-md-5 big-post">

			<div class="row align-items-center h-100">

				<div class="col d-flex rounded align-self-center text-center big-post-col" >

					<div class="big-post-col-header p-4 wow animate__fadeInUp" data-wow-duration="2s">

						<h1 class="h2 text-white">Notícias</h1>

						<span class="h3 text-tokio-navyblue font-italic">#alwaysfoward</span>

					</div>



				</div>

			</div>

		</div>

		<div class="container">

			<div class="row">

				<div class="col-sm-9 mx-auto mb-5 mb-lg-0">

					<div class="rounded shadow bg-white text-tokio-navyblue font-weight-bold text-center big-post-excerpt p-5 p-lg-5">

						Como nuestros alumnos, nuestras noticias siempre van por delante. Descubre  tendencias, avances y curiosidades sobre el sector tecnológico y el mundo que viene.

					</div>

				</div>

			</div>

		</div>





	</header><!-- #masthead -->

		<section id="posts-grid">
			
			<div class="container py-5 py-md-6 pt-lg-7 pl-lg-0">
				<h2 class="d-block mb-4">Notícias: <? single_cat_title(); ?></h2>
				<div class="row posts-container">

					<?php

					while ( have_posts() ) : the_post();
						$post_card = [
							'post_img' => get_the_post_thumbnail_url(null, 'medium'),
							'post_title' => get_the_title(),
							'post_excerpt' => get_the_excerpt(),
							'post_link' => get_the_permalink(),
						];
						echo '<div class="col-md-6 col-lg-4 mb-5">';
							ec_get_template_part('template-parts/components/component', 'post-card', $post_card);
						echo '</div>';
					endwhile;
					?>

			</div>
			<div class="row">
				<div style="display:none!important;" class="hidden-pagination">
					<? wpbeginner_numeric_posts_nav(); ?>
				</div>
				<? wp_reset_query(); ?>
				<div class="col text-center mt-5 mt-md-5">
					<!-- <a class="btn btn-primary py-3 px-4" href="#">Ver más noticias</a> -->
					<?php echo do_shortcode( '[facetwp facet="noticias_ver_ms"]' ); ?>
				</div>
			</div>
		</div>
	</section>

	<section id="nube-tags">
		<div class="container pt-5 pt-lg-6 pb-lg-5">
			<h2 class="text-tokio-navyblue mb-4 wow animate__fadeInUp" data-wow-duration="2s">Em Tóquio falamos sobre...</h2>
			<div class="row">
				<div class="col">
					<div class="single-post-badges py-1">
						<?php
							foreach (get_categories() as $categoria) {
								echo '<span class="badge badge-tokio-green mr-3 mt-3"><a class="text-white" href="' . get_category_link( $categoria ) . '">'.$categoria->cat_name.'</a></span>';
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>

</main><!-- #main -->

<?php
get_footer();
